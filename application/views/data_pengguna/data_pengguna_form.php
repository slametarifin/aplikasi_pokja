<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header">
                <div class="box-title">
                    <b><i class="fa fa-tasks"></i> <?= $button ?> Data Data_pengguna</b>
                </div>
                <div class="box-tools pull-right">
                    <button type="button" class="toggle-expand-btn btn btn-default btn-sm"><i class="fa fa-expand"></i></button>
                </div>
            </div>
            <div class="box-body">
                <form style="padding: 15px;" action="<?= $action; ?>" method="POST" enctype="multipart/form-data">
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="varchar">Nama Pengguna</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="nama_pengguna" id="nama_pengguna" placeholder="Nama Pengguna" value="<?= $nama_pengguna; ?>" />
                                <?= form_error('nama_pengguna') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="bigint">Kode Desa</label>
                            <div class="col-md-6">
                                <select class="form-control" name="kode_desa">
                                    <option value="-">pilih desa</option>
                                </select>
                                <!-- <input type="text" class="form-control" name="kode_desa" id="kode_desa" placeholder="Kode Desa" value="<?= $kode_desa; ?>" /> -->
                                <?= form_error('kode_desa') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="int">Level Pengguna</label>
                            <div class="col-md-6">
                                <select class="form-control" name="level_pengguna">
                                    <option value="-">pilih desa</option>
                                    <option value="1">pengguna</option>
                                    <option value="2">Admin_desa</option>
                                    <option value="3">Administrator</option>
                                </select>
                                <!-- <input type="text" class="form-control" name="level_pengguna" id="level_pengguna" placeholder="Level Pengguna" value="<?= $level_pengguna; ?>" /> -->
                                <?= form_error('level_pengguna') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-2">
                              
                                <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> <?= $button ?></button>
                                <a href="<?= site_url('data_pengguna') ?>" class="btn btn-danger"><i class="fa fa-sign-out"></i> Kembali</a>
                            </div>
                        </div>
                    </div>
				</form>
            </div>
        </div>
    </div>
</div>