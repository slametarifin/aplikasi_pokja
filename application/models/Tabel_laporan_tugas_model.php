<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tabel_laporan_tugas_model extends CI_Model
{

    public $table = 'tabel_laporan_tugas';
    public $id = 'id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function json($gugus=null,$tugas=null) {
        $this->datatables->select('a.id,a.id_tugas,a.id_gugus,a.link,a.acc,nama_gugus,tabel_tugas.tugas');
        $this->datatables->from('tabel_laporan_tugas a');
        $this->datatables->where('a.id_gugus',$gugus);
        $this->datatables->where('a.id_tugas',$tugas);
        //add this line for join
        $this->datatables->join('tabel_gugus', 'a.id_gugus = tabel_gugus.id');
        $this->datatables->join('tabel_tugas_gugus', 'a.id_tugas = tabel_tugas_gugus.id');
        $this->datatables->join('tabel_tugas', 'tabel_tugas_gugus.tugas = tabel_tugas.id');
        $this->datatables->add_column('action', anchor(site_url('tabel_laporan_tugas/read/$1'),'<i class="bi bi-eye" aria-hidden="true"></i>', array('class' => 'btn btn-sm btn-success', 'title' => 'Lihat Detail Data'))." 
            ".anchor(site_url('tabel_laporan_tugas/update/$1'),'<i class="bi bi-pencil" aria-hidden="true"></i>', array('class' => 'btn btn-sm btn-warning', 'title' => 'Ubah Data'))." 
                ".anchor(site_url('tabel_laporan_tugas/delete/$1'),'<i class="bi bi-trash" aria-hidden="true"></i>','class="btn btn-sm btn-danger hapus" title="Hapus Data"'), 'id');
        return $this->datatables->generate();
    }

    // datatables
    function jsonnn($id_opd) {
        $this->datatables->select('(select nama_opd from tabel_opd where tabel_opd.id=tabel_gugus.kode_opd)nama_opd,tabel_gugus.kode_opd,a.id,a.id_tugas,a.id_gugus,a.link,a.acc,nama_gugus, tabel_tugas.tugas');
        $this->datatables->from('tabel_laporan_tugas a');
        $this->datatables->where('tabel_gugus.kode_opd',$id_opd);
        // $this->datatables->where('a.id_tugas',$tugas);
        //add this line for join
        // $this->datatables->join('tabel_opd', 'tabel_gugus.kode_opd = tabel_opd.id');
        $this->datatables->join('tabel_gugus', 'a.id_gugus = tabel_gugus.id');
        $this->datatables->join('tabel_tugas_gugus', 'a.id_tugas = tabel_tugas_gugus.id');
        $this->datatables->join('tabel_tugas', 'tabel_tugas_gugus.tugas = tabel_tugas.id');
        $this->datatables->add_column('action', anchor(site_url('tabel_laporan_tugas/read/$1'),'<i class="bi bi-eye" aria-hidden="true"></i>', array('class' => 'btn btn-sm btn-success', 'title' => 'Lihat Detail Data'))." 
            ".anchor(site_url('tabel_laporan_tugas/update/$1'),'<i class="bi bi-pencil" aria-hidden="true"></i>', array('class' => 'btn btn-sm btn-warning', 'title' => 'Ubah Data'))." 
                ".anchor(site_url('tabel_laporan_tugas/delete/$1'),'<i class="bi bi-trash" aria-hidden="true"></i>','class="btn btn-sm btn-danger hapus" title="Hapus Data"'), 'id');
        return $this->datatables->generate();
    }

    // datatables
    function jsonn($nip_ketua=null) {
        $this->datatables->select('a.id,a.id_tugas,a.id_gugus,a.link,a.acc,nama_gugus,tabel_tugas.tugas,tabel_gugus.nip_ketua');
        $this->datatables->from('tabel_laporan_tugas a');
        $this->datatables->where('nip_ketua',$nip_ketua);
        //add this line for join
        $this->datatables->join('tabel_gugus', 'a.id_gugus = tabel_gugus.id');
        $this->datatables->join('tabel_tugas_gugus', 'a.id_tugas = tabel_tugas_gugus.id');
        $this->datatables->join('tabel_tugas', 'tabel_tugas_gugus.tugas = tabel_tugas.id');
        $this->datatables->add_column('action', anchor(site_url('tabel_laporan_tugas/read/$1'),'<i class="bi bi-eye" aria-hidden="true"></i>', array('class' => 'btn btn-sm btn-success', 'title' => 'Lihat Detail Data'))." 
            ".anchor(site_url('tabel_laporan_tugas/update/$1'),'<i class="bi bi-pencil" aria-hidden="true"></i>', array('class' => 'btn btn-sm btn-warning', 'title' => 'Ubah Data'))." 
                ".anchor(site_url('tabel_laporan_tugas/delete/$1'),'<i class="bi bi-trash" aria-hidden="true"></i>','class="btn btn-sm btn-danger hapus" title="Hapus Data"'), 'id');
        return $this->datatables->generate();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
       $this->db->like("cast( id as text)", $q);
		$this->db->or_like("cast(id_tugas as text)", $q);
		$this->db->or_like("cast(id_gugus as text)", $q);
		$this->db->or_like("cast(link as text)", $q);
		$this->db->or_like("cast(acc as text)", $q);
		$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like("cast( id as text)", $q);
		$this->db->or_like("cast(id_tugas as text)", $q);
		$this->db->or_like("cast(id_gugus as text)", $q);
		$this->db->or_like("cast(link as text)", $q);
		$this->db->or_like("cast(acc as text)", $q);
		$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

/* End of file Tabel_laporan_tugas_model.php */
/* Location: ./application/models/Tabel_laporan_tugas_model.php */
/* Please DO NOT modify this information : */
/* Generated by CRUDV2 Generator For AdminLTE Template 2024-01-15 03:04:46 */