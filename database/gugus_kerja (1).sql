-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 11, 2024 at 09:11 AM
-- Server version: 10.4.11-MariaDB
-- PHP Version: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gugus_kerja`
--

-- --------------------------------------------------------

--
-- Table structure for table `anggota_gugus`
--

CREATE TABLE `anggota_gugus` (
  `id` int(10) UNSIGNED NOT NULL,
  `nip` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_gugus` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2024_01_03_041109_create_anggota_guguses_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `tabel_akun`
--

CREATE TABLE `tabel_akun` (
  `id` int(11) NOT NULL,
  `user_name` varchar(20) NOT NULL,
  `pass` varchar(255) NOT NULL,
  `is_active` varchar(5) NOT NULL,
  `id_user` bigint(20) NOT NULL,
  `role` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_akun`
--

INSERT INTO `tabel_akun` (`id`, `user_name`, `pass`, `is_active`, `id_user`, `role`) VALUES
(1, 'admin', '$2y$10$qoZJ6tajVALqCQMayhPLHub9Iz5Occht1LSi9cy5s9rjgoHN9J8FG', 'true', 1, 2),
(2, 'slamet', '', 'true', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tabel_anggota_gugus`
--

CREATE TABLE `tabel_anggota_gugus` (
  `id` int(11) NOT NULL,
  `id_gugus` int(11) NOT NULL,
  `nip` varchar(20) NOT NULL,
  `status` varchar(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_anggota_gugus`
--

INSERT INTO `tabel_anggota_gugus` (`id`, `id_gugus`, `nip`, `status`) VALUES
(1, 1, '200004202022023009', '');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_gugus`
--

CREATE TABLE `tabel_gugus` (
  `id` int(11) NOT NULL,
  `nama_gugus` varchar(100) NOT NULL,
  `nip_ketua` varchar(20) NOT NULL,
  `periode_awal` varchar(20) NOT NULL,
  `periode_akhir` varchar(20) NOT NULL,
  `status` varchar(5) NOT NULL,
  `kode_opd` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_gugus`
--

INSERT INTO `tabel_gugus` (`id`, `nama_gugus`, `nip_ketua`, `periode_awal`, `periode_akhir`, `status`, `kode_opd`) VALUES
(1, 'penyelesaian aplikasi gugus kerja', '200004202022023009', '2024-01-01', '2024-03-01', 'true', 1),
(2, 'fsffvds', '200004202022023009', '2024-01-13', '2024-01-19', 'true', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tabel_jabatan`
--

CREATE TABLE `tabel_jabatan` (
  `id` bigint(20) NOT NULL,
  `nama_jabatan` varchar(255) NOT NULL,
  `id_opd` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_jabatan`
--

INSERT INTO `tabel_jabatan` (`id`, `nama_jabatan`, `id_opd`) VALUES
(1, 'Staf IT', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tabel_laporan_tugas`
--

CREATE TABLE `tabel_laporan_tugas` (
  `id` bigint(20) NOT NULL,
  `id_tugas` int(11) NOT NULL,
  `id_gugus` int(11) NOT NULL,
  `link` varchar(255) NOT NULL,
  `acc` varchar(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_laporan_tugas`
--

INSERT INTO `tabel_laporan_tugas` (`id`, `id_tugas`, `id_gugus`, `link`, `acc`) VALUES
(1, 3, 1, '_20240111021123.pdf', '1');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_menu`
--

CREATE TABLE `tabel_menu` (
  `id` int(11) NOT NULL,
  `menu` varchar(100) NOT NULL,
  `ket` varchar(255) NOT NULL,
  `parent` varchar(110) NOT NULL,
  `url` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_menu`
--

INSERT INTO `tabel_menu` (`id`, `menu`, `ket`, `parent`, `url`) VALUES
(2, 'Beranda', 'menu utama', 'parent', '#'),
(3, 'Gugus Kerja', 'etc', '2', 'gugus_kerja'),
(4, 'Administrator', 'list menu administrator', 'parent', '#'),
(5, 'Menu', 'tabel master menu', '4', 'menu'),
(6, 'Akun', 'data akun', '4', 'akun_pengguna'),
(7, 'Anggota Gugus', '-', '2', 'anggota_gugus'),
(8, 'Data Pegawai', '-', '4', 'pegawai'),
(9, 'Data Tugas', '-', '2', 'tugas'),
(10, 'Data Instansi', 'list instansi / opd', '4', 'opd'),
(11, 'Data Jabatan', 'Data Master Jabatan', '4', 'jabatan'),
(12, 'Dashbord Grafik', '-', '2', 'dashbord_grafik'),
(13, 'tugas pokja', 'list tugas untuk pokjaa', '2', 'tabel_tugas_gugus'),
(14, 'Akses Menu Pengguna', '-', '4', 'tbl_user_menu'),
(15, 'Data Laporan Tugas', '-', '2', 'tabel_laporan_tugas'),
(16, 'Halaman Approval', 'approve hasil tugas', '2', 'tabel_laporan_tugas/aproval');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_opd`
--

CREATE TABLE `tabel_opd` (
  `id` int(11) NOT NULL,
  `kode_opd` varchar(100) NOT NULL,
  `nama_opd` varchar(255) NOT NULL,
  `created_date` varchar(20) NOT NULL,
  `deleted_date` varchar(20) NOT NULL,
  `deleted_by` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_opd`
--

INSERT INTO `tabel_opd` (`id`, `kode_opd`, `nama_opd`, `created_date`, `deleted_date`, `deleted_by`) VALUES
(1, 'DISKOMINFO', 'Dinas Komunikasi Dan Informatika', '2024-01-04 02-31-16', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `tabel_pegawai`
--

CREATE TABLE `tabel_pegawai` (
  `id_peg` int(11) NOT NULL,
  `nip` varchar(20) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `id_opd` int(11) NOT NULL,
  `id_jabatan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_pegawai`
--

INSERT INTO `tabel_pegawai` (`id_peg`, `nip`, `nama`, `id_opd`, `id_jabatan`) VALUES
(1, '200004202022023009', 'asdasdasd', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tabel_tugas`
--

CREATE TABLE `tabel_tugas` (
  `id` bigint(20) NOT NULL,
  `tugas` varchar(255) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `pic` int(11) NOT NULL,
  `id_opd` int(11) NOT NULL,
  `id_bidang` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_tugas`
--

INSERT INTO `tabel_tugas` (`id`, `tugas`, `deskripsi`, `pic`, `id_opd`, `id_bidang`) VALUES
(1, 'sdsddsdsd', 'dsdsdsdsdsdrewwe', 1, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tabel_tugas_gugus`
--

CREATE TABLE `tabel_tugas_gugus` (
  `id` bigint(20) NOT NULL,
  `id_gugus_tugas` bigint(20) NOT NULL,
  `tugas` varchar(255) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `pic` varchar(20) NOT NULL,
  `status` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tabel_tugas_gugus`
--

INSERT INTO `tabel_tugas_gugus` (`id`, `id_gugus_tugas`, `tugas`, `deskripsi`, `pic`, `status`) VALUES
(1, 1, 'koding aplikasi', 'melkukan koding modul a,b,c dll CRUD', '1', 'true'),
(2, 1, '1', 'gdfgdgdfg', '1', 'true'),
(3, 1, '1', 'zxzxzxz', '1', 'true');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user_menu`
--

CREATE TABLE `tbl_user_menu` (
  `id` int(11) NOT NULL,
  `id_user` int(11) NOT NULL,
  `id_menu` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user_menu`
--

INSERT INTO `tbl_user_menu` (`id`, `id_user`, `id_menu`) VALUES
(2, 2, 1),
(3, 2, 6),
(4, 2, 7),
(5, 1, 3),
(6, 1, 5),
(7, 1, 14),
(8, 1, 8),
(9, 1, 11),
(10, 1, 15),
(11, 1, 16);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `anggota_gugus`
--
ALTER TABLE `anggota_gugus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Indexes for table `tabel_akun`
--
ALTER TABLE `tabel_akun`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tabel_anggota_gugus`
--
ALTER TABLE `tabel_anggota_gugus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tabel_gugus`
--
ALTER TABLE `tabel_gugus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tabel_jabatan`
--
ALTER TABLE `tabel_jabatan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tabel_laporan_tugas`
--
ALTER TABLE `tabel_laporan_tugas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tabel_menu`
--
ALTER TABLE `tabel_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tabel_opd`
--
ALTER TABLE `tabel_opd`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tabel_pegawai`
--
ALTER TABLE `tabel_pegawai`
  ADD PRIMARY KEY (`id_peg`);

--
-- Indexes for table `tabel_tugas`
--
ALTER TABLE `tabel_tugas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tabel_tugas_gugus`
--
ALTER TABLE `tabel_tugas_gugus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tbl_user_menu`
--
ALTER TABLE `tbl_user_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `anggota_gugus`
--
ALTER TABLE `anggota_gugus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tabel_akun`
--
ALTER TABLE `tabel_akun`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tabel_anggota_gugus`
--
ALTER TABLE `tabel_anggota_gugus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tabel_gugus`
--
ALTER TABLE `tabel_gugus`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `tabel_jabatan`
--
ALTER TABLE `tabel_jabatan`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tabel_laporan_tugas`
--
ALTER TABLE `tabel_laporan_tugas`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tabel_menu`
--
ALTER TABLE `tabel_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `tabel_opd`
--
ALTER TABLE `tabel_opd`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tabel_pegawai`
--
ALTER TABLE `tabel_pegawai`
  MODIFY `id_peg` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tabel_tugas`
--
ALTER TABLE `tabel_tugas`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `tabel_tugas_gugus`
--
ALTER TABLE `tabel_tugas_gugus`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `tbl_user_menu`
--
ALTER TABLE `tbl_user_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
