<div class="row">
	<div class="col-md-12">
		<div class="box box-success">
			<div class="box-header">
				<div class="box-title">
                    <b><i class="fa fa-eye"></i> Detail Data Tabel_atasan_bawahan</b>
                </div>
                <div class="box-tools pull-right">
                    <button type="button" class="toggle-expand-btn btn btn-default btn-sm"><i class="fa fa-expand"></i></button>
                </div>
			</div>
			<div class="box-body">
				<div style="padding: 15px;">
					<table class="table table-striped">
						<tr>
							<td width="20%"><b>Id Atasan</b></td>
							<td><?= $id_atasan; ?></td>
						</tr>
						<tr>
							<td width="20%"><b>Id Staff</b></td>
							<td><?= $id_staff; ?></td>
						</tr>
						<tr>
							<td width="20%"><b>Id Opd</b></td>
							<td><?= $id_opd; ?></td>
						</tr>
					</table>
					<a href="<?= site_url('tabel_atasan_bawahan') ?>" class="btn btn-danger pull-right">
						<i class="fa fa-sign-out"></i> Kembali
					</a>
				</div>
			</div>
		</div>
	</div>
</div>