<div class="row">
	<div class="col-md-12">
		<div class="box box-success">
			<div class="box-header">
				<div class="box-title">
                    <b><i class="fa fa-eye"></i> Detail Data Tbl_pelayanan</b>
                </div>
                <div class="box-tools pull-right">
                    <button type="button" class="toggle-expand-btn btn btn-default btn-sm"><i class="fa fa-expand"></i></button>
                </div>
			</div>
			<div class="box-body">
				<div style="padding: 15px;">
					<table class="table table-striped">
						<tr>
							<td width="20%"><b>Id Layanan</b></td>
							<td><?= $id_layanan; ?></td>
						</tr>
						<tr>
							<td width="20%"><b>Id Instansi</b></td>
							<td><?= $id_instansi; ?></td>
						</tr>
						<tr>
							<td width="20%"><b>Nm Layanan</b></td>
							<td><?= $nm_layanan; ?></td>
						</tr>
						<tr>
							<td width="20%"><b>Parent</b></td>
							<td><?= $parent; ?></td>
						</tr>
						<tr>
							<td width="20%"><b>Kategori</b></td>
							<td><?= $kategori; ?></td>
						</tr>
					</table>
					<a href="<?= site_url('tbl_pelayanan') ?>" class="btn btn-danger pull-right">
						<i class="fa fa-sign-out"></i> Kembali
					</a>
				</div>
			</div>
		</div>
	</div>
</div>