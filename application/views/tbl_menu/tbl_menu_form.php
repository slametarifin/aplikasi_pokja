<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.0/dist/jquery.slim.min.js"></script>
<!-- select2 form-select --><div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header">
                <div class="box-title">
                    <b> <?= $button ?> Data Tbl_menu</b>
                </div> 
            </div>
            <div class="box-body">
                <form style="padding: 15px;" action="<?= $action; ?>" method="POST" enctype="multipart/form-data">
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="varchar">Menu</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="menu" id="menu" placeholder="Menu" value="<?= $menu; ?>" />
                                <?= form_error('menu') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="varchar">Url</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="url" id="url" placeholder="Url" value="<?= $url; ?>" />
                                <?= form_error('url') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="varchar">ket</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="ket" id="ket" placeholder="ket" value="<?= $ket; ?>" />
                                <?= form_error('ket') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="int">Parent</label>
                            <div class="col-md-6">
                                <select name="parent" class="form-control select2 form-select">
                                    <option value="parent">Parent</option>
                                    <?php 
                                        $menu=$this->Tbl_menu_model->get_all();
                                        foreach($menu as $mn){?>
                                            <option value="<?=$mn->id?>" <?php if($parent==$mn->id){?>selected <?php }?>><?=$mn->menu?></option>
                                        <?php }
                                    ?>
                                </select>
                                <!-- <input type="text" class="form-control" name="parent" id="parent" placeholder="Parent" value="<?= $parent; ?>" /> -->
                                <?= form_error('parent') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-2">
                              
                                <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> <?= $button ?></button>
                                <a href="<?= site_url('tbl_menu') ?>" class="btn btn-danger"><i class="fa fa-sign-out"></i> Kembali</a>
                            </div>
                        </div>
                    </div>
				</form>
            </div>
        </div>
    </div>
</div>