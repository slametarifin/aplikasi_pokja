<section class="section profile">
  <div class="row">
    <div class="col-xl-4"> 
      <div class="card">
        <div class="card-body profile-card pt-4 d-flex flex-column align-items-center"> 
          <img src="<?=site_url('./')?>assets/img/admin_foto.png" alt="Profile" class="rounded-circle">
          <h2><?=$nama?></h2>
          <h3><?=$nama_jabatan?></h3>
          <div class="social-links mt-2" style="display:none;">
            <a href="#" class="twitter"><i class="bi bi-twitter"></i></a>
            <a href="#" class="facebook"><i class="bi bi-facebook"></i></a>
            <a href="#" class="instagram"><i class="bi bi-instagram"></i></a>
            <a href="#" class="linkedin"><i class="bi bi-linkedin"></i></a>
          </div>
        </div>
      </div>
    </div>
    <div class="col-xl-8"> 
      <div class="card">
        <div class="card-body profile-card pt-4 d-flex flex-column">  <!--align-items-center-->
          <h5 class="card-title">Bordered Tabs Justified</h5> 
          <ul class="nav nav-tabs nav-tabs-bordered d-flex" id="borderedTabJustified" role="tablist">
            <li class="nav-item flex-fill" role="presentation">
              <button class="nav-link w-100 active" id="home-tab" data-bs-toggle="tab" data-bs-target="#bordered-justified-home" type="button" role="tab" aria-controls="home" aria-selected="true">Akun</button>
            </li>
            <li class="nav-item flex-fill" role="presentation">
              <button class="nav-link w-100" id="profile-tab" data-bs-toggle="tab" data-bs-target="#bordered-justified-profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Profile</button>
            </li>
            <li class="nav-item flex-fill" role="presentation">
              <button class="nav-link w-100" id="contact-tab" data-bs-toggle="tab" data-bs-target="#bordered-justified-contact" type="button" role="tab" aria-controls="contact" aria-selected="false">Pekerjaan&nbsp;<span class="badge bg-primary badge-number">4</span></button>
            </li>
          </ul>
          <div class="tab-content pt-2" id="borderedTabJustifiedContent">
            <div class="tab-pane fade show active" id="bordered-justified-home" role="tabpanel" aria-labelledby="home-tab"> 
              <form method="POST" action="<?=site_url('login/update_pass')?>">
                <div class="row mb-3">
                  <label for="inputEmail3" class="col-sm-2 col-form-label">Nama Pengguna</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="user_name" id="inputText">
                  </div>
                </div> 
                <div class="row mb-3">
                  <label for="inputPassword3" class="col-sm-2 col-form-label">Password Lama</label>
                  <div class="col-sm-10">
                    <input type="password" class="form-control" name="password_lama" id="inputPassword">
                  </div>
                </div>  
                <div class="row mb-3">
                  <label for="inputPassword3" class="col-sm-2 col-form-label">Password Baru</label>
                  <div class="col-sm-10">
                    <input type="password" class="form-control" name="password_baru" id="inputPassword">
                  </div>
                </div>  
                <div class="text-center">
                  <button type="submit" class="btn btn-sm btn-primary col-md-12">Atur Ulang Akses Akun</button>
                  <!-- <button type="reset" class="btn btn-secondary">Reset</button> -->
                </div>
              </form><hr>
              <a class="btn btn-sm btn-warning col-md-12" href="<?=site_url('login/resset_akun_mandiri')?>">RESET AKUN PASSWORD</a><hr>
            </div>
            <div class="tab-pane fade" id="bordered-justified-profile" role="tabpanel" aria-labelledby="profile-tab">
              <?php 
                $data_peg= $this->db->query("
                  select b.*,(select nama_opd from tabel_opd where tabel_opd.id=b.id_opd)opd,(select nama_jabatan from tabel_jabatan where tabel_jabatan.id=b.id_jabatan)jabatan from tabel_pegawai b  
                  where b.id_peg=".$this->session->userdata('id_user')."
                  ")->row();
              ?>
              <table width="100%">
                <tr>
                  <td width="30%">Nama Lengkap</td>
                  <td>: <?=$data_peg->nama?></td>
                </tr>
                <tr>
                  <td width="30%">NIP</td>
                  <td>: <?=$data_peg->nip?></td>
                </tr>
                <tr>
                  <td width="30%">Instansi OPD</td>
                  <td>: <?=$data_peg->opd?></td>
                </tr>
                <tr>
                  <td width="30%">Jabatan</td>
                  <td>: <?=$data_peg->jabatan?></td>
                </tr>
              </table>
            </div>
            <div class="tab-pane fade" id="bordered-justified-contact" role="tabpanel" aria-labelledby="contact-tab">
              <?php 
                $data_gugus= $this->db->query("
                  select a.*,c.nama_gugus,b.nama,(select nama from tabel_pegawai d where d.nip=c.nip_ketua) nama_ketua from tabel_tugas_gugus a
                  join tabel_pegawai b on a.pic = b.id_peg
                  join tabel_gugus c on c.id = a.id_gugus_tugas
                  where a.pic=".$this->session->userdata('id_user')."
                  ")->result();
                foreach($data_gugus as $dg){?>
                  <table width="100%" id="tabel_gugus">
                    <tr>
                      <td width="30%">Gugus Kerja</td>
                      <td>: <?=$dg->nama_gugus?></td>
                    </tr>
                    <tr>
                      <td width="30%">Ketua Gugus Kerja</td>
                      <td>: <?=$dg->nama_ketua?></td>
                    </tr>
                  </table><hr>
                <?php }
              ?>
            </div>
          </div> 
        </div>
      </div>
    </div> 
  </div>
</div>
</section>
<div class="col-lg-8" style="display:none;">
  <div class="card">
    <div class="card-body">
      <h5 class="card-title">Bordered Tabs Justified</h5> 
      <ul class="nav nav-tabs nav-tabs-bordered d-flex" id="borderedTabJustified" role="tablist">
        <li class="nav-item flex-fill" role="presentation">
          <button class="nav-link w-100 active" id="home-tab" data-bs-toggle="tab" data-bs-target="#bordered-justified-home" type="button" role="tab" aria-controls="home" aria-selected="true">Home</button>
        </li>
        <li class="nav-item flex-fill" role="presentation">
          <button class="nav-link w-100" id="profile-tab" data-bs-toggle="tab" data-bs-target="#bordered-justified-profile" type="button" role="tab" aria-controls="profile" aria-selected="false">Profile</button>
        </li>
        <li class="nav-item flex-fill" role="presentation">
          <button class="nav-link w-100" id="contact-tab" data-bs-toggle="tab" data-bs-target="#bordered-justified-contact" type="button" role="tab" aria-controls="contact" aria-selected="false">Contact</button>
        </li>
      </ul>
      <div class="tab-content pt-2" id="borderedTabJustifiedContent">
        <div class="tab-pane fade show active" id="bordered-justified-home" role="tabpanel" aria-labelledby="home-tab">
          Sunt est soluta temporibus accusantium neque nam maiores cumque temporibus. Tempora libero non est unde veniam est qui dolor. Ut sunt iure rerum quae quisquam autem eveniet perspiciatis odit. Fuga sequi sed ea saepe at unde.
        </div>
        <div class="tab-pane fade" id="bordered-justified-profile" role="tabpanel" aria-labelledby="profile-tab">
          Nesciunt totam et. Consequuntur magnam aliquid eos nulla dolor iure eos quia. Accusantium distinctio omnis et atque fugiat. Itaque doloremque aliquid sint quasi quia distinctio similique. Voluptate nihil recusandae mollitia dolores. Ut laboriosam voluptatum dicta.
        </div>
        <div class="tab-pane fade" id="bordered-justified-contact" role="tabpanel" aria-labelledby="contact-tab">
          Saepe animi et soluta ad odit soluta sunt. Nihil quos omnis animi debitis cumque. Accusantium quibusdam perspiciatis qui qui omnis magnam. Officiis accusamus impedit molestias nostrum veniam. Qui amet ipsum iure. Dignissimos fuga tempore dolor.
        </div>
      </div> 
    </div>
  </div>
</div>