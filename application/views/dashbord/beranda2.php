<?php
// for($a=1;$a<13;$a++){
// $data=$this->db->query("select RIGHT(LEFT(tabel_gugus.periode_awal,7),2) as bulan from tabel_gugus where RIGHT(LEFT(tabel_gugus.periode_awal,7),2)=".$a)->result();
// print_r($data);echo "<br>";
// }
// die();
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Kab.Cilacap</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.gstatic.com" rel="preconnect">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- Vendor CSS Files -->
  <link href="<?=site_url()?>./assets/aplikasi_pokja/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?=site_url()?>./assets/aplikasi_pokja/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="<?=site_url()?>./assets/aplikasi_pokja/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="<?=site_url()?>./assets/aplikasi_pokja/quill/quill.snow.css" rel="stylesheet">
  <link href="<?=site_url()?>./assets/aplikasi_pokja/quill/quill.bubble.css" rel="stylesheet">
  <link href="<?=site_url()?>./assets/aplikasi_pokja/remixicon/remixicon.css" rel="stylesheet">
  <link href="<?=site_url()?>./assets/aplikasi_pokja/simple-datatables/style.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="<?=site_url()?>./assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: NiceAdmin
  * Updated: Nov 17 2023 with Bootstrap v5.3.2
  * Template URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>
    <section class="section">
      <div class="row">
        <div class="col-lg-6">
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Grafik Penyelesaian Tugas Gugus Kerja</h5>

              <!-- Pie Chart -->
              <div id="pieChart"></div>
              <?php 
              $jml_tugas_gugus=$this->db->query(
                "select id from tabel_tugas_gugus"
              )->num_rows();
              $jml_tugas_gugus_ok=$this->db->query(
                "select tabel_tugas_gugus.id_gugus_tugas from tabel_tugas_gugus 
                join tabel_laporan_tugas 
                on tabel_laporan_tugas.id_gugus=tabel_tugas_gugus.id_gugus_tugas
                GROUP BY id_gugus_tugas
                "
              )->num_rows();
              $jml_tugas_gugus_okk=$this->db->query(
                "select tabel_tugas_gugus.id_gugus_tugas from tabel_tugas_gugus 
                join tabel_laporan_tugas 
                on tabel_laporan_tugas.id_gugus=tabel_tugas_gugus.id_gugus_tugas
                GROUP BY id_gugus_tugas
                "
              )->row();  
              ?>
              <script>
                document.addEventListener("DOMContentLoaded", () => {
                  new ApexCharts(document.querySelector("#pieChart"), {
                    series: [<?=$jml_tugas_gugus_ok?>, <?=$jml_tugas_gugus-$jml_tugas_gugus_ok?>],
                    chart: {
                      height: 350,
                      type: 'pie',
                      toolbar: {
                        show: true
                      }
                    },
                    labels: ['Sudah Diselesaikan', 'Dalam Proses']
                  }).render();
                });
              </script>
              <!-- End Pie Chart -->

            </div>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Data Tugas Gugus Kerja</h5>

              <!-- Column Chart -->
              <div id="columnChart"></div>

              <script>
                document.addEventListener("DOMContentLoaded", () => {
                  new ApexCharts(document.querySelector("#columnChart"), {
                    series: [{
                      name: 'Tugas Gugus Kerja',
                      data: [
                        <?php  
                        for($a=1;$a<13;$a++){
                        $data=$this->db->query("select * from tabel_tugas_gugus where bulan=".$a)->num_rows();
                        echo $data.",";}
                        ?>]
                        // 4, 5, 7, 5, 1, 3, 3, 6, 8, 3, 6, 8]
                    },{
                      name: 'Tugas Selesai',
                      data: [
                        <?php  
                        for($a=1;$a<13;$a++){
                        $data=$this->db->query("
                          SELECT tabel_tugas_gugus.bulan FROM `tabel_laporan_tugas`
                          join tabel_tugas_gugus on tabel_tugas_gugus.id = tabel_laporan_tugas.id_tugas
                          where bulan=".$a."
                        ")->num_rows();
                        echo $data.",";}
                        ?>]
                        //76, 85, 101, 98, 87, 105, 91, 114, 94]
                    }, {
                      name: 'Tugas Dalam Proses',
                      data: [
                        <?php  
                        for($a=1;$a<13;$a++){
                        $data=$this->db->query("
                          SELECT tabel_tugas_gugus.bulan FROM `tabel_laporan_tugas`
                          join tabel_tugas_gugus on tabel_tugas_gugus.id = tabel_laporan_tugas.id_tugas
                          where bulan=".$a."
                        ")->num_rows();
                        $data2=$this->db->query("select * from tabel_tugas_gugus where bulan=".$a)->num_rows();
                        //echo $data.",";
                        echo $data2-$data.",";}
                        ?>]
                        //[35, 41, 36, 26, 45, 48, 52, 53, 41]
                    }
                    ],
                    chart: {
                      type: 'bar',
                      height: 350
                    },
                    plotOptions: {
                      bar: {
                        horizontal: false,
                        columnWidth: '55%',
                        endingShape: 'rounded'
                      },
                    },
                    dataLabels: {
                      enabled: false
                    },
                    stroke: {
                      show: true,
                      width: 2,
                      colors: ['transparent']
                    },
                    xaxis: {
                      categories: ['Jan', 'Feb','Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct','Nov','Des'],
                    },
                    yaxis: {
                      title: {
                        text: 'Jumlah Tugas'
                      }
                    },
                    fill: {
                      opacity: 1
                    },
                    tooltip: {
                      y: {
                        formatter: function(val) {
                          return val + " Tugas"
                        }
                      }
                    }
                  }).render();
                });
              </script>
              <!-- End Column Chart -->

            </div>
          </div>
        </div>
        <div class="col-lg-6">
          <div class="card">
            <div class="card-body">
              <h5 class="card-title">Data Tugas Gugus Kerja</h5>

              <!-- Column Chart -->
              <div id="GugusChart"></div>

              <script>
                document.addEventListener("DOMContentLoaded", () => {
                  new ApexCharts(document.querySelector("#GugusChart"), {
                    series: [{
                      name: 'Gugus Kerja',
                      data: [
                        <?php  
                        for($a=1;$a<13;$a++){
                          $data=$this->db->query("select RIGHT(LEFT(tabel_gugus.periode_awal,7),2) as bulan from tabel_gugus where RIGHT(LEFT(tabel_gugus.periode_awal,7),2)=".$a)->num_rows(); 
                          echo $data.",";}
                        ?>] 
                    }, 
                    ],
                    chart: {
                      type: 'bar',
                      height: 350
                    },
                    plotOptions: {
                      bar: {
                        horizontal: false,
                        columnWidth: '55%',
                        endingShape: 'rounded'
                      },
                    },
                    dataLabels: {
                      enabled: false
                    },
                    stroke: {
                      show: true,
                      width: 2,
                      colors: ['transparent']
                    },
                    xaxis: {
                      categories: ['Jan', 'Feb','Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct','Nov','Des'],
                    },
                    yaxis: {
                      title: {
                        text: 'Jumlah Gugus'
                      }
                    },
                    fill: {
                      opacity: 1
                    },
                    tooltip: {
                      y: {
                        formatter: function(val) {
                          return val + " Gugus Kerja"
                        }
                      }
                    }
                  }).render();
                });
              </script>
              <!-- End Column Chart -->

            </div>
          </div>
        </div>
</div>
</section>
</body>
</html>