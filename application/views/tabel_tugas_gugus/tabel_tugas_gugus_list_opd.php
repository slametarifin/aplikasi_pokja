<div class="row"> 
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <div class="box-title">
                    <b> Data tabel_tugas_gugus</b>
                </div> 
            </div>
            <div class="box-body">
                <!-- <a href="<?= site_url('tabel_tugas_gugus/create') ?>" class="btn btn-primary" style="margin-left : 15px"><i class="fa fa-plus"></i> Tambah Data</a> -->
                <div class="table-responsive" style="padding: 15px">
                    <table class="table table-bordered table-striped table-hover" width="100%" id="mytable">
                        <thead>
                            <tr>
                                <th width="5%">No</th>
                                <th>Instansi OPD</th>
                                <th>Gugus Tugas</th>
                                <th>Tugas</th>
                                <th>Deskripsi</th>
                                <th>PIC<br>Tugas</th>
                                <th>Status</th>
                                <th width="15%">Aksi</th>
                            </tr>
                        </thead>    
                    </table>

                    <script src="<?= base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
                    <script type="text/javascript">
                        $(document).ready(function() {
                            $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
                            {
                                return {
                                    "iStart": oSettings._iDisplayStart,
                                    "iEnd": oSettings.fnDisplayEnd(),
                                    "iLength": oSettings._iDisplayLength,
                                    "iTotal": oSettings.fnRecordsTotal(),
                                    "iFilteredTotal": oSettings.fnRecordsDisplay(),
                                    "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                                    "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                                };
                            };

                            var t = $("#mytable").dataTable({
                                "processing"  : true,
                                "serverSide"  : true,
                                "oLanguage"   : { sProcessing : "Loading. . ." },
                                "ajax"        : { "url" : "<?= site_url('tabel_tugas_gugus/json_opd/'.$id_opd) ?>", "type": "POST"},
                                "columns"     : [
                                    { 
                                        "data": "id",
                                        "orderable": false,
                                        "className" : "text-center"
                                    },
                                    {
                                        "data": "nama_opd"
                                    },
                                    {
                                        "data": "nama_gugus"
                                    },
                                    {
                                        "data": "nm_tugas"
                                    },
                                    {
                                        "data": "deskripsi"
                                    },
                                    {
                                        "data": "nama"
                                    },
                                    {
                                        "data": "status",
                                            render: function (data, type, row) {
                                                if(data.status=="true"){
                                                    return "<label class='lbl lbl-success'>Sudah Selesai</label>"
                                                }else{
                                                    return "<label class='lbl lbl-warning'>Belum Selesai</label>"
                                                }
                                            }
                                    },
                                    {
                                        "data" : "action",
                                        "orderable": false,
                                        "className" : "text-center"
                                    }
                                ],
                                order: [[0, 'desc']],
                                rowCallback: function(row, data, iDisplayIndex) {
                                    var info = this.fnPagingInfo();
                                    var page = info.iPage;
                                    var length = info.iLength;
                                    var index = page * length + (iDisplayIndex + 1);
                                    $('td:eq(0)', row).html(index);
                                }
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>
<?php // swal_delete("#mytable",".hapus") ?>
        <?= anchor(site_url('tabel_tugas_gugus/excel'), '<i class="fa fa-file-excel-o" aria-hidden="true"></i> Export Excel', 'class="btn btn-success"'); ?>