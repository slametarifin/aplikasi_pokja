<div class="row">
	<div class="col-md-12">
		<div class="box box-success">
			<div class="box-header">
				<div class="box-title">
                    <b><i class="fa fa-eye"></i> Detail Data Tabel_tugas</b>
                </div>
                <div class="box-tools pull-right">
                    <button type="button" class="toggle-expand-btn btn btn-default btn-sm"><i class="fa fa-expand"></i></button>
                </div>
			</div>
			<div class="box-body">
				<div style="padding: 15px;">
					<table class="table table-striped">
						<tr>
							<td width="20%"><b>Tugas</b></td>
							<td><?= $tugas; ?></td>
						</tr>
						<tr>
							<td width="20%"><b>Deskripsi</b></td>
							<td><?= $deskripsi; ?></td>
						</tr>
						<tr>
							<td width="20%"><b>Pic</b></td>
							<td><?= $pic; ?></td>
						</tr>
						<tr>
							<td width="20%"><b>Id Opd</b></td>
							<td><?= $id_opd; ?></td>
						</tr>
						<tr>
							<td width="20%"><b>Id Bidang</b></td>
							<td><?= $id_bidang; ?></td>
						</tr>
					</table>
					<a href="<?= site_url('tabel_tugas') ?>" class="btn btn-danger pull-right">
						<i class="fa fa-sign-out"></i> Kembali
					</a>
				</div>
			</div>
		</div>
	</div>
</div>