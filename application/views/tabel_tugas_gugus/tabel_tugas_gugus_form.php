  <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.0/dist/jquery.slim.min.js"></script>
  <div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header">
                <div class="box-title">
                    <b><i class="fa fa-tasks"></i> <?= $button ?> Data Tabel_tugas_gugus</b>
                </div>
                <div class="box-tools pull-right">
                    <button type="button" class="toggle-expand-btn btn btn-default btn-sm"><i class="fa fa-expand"></i></button>
                </div>
            </div>
            <div class="box-body">
                <form style="padding: 15px;" action="<?= $action; ?>" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="bigint">Gugus Tugas</label>
                            <div class="col-md-6">
                                <select name="id_gugus_tugas" class="form-control select2 form-select">
                                    <option value="-">pilih tugas</option>
                                    <?php 
                                    $data_tugas=$this->db->query("select * from tabel_gugus")->result();
                                    foreach($data_tugas as $dt){?>
                                        <option value="<?=$dt->id?>" <?php if($id_gugus_tugas==$dt->id){?> selected <?php }?>><?=$dt->nama_gugus?></option>
                                    <?php }
                                    ?>
                                </select>
                                <!-- <input type="text" class="form-control" name="id_gugus_tugas" id="id_gugus_tugas" placeholder="Id Gugus Tugas" value="<?= $id_gugus_tugas; ?>" /> -->
                                <?= form_error('id_gugus_tugas') ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="varchar">Tugas</label>
                            <div class="col-md-6">
                                <!-- <input type="text" class="form-control" name="tugas" id="tugas" placeholder="Tugas" value="<?= $tugas; ?>" /> -->
                                <select name="tugas" class="form-control select2 form-select">
                                    <option value="-">pilih tugas</option>
                                    <?php 
                                    $data_tugas=$this->db->query("select * from tabel_tugas")->result();
                                    foreach($data_tugas as $dt){?>
                                        <option value="<?=$dt->id?>" <?php if($tugas==$dt->id){?> selected <?php }?>><?=$dt->tugas?></option>
                                    <?php }
                                    ?>
                                </select>
                                <?= form_error('tugas') ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="varchar">Deskripsi</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="deskripsi" id="deskripsi" placeholder="Deskripsi" value="<?= $deskripsi; ?>" />
                                <?= form_error('deskripsi') ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="varchar">OPD / Instansi</label>
                            <div class="col-md-6"> 
                                <select name="kode_opd" id="kode_opd" class="form-control select2 form-select">
                                    <option value="-">pilih data</option>
                                    <?php 
                                    $opd=$this->db->query("
                                        select id,nama_opd from tabel_opd
                                        ")->result(); 
                                    foreach($opd as $pg){?>
                                        <option value="<?=$pg->id?>" <?php if($kode_opd==$pg->id){?>selected <?php }?>><?=$pg->nama_opd?></option>
                                    <?php }?>
                                </select>
                                <?= form_error('pic') ?>
                            </div>
                        </div>
                    </div> 
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="varchar">Jabatan</label>
                            <div class="col-md-6"> 
                                <select name="id_jabatan" id="kode_jabatan" class="form-control select2 form-select">
                                    <option value="-">pilih data</option> 
                                </select>
                                <?= form_error('pic') ?>
                            </div>
                        </div>
                    </div> 
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="varchar" <?php if($id_eselon=="i b"){?>selected <?php }?>>Eselon</label>
                            <div class="col-md-6"> 
                                <select name="id_eselon" id="kode_eselon" class="form-control select2 form-select">
                                    <option value="-">pilih data</option> 
                                    <option value="i b" <?php if($id_eselon=="i b"){?>selected <?php }?>>Eselon I B</option>
                                    <option value="i a" <?php if($id_eselon=="i a"){?>selected <?php }?>>Eselon I A</option>
                                    <option value="ii b" <?php if($id_eselon=="ii b"){?>selected <?php }?>>Eselon II B</option>
                                    <option value="ii a" <?php if($id_eselon=="ii a"){?>selected <?php }?>>Eselon II A</option>
                                    <option value="iii b" <?php if($id_eselon=="iii b"){?>selected <?php }?>>Eselon III B</option>
                                    <option value="iii a" <?php if($id_eselon=="iii a"){?>selected <?php }?>>Eselon III A</option>
                                    <option value="iv b" <?php if($id_eselon=="iv b"){?>selected <?php }?>>Eselon IV B</option>
                                    <option value="iv a" <?php if($id_eselon=="iv a"){?>selected <?php }?>>Eselon IV A</option>
                                </select>
                                <?= form_error('pic') ?>
                            </div>
                        </div>
                    </div> 
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="varchar">PIC Tugas</label>
                            <div class="col-md-6"> 
                                <select name="pic" id="cari_nama" class="form-control select2 form-select">
                                    <option value="-">pilih data</option> 
                                </select>
                                <?= form_error('pic') ?>
                            </div>
                        </div>
                    </div> 
                    <div class="form-group">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-2"> 
                                <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> <?= $button ?></button>
                                <a href="<?= site_url('tabel_gugus') ?>" class="btn btn-danger"><i class="fa fa-sign-out"></i> Kembali</a>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<script src="<?= base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
<script type="text/javascript">
$(document).ready(function() {
    $('#kode_opd').change(function() { 
        var kode_opd = $(this).val(); 
        var kode_jabatan = $('#kode_jabatan').val(); 
        var kode_eselon = $('#kode_eselon').val(); 
        // $('#cari_nama').remove().end();
        $('#cari_nama').children().remove();
        $('#kode_jabatan').children().remove();
        // var kode_jabatan = $(this).val(); 
        $.ajax({
            type: "POST",
            data: {
              kode_opd: kode_opd, 
            },
            url: "<?php echo site_url('tabel_pegawai/get_jabatan') ?>",
            dataType: "json",
            success: function(hasil) {
                $('#kode_jabatan').append($('<option>', {
                value: "'-'",
                text: "pilih data"
              }));
            $.each(hasil, function(index, option) {
                // console.log(option.id+':'+option.nama_jabatan);
              $('#kode_jabatan').append($('<option>', {
                value: option.id,
                text: option.nama_jabatan
              }));
            }); 
            },
            error: function(hasil){ 
                console.log('data tidak ditemukan');
            },
        }); 
        if($('#kode_jabatan').val()!='-'){
            $('#cari_nama').children().remove();
            $('#cari_nama').append($('<option>', {
            value: "'-'",
            text: "pilih data"
          }));
            $.ajax({
                type: "POST",
                data: {
                  kode_opd: kode_opd, 
                  kode_jabatan: kode_jabatan,
                  kode_eselon: kode_eselon,
                },
                url: "<?php echo site_url('tabel_pegawai/get_pegawai') ?>",
                dataType: "json",
                success: function(hasil) {
                $.each(hasil, function(index, option) {
                    // console.log(option.id+':'+option.nama_jabatan); 
                  $('#cari_nama').find('option').append($('<option>', {
                    value: option.nip,
                    text: option.nama_opd+' | '+option.nip+' | '+option.nama
                  }));
                }); 
                },
                error: function(hasil){ 
                    console.log('data tidak ditemukan');
                },
            }); 
        }
    }); 
    $('#kode_eselon').change(function() { 
        var kode_opd = $('#kode_opd').val(); 
        var kode_jabatan = $('#kode_jabatan').val(); 
        var kode_eselon = $(this).val();  
        $('#cari_nama').children().remove();
        $('#cari_nama').append($('<option>', {
        value: "'-'",
        text: "pilih data"
      }));
        // $('#cari_nama').remove().end();
        $.ajax({
            type: "POST",
            data: {
              kode_opd: kode_opd, 
              kode_eselon: kode_eselon,
              kode_jabatan: kode_jabatan,
            },
            url: "<?php echo site_url('tabel_pegawai/get_pegawai') ?>",
            dataType: "json",
            success: function(hasil) {
            $.each(hasil, function(index, option) {
              $('#cari_nama').append($('<option>', {
                value: option.nip,
                text: option.nama_opd+' | '+option.nip+' | '+option.nama
              }));
            }); 
            },
            error: function(hasil){ 
                console.log('data tidak ditemukan');
            },
        }); 
    });
    $('#kode_jabatan').change(function() { 
        var kode_opd = $('#kode_opd').val(); 
        var kode_eselon = $('#kode_eselon').val(); 
        var kode_jabatan = $(this).val();  
        $('#cari_nama').children().remove();
        $('#cari_nama').append($('<option>', {
        value: "'-'",
        text: "pilih data"
      }));
        // $('#cari_nama').remove().end();
        $.ajax({
            type: "POST",
            data: {
              kode_opd: kode_opd, 
              kode_eselon: kode_eselon,
              kode_jabatan: kode_jabatan,
            },
            url: "<?php echo site_url('tabel_pegawai/get_pegawai') ?>",
            dataType: "json",
            success: function(hasil) {
            $.each(hasil, function(index, option) {
              $('#cari_nama').append($('<option>', {
                value: option.nip,
                text: option.nama_opd+' | '+option.nip+' | '+option.nama
              }));
            }); 
            },
            error: function(hasil){ 
                console.log('data tidak ditemukan');
            },
        }); 
    }); 
});
</script>