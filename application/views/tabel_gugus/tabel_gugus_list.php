<div class="row"> 
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <div class="box-title">
                    <b><i class="fa fa-list-alt"></i> Data Tabel_gugus</b>
                </div>
                <div class="box-tools pull-right">
                    <button type="button" class="toggle-expand-btn btn btn-default btn-sm"><i class="fa fa-expand"></i></button>
                </div>
            </div>
            <div class="box-body">
                <a href="<?= site_url('tabel_gugus/create') ?>" class="btn btn-primary col-md-2" style="display: <?=$display?>;margin-left : 15px"><i class="fa fa-plus"></i> Tambah Data</a>
                <div class="table-responsive" style="padding: 15px">
                    <table class="table table-bordered table-striped table-hover" width="100%" id="mytable">
                        <thead>
                            <tr>
                                <th width="5%">No</th>
								<th>Nama Gugus</th>
								<th>Nip Ketua</th>
								<th>Periode Awal</th>
								<th>Periode Akhir</th>
								<th>Status</th>
								<th>Kode Opd</th>
								<th width="15%">Aksi</th>
                            </tr>
                        </thead>    
                    </table>

                    <script src="<?= base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
                    <script type="text/javascript">
                        $(document).ready(function() {
                            $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
                            {
                                return {
                                    "iStart": oSettings._iDisplayStart,
                                    "iEnd": oSettings.fnDisplayEnd(),
                                    "iLength": oSettings._iDisplayLength,
                                    "iTotal": oSettings.fnRecordsTotal(),
                                    "iFilteredTotal": oSettings.fnRecordsDisplay(),
                                    "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                                    "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                                };
                            };

                            var t = $("#mytable").dataTable({
                                "processing"  : true,
                                "serverSide"  : true,
                                "oLanguage"   : { sProcessing : "Loading. . ." },
                                "ajax"        : { "url" : "<?= site_url('tabel_gugus/json') ?>", "type": "POST"},
                                "columns"     : [
                                    {
                                        "data": "id",
                                        "orderable": false,
                                        "className" : "text-center"
                                    },
									{
										"data": "nama_gugus"
									},
									{
										"data": "nip_ketua"
									},
									{
										"data": "periode_awal"
									},
									{
										"data": "periode_akhir"
									},
									{
										"data": "status"
									},
									{
										"data": "kode_opd"
									},
                                    {
                                        "data" : "action",
                                        "orderable": false,
                                        "className" : "text-center",
                                            render: function (data, type, row) {
                                                var akses="<?=$akses?>";
                                                if(akses==2||akses==1){
                                                return row.action;
                                                }else{
                                                return "tidak ada akses";
                                                }
                                            }
                                    }
                                ],
                                order: [[0, 'desc']],
                                rowCallback: function(row, data, iDisplayIndex) {
                                    var info = this.fnPagingInfo();
                                    var page = info.iPage;
                                    var length = info.iLength;
                                    var index = page * length + (iDisplayIndex + 1);
                                    $('td:eq(0)', row).html(index);
                                }
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>
<?php // swal_delete("#mytable",".hapus") ?>
		<?= anchor(site_url('tabel_gugus/excel'), '<i class="fa fa-file-excel-o" aria-hidden="true"></i> Export Excel', 'class="btn btn-success"'); ?>