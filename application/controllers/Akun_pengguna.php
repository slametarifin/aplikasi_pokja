<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Akun_pengguna extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Akun_pengguna_model');
        $this->load->library('form_validation');
		$this->load->library('datatables');
    }

    public function index()
    {
        $data = array(
            'akun_pengguna_data' => $this->Akun_pengguna_model->get_all(),
        );
        $this->template->load('template_antrian','akun_pengguna/akun_pengguna_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Akun_pengguna_model->get_by_id($id);
        if ($row) {
            $data = array(
				'id' => $row->id,
				'user_name' => $row->user_name,
				'pass' => $row->pass,
				'id_user' => $row->id_user, 
			);
            $this->template->load('template_antrian','akun_pengguna/akun_pengguna_read', $data);
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('akun_pengguna'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Tambah',
            'action' => site_url('akun_pengguna/create_action'),
			'id' => set_value('id'),
			'user_name' => set_value('user_name'),
			'pass' => set_value('pass'),
			'id_user' => set_value('id_user'), 
            'role' => set_value('role'), 
		);
        $this->template->load('template_antrian','akun_pengguna/akun_pengguna_form', $data);
    }
    public function json() {
        header('Content-Type: application/json');
        echo $this->Akun_pengguna_model->json();
    }
    
    public function create_action() 
    { 
            $data = array(
				'user_name' => $this->input->post('user_name',TRUE),
				'pass' => password_hash($this->input->post('pass',TRUE),PASSWORD_DEFAULT),
				'id_user' => $this->input->post('id_user',TRUE), 
                'role' => $this->input->post('role',TRUE), 
			);  
            $this->Akun_pengguna_model->insert($data);
            $this->session->set_flashdata('success', 'Berhasil Tambah Data');
            redirect(site_url('akun_pengguna'));
        }
    
    public function update($id) 
    {
        $row = $this->Akun_pengguna_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Ubah',
                'action' => site_url('akun_pengguna/update_action/'.$id),
				'id' => set_value('id', $row->id),
				'user_name' => set_value('user_name', $row->user_name),
				'pass' => set_value('pass', $row->pass),
				'id_user' => set_value('id_user', $row->id_user), 
                'role' => set_value('role', $row->role), 
			);
            // print_r($data);die();
            $this->template->load('template_antrian','akun_pengguna/akun_pengguna_form', $data);
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('akun_pengguna'));
        }
    }
    
    public function update_action($id) 
    { 
            $data = array(
                'user_name' => $this->input->post('user_name',TRUE),
                'pass' => $this->input->post('pass',TRUE),
                'id_user' => $this->input->post('id_user',TRUE), 
                'role' => $this->input->post('role',TRUE), 
            );

            $this->Akun_pengguna_model->update($id, $data);
            $this->session->set_flashdata('success', 'Berhasil Edit Data');
            redirect(site_url('akun_pengguna')); 
    }
    
    public function reset_pass($id) 
    { 
            $data = array(
                'pass' => password_hash('12345',PASSWORD_DEFAULT),
            );

            $this->Akun_pengguna_model->update($id, $data);
            $this->session->set_flashdata('success', 'Berhasil Edit Data');
            redirect(site_url('akun_pengguna')); 
    }
    
    public function reset_passs($id) 
    {
            $row=$this->db->query("select id_peg from tabel_pegawai where id_peg=".$id)->row();
            $data = array(
                'pass' => password_hash('12345',PASSWORD_DEFAULT),
            );

            $this->Akun_pengguna_model->update_($id, $data);
            $this->session->set_flashdata('success', 'Berhasil Edit Data');
            redirect(site_url('tabel_pegawai')); 
    }
    
    public function delete($id) 
    {
        $row = $this->Akun_pengguna_model->get_by_id($id);

        if ($row) {
            $this->Akun_pengguna_model->delete($id);
            $this->session->set_flashdata('success', 'Berhasil Hapus Data');
            redirect(site_url('akun_pengguna'));
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('akun_pengguna'));
        }
    }

    public function _rules() 
    {
		$this->form_validation->set_rules('user_name', 'user_name', 'trim|required');
		$this->form_validation->set_rules('pass', 'pass', 'trim|required');
		$this->form_validation->set_rules('id_user', 'id pegawai', 'trim|required'); 

		$this->form_validation->set_rules('', '', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "akun_pengguna.xls";
        $judul = "akun_pengguna";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "user_name");
	xlsWriteLabel($tablehead, $kolomhead++, "Password");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Pegawai");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Desa");

	foreach ($this->Akun_pengguna_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->user_name);
	    xlsWriteLabel($tablebody, $kolombody++, $data->password);
	    xlsWriteLabel($tablebody, $kolombody++, $data->id_user);
	    xlsWriteLabel($tablebody, $kolombody++, $data->id_desa);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Akun_pengguna.php */
/* Location: ./application/controllers/Akun_pengguna.php */
/* Please DO NOT modify this information : */
/* Generated by CRUDV2 Generator For AdminLTE Template 2024-01-02 14:11:57 */