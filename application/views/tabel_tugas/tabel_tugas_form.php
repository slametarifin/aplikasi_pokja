<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header">
                <div class="box-title">
                    <b><i class="fa fa-tasks"></i> <?= $button ?> Data Tabel_tugas</b>
                </div>
                <div class="box-tools pull-right">
                    <button type="button" class="toggle-expand-btn btn btn-default btn-sm"><i class="fa fa-expand"></i></button>
                </div>
            </div>
            <div class="box-body">
                <form style="padding: 15px;" action="<?= $action; ?>" method="POST" enctype="multipart/form-data">
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="varchar">Tugas</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="tugas" id="tugas" placeholder="Tugas" value="<?= $tugas; ?>" />
                                <?= form_error('tugas') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="varchar">Deskripsi</label>
                            <div class="col-md-6">
                                <textarea class="form-control" name="deskripsi"><?=$deskripsi?></textarea>
                                <!-- <input type="text" class="form-control" name="deskripsi" id="deskripsi" placeholder="Deskripsi" value="<?= $deskripsi; ?>" /> -->
                                <?= form_error('deskripsi') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="int">PIC Tugas</label>
                            <div class="col-md-6">
                                <select name="pic" class="form-control">
                                    <option value="-">pilih Pegawai</option> 
                                    <?php 
                                    $data=$this->db->query("select * from tabel_pegawai")->result();
                                    foreach($data as $dt){?>
                                        <option value="<?=$dt->id_peg?>" <?php if($dt->id_peg==$pic){?>selected <?php }?>><?=$dt->nama?></option>
                                    <?php }
                                    ?>
                                </select>
                                <!-- <input type="text" class="form-control" name="pic" id="pic" placeholder="Pic" value="<?= $pic; ?>" /> -->
                                <?= form_error('pic') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="int">Instasi OPD</label>
                            <div class="col-md-6">
                                <select name="id_opd" class="form-control">
                                    <option value="-">pilih Pegawai</option> 
                                    <?php 
                                    $data=$this->db->query("select * from tabel_opd")->result();
                                    foreach($data as $dt){?>
                                        <option value="<?=$dt->id?>" <?php if($dt->id==$id_opd){?>selected <?php }?>><?=$dt->nama_opd?></option>
                                    <?php }
                                    ?>
                                </select>
                                <!-- <input type="text" class="form-control" name="id_opd" id="id_opd" placeholder="Id Opd" value="<?= $id_opd; ?>" /> -->
                                <?= form_error('id_opd') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="int">Bidang</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="id_bidang" id="id_bidang" placeholder="Id Bidang" value="<?= $id_bidang; ?>" />
                                <?= form_error('id_bidang') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-2">
                              
                                <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> <?= $button ?></button>
                                <a href="<?= site_url('tabel_tugas') ?>" class="btn btn-danger"><i class="fa fa-sign-out"></i> Kembali</a>
                            </div>
                        </div>
                    </div>
				</form>
            </div>
        </div>
    </div>
</div>