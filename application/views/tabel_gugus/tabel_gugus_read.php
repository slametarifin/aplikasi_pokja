<div class="row">
	<div class="col-md-12">
		<div class="box box-success">
			<div class="box-header">
				<div class="box-title">
                    <b><i class="fa fa-eye"></i> Detail Data Tabel_gugus</b>
                </div>
                <div class="box-tools pull-right">
                    <button type="button" class="toggle-expand-btn btn btn-default btn-sm"><i class="fa fa-expand"></i></button>
                </div>
			</div>
			<div class="box-body">
				<div style="padding: 15px;">
					<table class="table table-striped">
						<tr>
							<td width="20%"><b>Nama Gugus</b></td>
							<td><?= $nama_gugus; ?></td>
						</tr>
						<tr>
							<td width="20%"><b>Nip Ketua</b></td>
							<td><?= $nip_ketua; ?></td>
						</tr>
						<tr>
							<td width="20%"><b>Periode Awal</b></td>
							<td><?= $periode_awal; ?></td>
						</tr>
						<tr>
							<td width="20%"><b>Periode Akhir</b></td>
							<td><?= $periode_akhir; ?></td>
						</tr>
						<tr>
							<td width="20%"><b>Status</b></td>
							<td><?= $status; ?></td>
						</tr>
						<tr>
							<td width="20%"><b>Kode Opd</b></td>
							<td><?= $kode_opd; ?></td>
						</tr>
					</table>
					<a href="<?= site_url('tabel_gugus') ?>" class="btn btn-danger pull-right">
						<i class="fa fa-sign-out"></i> Kembali
					</a>
				</div>
			</div>
		</div>
	</div>
</div>