1. src:https://stackoverflow.com/questions/20579959/silent-printing-direct-using-kiosk-mode-in-google-chrome
--kiosk --kiosk-printing <application_URL>
MUST USE:
PRINT DIRECT: --kiosk-printing
KIOSK MODE: --kiosk [application_url]
KIOSK MODE & PRINT DIRECT: --kiosk-printing --kiosk [application_url]
2. https://chrome.google.com/webstore/detail/chrome-direct-print/fnfkcaeloalplnglklappfjfjeafakeo?hl=id
3. Cara Mengaktifkan Mode Kiosk Printing untuk Google Chrome
Unduh dan install Google Chrome di sini: http://www.google.com/chrome

Pastikan printer struk Anda diatur sebagai printer default

Buka Google Chrome dan periksa bahwa Versi Chrome adalah versi terbaru

Buka akun Retail POS Anda, buka layar Penjualan (Sell) dan salin URL di bilah alamat di atas.

Selanjutnya, buka halaman pengaturan Chrome dengan mengetik chrome://settings/ di bilah alamat.

Di bagian On Startup, pilih buka halaman tertentu atau set kelompok halaman dan klik set halaman.

Tempelkan URL Layar Penjualan (Sell) Retail POS Anda di sini dan klik simpan.

Tutup semua jendela Google Chrome

Tekan CTRL-SHIFT-ESC dan akhiri tugas untuk semua Google Chrome

Setelah itu, temukan pintasan browser Chrome di desktop Anda, klik kanan dan pilih properti.

Tambahkan teks berikut di akhir bidang target setelah spasi: --enable-print-preview --kiosk --kiosk-printing

Simpan perubahan ini dan jalankan Google Chrome. Sekarang seharusnya akan tampil dalam mode layar penuh (kiosk)!

Lakukan penjualan atau tekan CTRL-E (buka laci kas).

Tampilan pratinjau cetak akan muncul di layar Anda, tetapi akan segera mencetak struk ke printer default Anda.