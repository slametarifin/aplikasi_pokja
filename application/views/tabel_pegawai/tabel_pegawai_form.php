  <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.0/dist/jquery.slim.min.js"></script>
  <div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header">
                <div class="box-title">
                    <b><i class="fa fa-tasks"></i> <?= $button ?> Data Tabel_pegawai</b>
                </div>
                <div class="box-tools pull-right">
                    <button type="button" class="toggle-expand-btn btn btn-default btn-sm"><i class="fa fa-expand"></i></button>
                </div>
            </div>
            <div class="box-body">
                <form style="padding: 15px;" action="<?= $action; ?>" method="POST" enctype="multipart/form-data">
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="varchar">NIP</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="nip" id="nip" placeholder="Nip" value="<?= $nip; ?>" />
                                <?= form_error('nip') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="varchar">Nama Pegawai</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="nama" id="nama" placeholder="Nama" value="<?= $nama; ?>" />
                                <?= form_error('nama') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="int">Instansi / OPD</label>
                            <div class="col-md-6">
                                <select name="id_opd" class="form-control select2 form-select">
                                    <option value="-">pilih data</option>
                                    <?php 
                                    $peg=$this->db->query("select id,nama_opd from tabel_opd")->result();
                                    foreach($peg as $pg){?>
                                        <option value="<?=$pg->id?>" <?php if($id_opd==$pg->id){?>selected <?php }?>><?=$pg->nama_opd?></option>
                                    <?php }?>
                                </select>
                                <!-- <input type="text" class="form-control" name="id_opd" id="id_opd" placeholder="Id Opd" value="<?= $id_opd; ?>" /> -->
                                <?= form_error('id_opd') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="int">Jabatan</label>
                            <div class="col-md-6">
                                <select name="id_jabatan" class="form-control select2 form-select">
                                    <option value="-">pilih data</option>
                                    <?php 
                                    $peg=$this->db->query("select id,nama_jabatan from tabel_jabatan")->result();
                                    foreach($peg as $pg){?>
                                        <option value="<?=$pg->id?>" <?php if($id_opd==$pg->id){?>selected <?php }?>><?=$pg->nama_jabatan?></option>
                                    <?php }?>
                                </select>
                                <!-- <input type="text" class="form-control" name="id_jabatan" id="id_jabatan" placeholder="Id Jabatan" value="<?= $id_jabatan; ?>" /> -->
                                <?= form_error('id_jabatan') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-2">
                              
                                <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> <?= $button ?></button>
                                <a href="<?= site_url('tabel_pegawai') ?>" class="btn btn-danger"><i class="fa fa-sign-out"></i> Kembali</a>
                            </div>
                        </div>
                    </div>
				</form>
            </div>
        </div>
    </div>
</div>