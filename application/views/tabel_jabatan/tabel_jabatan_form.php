  <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.0/dist/jquery.slim.min.js"></script>
  <div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header">
                <div class="box-title">
                    <b><i class="fa fa-tasks"></i> <?= $button ?> Data Tabel_jabatan</b>
                </div>
                <div class="box-tools pull-right">
                    <button type="button" class="toggle-expand-btn btn btn-default btn-sm"><i class="fa fa-expand"></i></button>
                </div>
            </div>
            <div class="box-body">
                <form style="padding: 15px;" action="<?= $action; ?>" method="POST" enctype="multipart/form-data">
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="varchar">Nama Jabatan</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="nama_jabatan" id="nama_jabatan" placeholder="Nama Jabatan" value="<?= $nama_jabatan; ?>" />
                                <?= form_error('nama_jabatan') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="int">Instansi OPD</label>
                            <div class="col-md-6">
                                <select name="id_opd" class="form-control select2 form-select">
                                    <option value="-">pilih data</option>
                                    <?php 
                                    $dataa=$this->db->query("select id,nama_opd from tabel_opd")->result();
                                    foreach($dataa as $dt){?>
                                        <option value="<?=$dt->id?>" <?php if($id_opd==$dt->id){?>selected <?php }?>><?=$dt->nama_opd?></option>
                                    <?php }
                                    ?>
                                </select>
                                <!-- <input type="text" class="form-control" name="id_opd" id="id_opd" placeholder="Id Opd" value="<?= $id_opd; ?>" /> -->
                                <?= form_error('id_opd') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-2">
                              
                                <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> <?= $button ?></button>
                                <a href="<?= site_url('tabel_jabatan') ?>" class="btn btn-danger"><i class="fa fa-sign-out"></i> Kembali</a>
                            </div>
                        </div>
                    </div>
				</form>
            </div>
        </div>
    </div>
</div>