<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header">
                <div class="box-title">
                    <b><i class="fa fa-tasks"></i> <?= $button ?> Data Tbl_pelayanan</b>
                </div>
                <div class="box-tools pull-right">
                    <button type="button" class="toggle-expand-btn btn btn-default btn-sm"><i class="fa fa-expand"></i></button>
                </div>
            </div>
            <div class="box-body">
                <form style="padding: 15px;" action="<?= $action; ?>" method="POST" enctype="multipart/form-data"> 
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="int">Instansi OPD</label>
                            <div class="col-md-6">
                                <select name="instansi" class="form-control">
                                <option value="-">pilih data</option>
                                <?php 
                                $instansi=$this->db->query("select id, nm_instansi from Tbl_instansi")->result();
                                foreach($instansi as $dt){?>
                                <option value="<?=$dt->id?>" <?php if($dt->id==$id_instansi){?>selected <?php }?>><?=$dt->nm_instansi?></option>
                                <?php }
                                ?>
                                </select>
                                <?= form_error('id_layanan') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group" style="display:none;">
                        <div class="row">
                            <label class="col-md-2" for="int">Instansi</label>
                            <div class="col-md-6">
                                <select name="id_instansi" class="form-control">
                                    <option value="-">pilih data</option>
                                    <?php 
                                    $ins=$this->db->query("select * from Tbl_instansi")->result();
                                    foreach($ins as $inst){
                                        ?>
                                        <option value="<?=$inst->id?>" <?php if($id_instansi==$inst->id){?>selected <?php }?>><?=$inst->nm_instansi?></option>
                                        <?php
                                    }
                                    ?>
                                </select> 
                                <?= form_error('id_instansi') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="varchar">Nama Layanan</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="nm_layanan" id="nm_layanan" placeholder="Nm Layanan" value="<?= $nm_layanan; ?>" />
                                <?= form_error('nm_layanan') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="varchar">Grup Layanan</label>
                            <div class="col-md-6">
                                <select name="grup_layanan" class="form-control">
                                    <option value="-">pilih data</option>
                                    <?php
                                    // cek role
                                    $user=$this->db->query("select id,role from Tbl_pengguna where id=".$this->session->userdata('id_user')."")->row();
                                    $role='';
                                    if($user){
                                        if($user->role==2){
                                            $where="";
                                        }
                                        else{
                                            $where="where id_instansi = (select id_instansi from Tbl_pengguna where id=".$this->session->userdata('id_user').")";
                                        }
                                    }
                                    $grup=$this->db->query("select id_instansi,id,nm_grup_layanan,(select nm_instansi from Tbl_instansi where id=id_instansi)nm_dinas from Tbl_grup_pelayanan ".$where."")->result();
                                    foreach($grup as $gr){?>
                                    <option value="<?=$gr->id?>" <?php if($grup_layanan==$gr->id){?>selected <?php }?>><?=$gr->nm_dinas.' '.$gr->nm_grup_layanan?></option>
                                    <?php }
                                    ?>
                                </select>
                                <!--<input type="text" class="form-control" name="nm_layanan" id="nm_layanan" placeholder="Nm Layanan" value="<?= $nm_layanan; ?>" />-->
                                <?= form_error('nm_layanan') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="int">Parent</label>
                            <div class="col-md-6">
                                <select class="form-control" name="parent">
                                    <option value="-">pilih data</option>
                                    <option value="parent" <?php if($parent=='parent'){?>selected <?php }?>>Parent</option>
                                    <?php 
                                    $user=$this->db->query("select role from Tbl_pengguna where id=".$this->session->userdata('id_user')."")->row();
                                    if($user->role==2){
                                        $where="";
                                    }else{
                                        $where="where a.id_instansi=(select id_instansi from Tbl_pengguna where id=".$this->session->userdata('id_user').")";
                                    }
                                    $layanan=$this->db->query("select a.*,(select nm_instansi from Tbl_instansi where Tbl_instansi.id=a.id_instansi) opd from Tbl_pelayanan a ".$where." ")->result();
                                    foreach($layanan as $lyn){?>
                                        <option value="<?=$lyn->id?>" <?php if($parent==$lyn->id){?>selected <?php }?>><?=$lyn->opd.' '.$lyn->nm_layanan?></option>
                                    <?php }
                                    ?>
                                </select>
                                <?= form_error('parent') ?>
                            </div>
                        </div>
                    </div> 
					<div class="form-group">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-2"> 
                                <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> <?= $button ?></button>
                                <a href="<?= site_url('tbl_pelayanan') ?>" class="btn btn-danger"><i class="fa fa-sign-out"></i> Kembali</a>
                            </div>
                        </div>
                    </div>
				</form>
            </div>
        </div>
    </div>
</div>