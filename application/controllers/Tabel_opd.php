<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tabel_opd extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Tabel_opd_model');
        $this->load->library('form_validation');
		$this->load->library('datatables');
    }

    public function index()
    {
        $cek_role=$this->db->query("
            select id,role,user_name from tabel_akun 
            where user_name='".$this->session->userdata('username')."'
        ")->row();
        if($cek_role->role==2){
            $display="block";
        }elseif($cek_role->role==0||$cek_role->role==1){
            $display="none";
        }
        $data=array('id_user'=>$cek_role->id,'akses'=>$cek_role->role,'display'=>$display);
        $this->template->load('template_antrian','tabel_opd/tabel_opd_list',$data);
    } 
    
    public function json() {
        header('Content-Type: application/json');
        $cek_role=$this->db->query("
            select id,role,user_name,id_user,id_opd from tabel_akun 
            join tabel_pegawai on tabel_pegawai.id_peg=tabel_akun.id_user
            where user_name='".$this->session->userdata('username')."'
        ")->row();
        if($cek_role->role==2){
            echo $this->Tabel_opd_model->json();
            // $display="block";
        }elseif($cek_role->role==0||$cek_role->role==1){
            // $display="none";
            echo $this->Tabel_opd_model->json_opd($cek_role->id_opd);
        }
    }

    public function read($id) 
    {
        $row = $this->Tabel_opd_model->get_by_id($id);
        if ($row) {
            $data = array(
				'id' => $row->id,
				'kode_opd' => $row->kode_opd,
				'nama_opd' => $row->nama_opd,
				'created_date' => $row->created_date,
				'deleted_date' => $row->deleted_date,
				'deleted_by' => $row->deleted_by,
			);
            $this->template->load('template_antrian','tabel_opd/tabel_opd_read', $data);
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('tabel_opd'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Tambah',
            'action' => site_url('tabel_opd/create_action'),
			'id' => set_value('id'),
			'kode_opd' => set_value('kode_opd'),
			'nama_opd' => set_value('nama_opd'),
			'created_date' => set_value('created_date'),
			'deleted_date' => set_value('deleted_date'),
			'deleted_by' => set_value('deleted_by'),
		);
        $this->template->load('template_antrian','tabel_opd/tabel_opd_form', $data);
    }
    
    public function create_action() 
    { 
            $data = array(
				'kode_opd' => $this->input->post('kode_opd',TRUE),
				'nama_opd' => $this->input->post('nama_opd',TRUE),
				'created_date' => date('Y-m-d H:i:s'),
				'deleted_date' => '',
				'deleted_by' => '',
			);

            $this->Tabel_opd_model->insert($data);
            $this->session->set_flashdata('success', 'Berhasil Tambah Data');
            redirect(site_url('tabel_opd')); 
    }
    
    public function update($id) 
    {
        $row = $this->Tabel_opd_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Ubah',
                'action' => site_url('tabel_opd/update_action/'.$id),
				'id' => set_value('id', $row->id),
				'kode_opd' => set_value('kode_opd', $row->kode_opd),
				'nama_opd' => set_value('nama_opd', $row->nama_opd), 
                'created_date' => set_value('created_date', $row->created_date), 
			);
            $this->template->load('template_antrian','tabel_opd/tabel_opd_form', $data);
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('tabel_opd'));
        }
    }
    
    public function update_action($id) 
    { 
            $data = array(
				'kode_opd' => $this->input->post('kode_opd',TRUE),
				'nama_opd' => $this->input->post('nama_opd',TRUE), 
			);

            $this->Tabel_opd_model->update($id, $data);
            $this->session->set_flashdata('success', 'Berhasil Edit Data');
            redirect(site_url('tabel_opd')); 
    }
    
    public function delete($id) 
    {
        $row = $this->Tabel_opd_model->get_by_id($id);

        if ($row) {
            $this->Tabel_opd_model->delete($id);
            $this->session->set_flashdata('success', 'Berhasil Hapus Data');
            redirect(site_url('tabel_opd'));
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('tabel_opd'));
        }
    }

    public function _rules() 
    {
		$this->form_validation->set_rules('kode_opd', 'kode opd', 'trim|required');
		$this->form_validation->set_rules('nama_opd', 'nama opd', 'trim|required');
		$this->form_validation->set_rules('created_date', 'created date', 'trim|required');
		$this->form_validation->set_rules('deleted_date', 'deleted date', 'trim|required');
		$this->form_validation->set_rules('deleted_by', 'deleted by', 'trim|required');

		$this->form_validation->set_rules('', '', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "tabel_opd.xls";
        $judul = "tabel_opd";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Kode Opd");
	xlsWriteLabel($tablehead, $kolomhead++, "Nama Opd");
	xlsWriteLabel($tablehead, $kolomhead++, "Created Date");
	xlsWriteLabel($tablehead, $kolomhead++, "Deleted Date");
	xlsWriteLabel($tablehead, $kolomhead++, "Deleted By");

	foreach ($this->Tabel_opd_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->kode_opd);
	    xlsWriteLabel($tablebody, $kolombody++, $data->nama_opd);
	    xlsWriteLabel($tablebody, $kolombody++, $data->created_date);
	    xlsWriteLabel($tablebody, $kolombody++, $data->deleted_date);
	    xlsWriteLabel($tablebody, $kolombody++, $data->deleted_by);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Tabel_opd.php */
/* Location: ./application/controllers/Tabel_opd.php */
/* Please DO NOT modify this information : */
/* Generated by CRUDV2 Generator For AdminLTE Template 2024-01-08 09:04:52 */