<div class="row">
	<div class="col-md-12">
		<div class="box box-success">
			<div class="box-header">
				<div class="box-title">
                    <b><i class="fa fa-eye"></i> Detail Data Tbl_grup_pelayanan</b>
                </div>
                <div class="box-tools pull-right">
                    <button type="button" class="toggle-expand-btn btn btn-default btn-sm"><i class="fa fa-expand"></i></button>
                </div>
			</div>
			<div class="box-body">
				<div style="padding: 15px;">
					<table class="table table-striped">
						<tr>
							<td width="20%"><b>Id Instansi</b></td>
							<td><?= $id_instansi; ?></td>
						</tr>
						<tr>
							<td width="20%"><b>Nm Grup Layanan</b></td>
							<td><?= $nm_grup_layanan; ?></td>
						</tr>
					</table>
					<a href="<?= site_url('tbl_grup_pelayanan') ?>" class="btn btn-danger pull-right">
						<i class="fa fa-sign-out"></i> Kembali
					</a>
				</div>
			</div>
		</div>
	</div>
</div>