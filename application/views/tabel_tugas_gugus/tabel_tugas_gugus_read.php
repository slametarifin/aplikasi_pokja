<div class="row">
	<div class="col-md-12">
		<div class="box box-success">
			<div class="box-header">
				<div class="box-title">
                    <b><i class="fa fa-eye"></i> Detail Data Tabel_tugas_gugus</b>
                </div>
                <div class="box-tools pull-right">
                    <button type="button" class="toggle-expand-btn btn btn-default btn-sm"><i class="fa fa-expand"></i></button>
                </div>
			</div>
			<div class="box-body">
				<div style="padding: 15px;">
					<table class="table table-striped">
						<tr>
							<td width="20%"><b>Id Gugus Tugas</b></td>
							<td><?= $id_gugus_tugas; ?></td>
						</tr>
						<tr>
							<td width="20%"><b>Tugas</b></td>
							<td><?= $tugas; ?></td>
						</tr>
						<tr>
							<td width="20%"><b>Deskripsi</b></td>
							<td><?= $deskripsi; ?></td>
						</tr>
						<tr>
							<td width="20%"><b>Pic</b></td>
							<td><?= $pic; ?></td>
						</tr>
						<tr>
							<td width="20%"><b>Status</b></td>
							<td><?= $status; ?></td>
						</tr>
					</table>
					<a href="<?= site_url('tabel_tugas_gugus') ?>" class="btn btn-danger pull-right">
						<i class="fa fa-sign-out"></i> Kembali
					</a>
				</div>
			</div>
		</div>
	</div>
</div>