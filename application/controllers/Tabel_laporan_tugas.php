<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tabel_laporan_tugas extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Tabel_laporan_tugas_model');
        $this->load->library('form_validation');
		$this->load->library('datatables');
    }

    public function index()
    {
        $cek_role=$this->db->query("select (select id_opd from tabel_pegawai where id_peg=id_user)id_opd,id,role,user_name from tabel_akun where user_name='".$this->session->userdata('username')."'")->row();
        if($cek_role->role==2){
            $display="block";
        }elseif($cek_role->role==0||$cek_role->role==1){
            // $display="none";
            redirect(site_url('/'));
        }
        $data=array('id_opd'=>$cek_role->id_opd,'id_user'=>$cek_role->id,'akses'=>$cek_role->role,'display'=>$display);
        $this->template->load('template_antrian','tabel_laporan_tugas/tabel_laporan_tugas_listtt',$data);
    } 

    public function aproval()
    {
        $dataa=$this->db->query("
            select a.*,b.nip,c.id id_gugus from tabel_akun a
            join tabel_pegawai b on b.id_peg=a.id_user
            join tabel_gugus c on c.nip_ketua=b.nip
            where a.id='".$this->session->userdata('id_user')."'
        ")->row();
        if($dataa){
            $data=array('nip_ketua'=>$dataa->nip,'id_gugus'=>$dataa->id_gugus);
            $this->template->load('template_antrian','tabel_laporan_tugas/tabel_laporan_tugas_listt',$data);
        }else{
            redirect(site_url('/'));
        }
    } 
    public function bukti($tugas=null,$gugus=null){
        $data=array('gugus'=>$gugus,'tugas'=>$tugas);
        $this->template->load('template_antrian','tabel_laporan_tugas/tabel_laporan_tugas_list',$data);        
    }
    
    public function json($gugus=null,$tugas=null){
        header('Content-Type: application/json');
        echo $this->Tabel_laporan_tugas_model->json($gugus,$tugas);
    }
    
    public function jsonn($nip_ketua=null) {
        header('Content-Type: application/json');
        echo $this->Tabel_laporan_tugas_model->jsonn($nip_ketua);
    }
    
    public function jsonnn($id_opd=null) {
        header('Content-Type: application/json');
        echo $this->Tabel_laporan_tugas_model->jsonnn($id_opd);
    }

    public function read($id) 
    {
        $row = $this->Tabel_laporan_tugas_model->get_by_id($id);
        if ($row) {
            $data = array(
				'id' => $row->id,
				'id_tugas' => $row->id_tugas,
				'id_gugus' => $row->id_gugus,
				'link' => $row->link,
				'acc' => $row->acc,
			);
            $this->template->load('template_antrian','tabel_laporan_tugas/tabel_laporan_tugas_read', $data);
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('tabel_laporan_tugas'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Tambah',
            'action' => site_url('tabel_laporan_tugas/create_action'),
			'id' => set_value('id'),
			'id_tugas' => set_value('id_tugas'),
			'id_gugus' => set_value('id_gugus'),
			'link' => set_value('link'),
			'acc' => set_value('acc'),
		);
        $this->template->load('template_antrian','tabel_laporan_tugas/tabel_laporan_tugas_form', $data);
    }
    
    public function create_action() 
    { 
            $data = array(
				'id_tugas' => $this->input->post('id_tugas',TRUE),
				'id_gugus' => $this->input->post('id_gugus',TRUE),
				'link' => $this->input->post('link',TRUE),
				'acc' => $this->input->post('acc',TRUE),
			);

            $this->Tabel_laporan_tugas_model->insert($data);
            $this->session->set_flashdata('success', 'Berhasil Tambah Data');
            redirect(site_url('tabel_laporan_tugas')); 
    }
    
    public function update($id) 
    {
        $row = $this->Tabel_laporan_tugas_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Ubah',
                'action' => site_url('tabel_laporan_tugas/update_action/'.$id),
				'id' => set_value('id', $row->id),
				'id_tugas' => set_value('id_tugas', $row->id_tugas),
				'id_gugus' => set_value('id_gugus', $row->id_gugus),
				'link' => set_value('link', $row->link),
				'acc' => set_value('acc', $row->acc),
			);
            $this->template->load('template_antrian','tabel_laporan_tugas/tabel_laporan_tugas_form', $data);
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('tabel_laporan_tugas'));
        }
    }
    
    public function aprove($id=null,$val=null) 
    { 
        $data = array( 
            'acc' => $val,
        ); 

        $this->Tabel_laporan_tugas_model->update($id, $data);
        $this->session->set_flashdata('success', 'Berhasil Edit Data');
        redirect(site_url('tabel_laporan_tugas/aproval')); 
    }
    
    public function update_action() 
    { 
            $data = array(
				'id_tugas' => $this->input->post('id_tugas',TRUE),
				'id_gugus' => $this->input->post('id_gugus',TRUE),
				'link' => $this->input->post('link',TRUE),
				'acc' => $this->input->post('acc',TRUE),
			);

            $this->Tabel_laporan_tugas_model->update($id, $data);
            $this->session->set_flashdata('success', 'Berhasil Edit Data');
            redirect(site_url('tabel_laporan_tugas')); 
    }
    
    public function delete($id) 
    {
        $row = $this->Tabel_laporan_tugas_model->get_by_id($id);

        if ($row) {
            $this->Tabel_laporan_tugas_model->delete($id);
            $this->session->set_flashdata('success', 'Berhasil Hapus Data');
            redirect(site_url('tabel_laporan_tugas'));
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('tabel_laporan_tugas'));
        }
    }

    public function _rules() 
    {
		$this->form_validation->set_rules('id_tugas', 'id tugas', 'trim|required');
		$this->form_validation->set_rules('id_gugus', 'id gugus', 'trim|required');
		$this->form_validation->set_rules('link', 'link', 'trim|required');
		$this->form_validation->set_rules('acc', 'acc', 'trim|required');

		$this->form_validation->set_rules('', '', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "tabel_laporan_tugas.xls";
        $judul = "tabel_laporan_tugas";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Tugas");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Gugus");
	xlsWriteLabel($tablehead, $kolomhead++, "Link");
	xlsWriteLabel($tablehead, $kolomhead++, "Acc");

	foreach ($this->Tabel_laporan_tugas_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteNumber($tablebody, $kolombody++, $data->id_tugas);
	    xlsWriteNumber($tablebody, $kolombody++, $data->id_gugus);
	    xlsWriteLabel($tablebody, $kolombody++, $data->link);
	    xlsWriteLabel($tablebody, $kolombody++, $data->acc);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Tabel_laporan_tugas.php */
/* Location: ./application/controllers/Tabel_laporan_tugas.php */
/* Please DO NOT modify this information : */
/* Generated by CRUDV2 Generator For AdminLTE Template 2024-01-15 03:04:46 */