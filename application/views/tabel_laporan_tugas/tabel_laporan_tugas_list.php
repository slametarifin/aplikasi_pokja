<div class="row"> 
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <div class="box-title">
                    <b><i class="fa fa-list-alt"></i> Data Tabel_laporan_tugas</b>
                </div>
                <div class="box-tools pull-right">
                    <button type="button" class="toggle-expand-btn btn btn-default btn-sm"><i class="fa fa-expand"></i></button>
                </div>
            </div>
            <div class="box-body">
                <a href="<?= site_url('tabel_gugus/tugas_gugus/'.$gugus) ?>" class="btn btn-danger" style="margin-left : 15px"><i class="fa fa-plus"></i> Kembali</a> 
                <div class="table-responsive" style="padding: 15px">
                    <table class="table table-bordered table-striped table-hover" width="100%" id="mytable">
                        <thead>
                            <tr>
                                <th width="5%">No</th>
								<th>Tugas</th>
								<th>Gugus Tugas</th>
								<th>Link</th>
								<th>Acc</th>
								<th width="15%">Aksi</th>
                            </tr>
                        </thead>    
                    </table>

                    <script src="<?= base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
                    <script type="text/javascript">
                        $(document).ready(function() {
                            $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
                            {
                                return {
                                    "iStart": oSettings._iDisplayStart,
                                    "iEnd": oSettings.fnDisplayEnd(),
                                    "iLength": oSettings._iDisplayLength,
                                    "iTotal": oSettings.fnRecordsTotal(),
                                    "iFilteredTotal": oSettings.fnRecordsDisplay(),
                                    "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                                    "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                                };
                            };

                            var t = $("#mytable").dataTable({
                                "processing"  : true,
                                "serverSide"  : true,
                                "oLanguage"   : { sProcessing : "Loading. . ." },
                                "ajax"        : { "url" : "<?= site_url('tabel_laporan_tugas/json/'.$gugus.'/'.$tugas) ?>", "type": "POST"},
                                "columns"     : [
                                    {
                                        "data": "id",
                                        "orderable": false,
                                        "className" : "text-center"
                                    },
									{
										"data": "tugas"
									},
									{
										"data": "nama_gugus"
									},
									{
										"data": "link",
                                        render: function (data, type, row) {
                                            if(row.link==''||row.link==null){
                                                return "<label class='badge rounded-pill bg-danger'>Belum ada laporan</label>";
                                            }else{
                                                return "<a title='Priview laporan' href='<?=site_url('./uploads/laporan/')?>"+row.link+"' class='btn btn-sm btn-primary'><i class='bi bi-download'></i></a>";
                                            }
                                        }
									},
									{
										"data": "acc",
                                        render: function (data, type, row) {
                                            if(row.acc==1){ 
                                                return "<label class='badge rounded-pill bg-info'>Belum Approval</label>";
                                            }else if(row.acc==2){
                                                return "<label class='badge rounded-pill bg-success'>Sudah Diterima</label>";
                                            }
                                            else{
                                                return "<label class='badge rounded-pill bg-danger'>Laporan Tertolak</label>";
                                            }
                                        }
									},
                                    {
                                        "data" : "action",
                                        "orderable": false,
                                        "className" : "text-center",
                                        render: function (data, type, row) {
                                            if(row.acc==1||row.acc==3){
                                                return "<a title='Hapus Data' href='<?=site_url('/tabel_laporan_tugas/hapus/')?>"+row.id+"' class='btn btn-sm btn-danger'><i class='bi bi-trash'></i></a>";
                                            }
                                            else if(row.acc==2){
                                                return "<label class='badge rounded-pill bg-success'>Sudah Diterima</label>";
                                            }
                                        }
                                    }
                                ],
                                order: [[0, 'desc']],
                                rowCallback: function(row, data, iDisplayIndex) {
                                    var info = this.fnPagingInfo();
                                    var page = info.iPage;
                                    var length = info.iLength;
                                    var index = page * length + (iDisplayIndex + 1);
                                    $('td:eq(0)', row).html(index);
                                }
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>
<?php // swal_delete("#mytable",".hapus") ?>
		<?= anchor(site_url('tabel_laporan_tugas/excel'), '<i class="fa fa-file-excel-o" aria-hidden="true"></i> Export Excel', 'class="btn btn-success"'); ?>