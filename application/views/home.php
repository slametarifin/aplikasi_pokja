<div class="row" style="margin-top:2%">
	<div class="col-md-12">
		<div class="col-lg-3 col-6">
			<div class="small-box bg-success">
				<div class="inner">
					<h3>Pengguna<sup style="font-size: 20px"> </sup></h3>
					<p>Tabel username</p>
				</div>
				<div class="icon">
					<i class="ion ion-person"></i>
				</div>
				<a href="<?=site_url('Tbl_userc')?>" class="small-box-footer">lebih lanjut <i class="fas fa-arrow-circle-right-o"></i></a>
			</div>
		</div>
		<div class="col-lg-3 col-6">
			<div class="small-box bg-info">
				<div class="inner">
					<h3>Menu<sup style="font-size: 20px"> </sup></h3>
					<p>Tabel fitur</p>
				</div>
				<div class="icon">
					<i class="ion ion-grid"></i>
				</div>
				<a href="<?=site_url('tbl_menuc')?>" class="small-box-footer">lebih lanjut <i class="fas fa-arrow-circle-right-o"></i></a>
			</div>
		</div>
		<div class="col-lg-3 col-6">
			<div class="small-box bg-waring">
				<div class="inner">
					<h3>Menu User<sup style="font-size: 20px"> </sup></h3>
					<p>Tabel fitur user</p>
				</div>
				<div class="icon">
					<i class="ion ion-grid"></i>
				</div>
				<a href="<?=site_url('tbl_menu_userc')?>" class="small-box-footer">lebih lanjut <i class="fas fa-arrow-circle-right-o"></i></a>
			</div>
		</div>   
		<div class="col-lg-3 col-6">
			<div class="small-box bg-success">
				<div class="inner">
					<h3>Evaluasi <sup style="font-size: 20px"> </sup></h3>
					<p>Evaluasi pelaksanaan program</p>
				</div>
				<div class="icon">
					<i class="ion ion-grid"></i>
				</div>
				<a href="<?=site_url('evaluasi_program')?>" class="small-box-footer">lebih lanjut <i class="fas fa-arrow-circle-right-o"></i></a>
			</div>
		</div>   
	</div>
</div> 