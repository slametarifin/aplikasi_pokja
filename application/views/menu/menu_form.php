
<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header">
                <div class="box-title">
                    <b><i class="fa fa-tasks"></i> <?= $button ?> Data Menu</b>
                </div>
                <div class="box-tools pull-right">
                    <button type="button" class="toggle-expand-btn btn btn-default btn-sm"><i class="fa fa-expand"></i></button>
                </div>
            </div>
            <div class="box-body">
                <form style="padding: 15px;" action="<?= $action; ?>" method="POST" enctype="multipart/form-data">
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="varchar">Caption</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="caption" id="caption" placeholder="Caption" value="<?= $caption; ?>" />
                                <?= form_error('caption') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="varchar">Name</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?= $name; ?>" />
                                <?= form_error('name') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="tinyint">Icon</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="icon" id="icon" placeholder="Icon" value="<?= $icon; ?>" />
                                <?= form_error('icon') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="varchar">Grup</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="grup" id="grup" placeholder="Grup" value="<?= $grup; ?>" />
                                <?= form_error('grup') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-2">
                              
                                <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> <?= $button ?></button>
                                <a href="<?= site_url('menu') ?>" class="btn btn-danger"><i class="fa fa-sign-out"></i> Kembali</a>
                            </div>
                        </div>
                    </div>
				</form>
            </div>
        </div>
    </div>
</div>