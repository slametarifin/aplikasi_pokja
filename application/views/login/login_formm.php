<style type="text/css">
    h1,h2,label{color: #000;}
</style>
<div class="row">
    <div class="col-md-12">
        <div class="box box-success"> 
            <div class="box-body"> 
                <!-- <center><img src="<?=site_url('./assets/img/Logo-Cilacap.png')?>" height="30%" width="20%"></center><br> -->
                <!-- <h1><center>Sistem Program Kerja</center></h1> -->
                <!-- <h2><center>Selamat Datang</h2> -->
                <?php //print_r($this->session->userdata());?>
                <form style="padding: 15px;" action="<?=$action?>" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="varchar">User name</label>
                            <div class="col-md-10">
                                <input type="text" class="form-control" name="user_name" id="user_name" placeholder="Username" value= "<?=$user_name?>" />
                                <?= form_error('user_name') ?>
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="varchar">Password</label>
                            <div class="col-md-10">
                                <input type="pass" class="form-control" minlength="5" name="pass" id="pass" placeholder="pass" value="" />
                                <?= form_error('password') ?>
                            </div>
                        </div>
                    </div> 
                    <div class="form-group" style="margin-top:1%">
                        <div class="row">
                            <div class="col-md-2"></div>
                            <div class="col-md-4"> 
                                <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> <?= $button ?></button>
                                <a href="<?= site_url('login/data_login') ?>" class="btn btn-danger"><i class="fa fa-sign-out"></i> Kembali</a>
                            </div>
                        </div>
                    </div>
                   <!--  <div class="form-group">
                        <div class="row">
                            <div class="col-md-4 col-md-offset-2">
                                <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> Simpan Data </button>
                                <a style="margin-left: 60%;" href="<?= site_url('login/data_login') ?>" class="btn btn-danger"><i class="fa fa-sign-out"></i> Batal</a>
                            </div>
                        </div>
                    </div> -->
                </form>
            </div>
        </div>
    </div>
</div>