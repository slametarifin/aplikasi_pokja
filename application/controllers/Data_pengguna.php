<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Data_pengguna extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Data_pengguna_model');
        $this->load->library('form_validation');
		$this->load->library('datatables');
    }

    public function index()
    {
        $data = array(
            'data_pengguna_data' => $this->Data_pengguna_model->get_all(),
        );
        $this->template->load('templatex','data_pengguna/data_pengguna_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Data_pengguna_model->get_by_id($id);
        if ($row) {
            $data = array(
				'id' => $row->id,
				'nama_pengguna' => $row->nama_pengguna,
				'kode_desa' => $row->kode_desa,
				'level_pengguna' => $row->level_pengguna,
			);
            $this->template->load('templatex','data_pengguna/data_pengguna_read', $data);
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('data_pengguna'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Tambah',
            'action' => site_url('data_pengguna/create_action'),
			'id' => set_value('id'),
			'nama_pengguna' => set_value('nama_pengguna'),
			'kode_desa' => set_value('kode_desa'),
			'level_pengguna' => set_value('level_pengguna'),
		);
        $this->template->load('templatex','data_pengguna/data_pengguna_form', $data);
    }
    
    public function create_action() {
            $data = array(
				'nama_pengguna' => $this->input->post('nama_pengguna',TRUE),
				'kode_desa' => $this->input->post('kode_desa',TRUE),
				'level_pengguna' => $this->input->post('level_pengguna',TRUE),
			); 
            $this->Data_pengguna_model->insert($data);
            $this->session->set_flashdata('success', 'Berhasil Tambah Data');
            redirect(site_url('data_pengguna')); 
    }
    
    public function update($id) 
    {
        $row = $this->Data_pengguna_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Ubah',
                'action' => site_url('data_pengguna/update_action'),
				'id' => set_value('id', $row->id),
				'nama_pengguna' => set_value('nama_pengguna', $row->nama_pengguna),
				'kode_desa' => set_value('kode_desa', $row->kode_desa),
				'level_pengguna' => set_value('level_pengguna', $row->level_pengguna),
			);
            $this->template->load('templatex','data_pengguna/data_pengguna_form', $data);
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('data_pengguna'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('', TRUE));
        } else {
            $data = array(
				'nama_pengguna' => $this->input->post('nama_pengguna',TRUE),
				'kode_desa' => $this->input->post('kode_desa',TRUE),
				'level_pengguna' => $this->input->post('level_pengguna',TRUE),
			);

            $this->Data_pengguna_model->update($this->input->post('', TRUE), $data);
            $this->session->set_flashdata('success', 'Berhasil Edit Data');
            redirect(site_url('data_pengguna'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Data_pengguna_model->get_by_id($id);

        if ($row) {
            $this->Data_pengguna_model->delete($id);
            $this->session->set_flashdata('success', 'Berhasil Hapus Data');
            redirect(site_url('data_pengguna'));
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('data_pengguna'));
        }
    }

    public function _rules() 
    {
		$this->form_validation->set_rules('nama_pengguna', 'nama pengguna', 'trim|required');
		$this->form_validation->set_rules('kode_desa', 'kode desa', 'trim|required');
		$this->form_validation->set_rules('level_pengguna', 'level pengguna', 'trim|required');

		$this->form_validation->set_rules('', '', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "data_pengguna.xls";
        $judul = "data_pengguna";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Nama Pengguna");
	xlsWriteLabel($tablehead, $kolomhead++, "Kode Desa");
	xlsWriteLabel($tablehead, $kolomhead++, "Level Pengguna");

	foreach ($this->Data_pengguna_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->nama_pengguna);
	    xlsWriteLabel($tablebody, $kolombody++, $data->kode_desa);
	    xlsWriteNumber($tablebody, $kolombody++, $data->level_pengguna);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Data_pengguna.php */
/* Location: ./application/controllers/Data_pengguna.php */
/* Please DO NOT modify this information : */
/* Generated by CRUDV2 Generator For AdminLTE Template 2024-01-02 13:49:28 */