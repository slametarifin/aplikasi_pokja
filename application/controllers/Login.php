<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Login_model');
        $this->load->library('form_validation');
		$this->load->library('datatables');
        $this->load->library('session');
    }

    public function index()
    {
        if($this->session->userdata()){
            if($this->session->userdata('username')){
                redirect(site_url('beranda'));
            }else{
            }
        }
        $this->template->load('template_home','login/login_form');
    }
    public function log_out(){
        $array_val = array('username' => '','id_user' => '');
        $this->session->unset_userdata($array_val);
        $this->session->sess_destroy();
        redirect(site_url('login')); 
    }

    public function data_login()
    {
        // cek role user
        $user=$this->session->userdata();
        // print_r($user=$this->session->userdata());
        if($this->session->userdata('id_user')){

        }else{
            redirect(site_url('login'));
        }//die(); 
        if($user){
            $kode_user=$this->db->query("select * from tabel_akun where id=".$this->session->userdata('id_user')."")->row();
            // echo $kode_user->role;
            // echo $kode_user->role; die();
            if($kode_user->role=='2'){
                // echo "lanjut";
            }else{
                // echo "user bisasa redirect ke dashbord user";
                redirect(site_url('beranda'));
            } 
            $this->template->load('template_antrian','login/login_list');
            // $this->template->load('template_antrian','tbl_menu/tbl_menu_list');
        }else{
            redirect(site_url('login'));
        }
    }  
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Login_model->json();
    } 
    public function cek_login(){
        $user = $this->db->query("
        select a.*,b.id_opd,b.id_peg,id_user from tabel_akun a 
        join tabel_pegawai b on a.id_user = b.id_peg
        where user_name='".$this->input->post('user_name',TRUE)."'
        ")->row();  
        if ($user) { 
            if (password_verify($this->input->post('pass',TRUE), $user->pass)) {
                // echo "ke halaman dahbord";
                $newdata = array(
                'username'  => $this->input->post('user_name',TRUE),
                'id_user'   => $user->id_user, 
                ); 
                $this->session->set_userdata($newdata); 
                redirect(site_url('beranda'));
            } else {
                // echo "ke halaman login"; 
                redirect(site_url('login'));
            }
        } else {
            // echo "ke halaman login, cek user";
            redirect(site_url('login')); 
        }
    }
    public function resset_akun_mandiri(){
        $user = $this->db->query("
        select a.*,b.id_opd,b.id_peg,id_user from tabel_akun a 
        join tabel_pegawai b on a.id_user = b.id_peg
        where id_peg='".$this->session->userdata('id_user')."'
        ")->row();   
        if ($user) {  
            $data = array(
                'user_name' => $this->input->post('user_name',TRUE),
                'pass' => password_hash('12345', PASSWORD_DEFAULT),
                'id_user' => $this->input->post('id_user',TRUE),  
            ); 
            $this->Login_model->update($user->id, $data);
            $this->session->set_flashdata('success', 'Berhasil Edit Data');
            redirect(site_url('tabel_pegawai/dashbord'));  
        } else { 
            redirect(site_url('tabel_pegawai/dashbord')); 
        }
    }

    public function read($id) 
    {
        $row = $this->Login_model->get_by_id($id);
        if ($row) {
            $data = array(
				'id' => $row->id,
				'user_name' => $row->user_name,
				'pass' => $row->pass,
				'id_user' => $row->id_user,
			);
            $this->template->load('template_antrian','login/login_read', $data);
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('login'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Tambah',
            'action' => site_url('login/create_action'),
			'id' => set_value('id'),
			'user_name' => set_value('user_name'),
			'pass' => set_value('pass'),
			'id_user' => set_value('id_user'),
		);
        $this->template->load('template_antrian','login/login_formm', $data);
    }
    
    public function create_action() 
    { 
            $data = array(
				'user_name' => $this->input->post('user_name',TRUE),
                'pass' => password_hash($this->input->post('pass',TRUE), PASSWORD_DEFAULT),
				'id_user' => $this->input->post('id_user',TRUE),
			); 
            $this->Login_model->insert($data);
            $this->session->set_flashdata('success', 'Berhasil Tambah Data');
            redirect(site_url('login/data_login')); 
    }
    
    public function update($id) 
    {
        $row = $this->Login_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Ubah',
                'action' => site_url('login/update_action/'.$id),
				'id' => set_value('id', $row->id),
				'user_name' => set_value('user_name', $row->user_name),
				'pass' => set_value('pass', $row->pass),
				'id_user' => set_value('id_user', $row->id_user),
			);
            $this->template->load('template_antrian','login/login_formm', $data);
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('login'));
        }
    }
    
    public function update_action($id) 
    {
        $row = $this->Login_model->get_by_id($id);
            if (password_verify($this->input->post('pass',TRUE), $row->password)) {
                    $data = array(
                        'user_name' => $this->input->post('user_name',TRUE),
                    );
            }else{
                if($this->input->post('pass',TRUE)!=null){
                    $data = array(
                        'user_name' => $this->input->post('user_name',TRUE),
                        'pass' => password_hash($this->input->post('pass',TRUE), PASSWORD_DEFAULT), 
                    );

                }else{
                    $data = array(
                        'user_name' => $this->input->post('user_name',TRUE),
                        'pass' => password_hash($this->input->post('pass',TRUE), PASSWORD_DEFAULT),
                        'id_user' => $this->input->post('id_user',TRUE),  
                    );
                }
            }

            $this->Login_model->update($id, $data);
            $this->session->set_flashdata('success', 'Berhasil Edit Data');
            redirect(site_url('login')); 
    }
    public function update_pass() 
    {
        $row = $this->Login_model->get_by_user($this->input->post('user_name'));
        print_r($row);
            if (password_verify($this->input->post('password_lama',TRUE), $row->pass)) {
                // echo "pass cocok";
                // $data = array(
                //     'user_name' => $this->input->post('user_name',TRUE),
                // );
                if($this->input->post('password_baru',TRUE)!=null){
                    $data = array( 
                        'pass' => password_hash($this->input->post('password_baru',TRUE), PASSWORD_DEFAULT), 
                    );
                }else{
                    redirect(site_url('tabel_pegawai/dashbord')); 
                }
            }else{
                echo "pass belum cocok";
                // redirect(site_url('tabel_pegawai/dashbord')); 
            }
            // print_r($data);die();
            $this->Login_model->update($row->id, $data);
            $this->session->set_flashdata('success', 'Berhasil Edit Data');
            redirect(site_url('login')); 
    }
    
    public function delete($id) 
    {
        $row = $this->Login_model->get_by_id($id);

        if ($row) {
            $this->Login_model->delete($id);
            $this->session->set_flashdata('success', 'Berhasil Hapus Data');
            redirect(site_url('login'));
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('login'));
        }
    }

    public function _rules() 
    {
		$this->form_validation->set_rules('user_name', 'user_name', 'trim|required');
		$this->form_validation->set_rules('pass', 'pass', 'trim|required');
		$this->form_validation->set_rules('id_user', 'id user', 'trim|required');

		$this->form_validation->set_rules('', '', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "login.xls";
        $judul = "login";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "user_name");
	xlsWriteLabel($tablehead, $kolomhead++, "Password");
	xlsWriteLabel($tablehead, $kolomhead++, "Id User");

	foreach ($this->Login_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->user_name);
	    xlsWriteLabel($tablebody, $kolombody++, $data->password);
	    xlsWriteNumber($tablebody, $kolombody++, $data->id_user);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Login.php */
/* Location: ./application/controllers/Login.php */
/* Please DO NOT modify this information : */
/* Generated by CRUDV2 Generator For AdminLTE Template 2023-07-04 09:17:21 */