<?php
// print_r($this->session->userdata());die(); 
$nama=''; 
if($this->session->userdata('username')){
  $dataa=$this->db->query("
    select nip,id_peg,nama from tabel_pegawai where id_peg = ".$this->session->userdata('id_user')."
    ")->row();
  // ('tabel_pegawai')
  //         ->where('id_peg', '=',$this->session->userdata('id_user'))
  //         ->select('*')
  //         ->orderBy('id_peg', 'asc')
  //         ->first(); 
  $data['nip']=$dataa->nip; 
  if($dataa){
    $nama = $dataa->nama;
  }
}else{
  redirect(site_url('login'));
}
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">

  <title>Kab.Cilacap</title>
  <meta content="" name="description">
  <meta content="" name="keywords">

  <!-- Favicons -->
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  <!-- Google Fonts -->
  <link href="https://fonts.gstatic.com" rel="preconnect">
  <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i|Nunito:300,300i,400,400i,600,600i,700,700i|Poppins:300,300i,400,400i,500,500i,600,600i,700,700i" rel="stylesheet">

  <!-- aplikasi_pokja CSS Files -->
  <link href="<?=site_url()?>./assets/aplikasi_pokja/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <link href="<?=site_url()?>./assets/aplikasi_pokja/bootstrap-icons/bootstrap-icons.css" rel="stylesheet">
  <link href="<?=site_url()?>./assets/aplikasi_pokja/boxicons/css/boxicons.min.css" rel="stylesheet">
  <link href="<?=site_url()?>./assets/aplikasi_pokja/quill/quill.snow.css" rel="stylesheet">
  <link href="<?=site_url()?>./assets/aplikasi_pokja/quill/quill.bubble.css" rel="stylesheet">
  <link href="<?=site_url()?>./assets/aplikasi_pokja/remixicon/remixicon.css" rel="stylesheet">
  <link href="<?=site_url()?>./assets/aplikasi_pokja/simple-datatables/style.css" rel="stylesheet">

  <!-- Template Main CSS File -->
  <link href="<?=site_url()?>./assets/css/style.css" rel="stylesheet">

  <!-- =======================================================
  * Template Name: NiceAdmin
  * Updated: Nov 17 2023 with Bootstrap v5.3.2
  * Template URL: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/
  * Author: BootstrapMade.com
  * License: https://bootstrapmade.com/license/
  ======================================================== -->
</head>

<body>

  <!-- ======= Header ======= -->
  <header id="header" class="header fixed-top d-flex align-items-center">

    <div class="d-flex align-items-center justify-content-between">
      <a href="<?=site_url()?>" class="logo d-flex align-items-center">
        <img src="<?=site_url()?>./assets/img/logo.png" alt="">
        <span class="d-none d-lg-block">Kab.Cilacap</span>
      </a>
      <i class="bi bi-list toggle-sidebar-btn"></i>
    </div><!-- End Logo --> 

    <nav class="header-nav ms-auto">
      <ul class="d-flex align-items-center">    
        <li class="nav-item dropdown pe-3"> 
          <a class="nav-link nav-profile d-flex align-items-center pe-0" href="#" data-bs-toggle="dropdown">
            <img src="<?=site_url()?>./assets/img/person.png" alt="Profile" class="rounded-circle">
            <span class="d-none d-md-block dropdown-toggle ps-2"><?=$nama?></span>
          </a><!-- End Profile Iamge Icon -->

          <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow profile">
            <li class="dropdown-header">
              <h6>Nama Anda</h6>
              <span>Administrator</span>
            </li>
            <li>
              <hr class="dropdown-divider">
            </li>

            <li>
              <a class="dropdown-item d-flex align-items-center" href="<?=site_url('#')?>">
                <i class="bi bi-person"></i>
                <span>My Profile</span>
              </a>
            </li>
            <li>
              <hr class="dropdown-divider">
            </li>

            <!-- <li>
              <a class="dropdown-item d-flex align-items-center" href="users-profile.html">
                <i class="bi bi-gear"></i>
                <span>Account Settings</span>
              </a>
            </li> -->
            <li>
              <hr class="dropdown-divider">
            </li>

            <!-- <li>
              <a class="dropdown-item d-flex align-items-center" href="pages-faq.html">
                <i class="bi bi-question-circle"></i>
                <span>Need Help?</span>
              </a>
            </li>
            <li>
              <hr class="dropdown-divider">
            </li> -->

            <li>
              <a class="dropdown-item d-flex align-items-center" href="<?=site_url('logout')?>">
                <i class="bi bi-box-arrow-right"></i>
                <span>Sign Out</span>
              </a>
            </li>

          </ul><!-- End Profile Dropdown Items -->
        </li><!-- End Profile Nav -->

      </ul>
    </nav><!-- End Icons Navigation -->

  </header><!-- End Header -->

  <!-- ======= Sidebar ======= -->
  <aside id="sidebar" class="sidebar">

    <ul class="sidebar-nav" id="sidebar-nav">

      <!-- <li class="nav-item">
        <a class="nav-link " href="<?=site_url()?>">
          <i class="bi bi-grid"></i>
          <span>Dashboard</span>
        </a>
      </li> -->
      <!-- End Dashboard Nav -->
     <?php 
        $dataa=$this->db->query("select parent,id,menu,url from tabel_menu where parent = 'parent'")->result();
        /*('tabel_menu')
                ->where('parent', '=', 'parent')
                ->select('*')
                ->orderBy('id', 'asc')
                // ->first();
                ->get();*/
        foreach($dataa as $dt){?>
      <li class="nav-item">
        <a class="nav-link collapsed" data-bs-target="#components-nav<?=$dt->id?>" data-bs-toggle="collapse" href="#">
          <i class="bi bi-menu-button-wide"></i><span><?=$dt->menu?></span><i class="bi bi-chevron-down ms-auto"></i>
        </a>
        <ul id="components-nav<?=$dt->id?>" class="nav-content collapse " data-bs-parent="#sidebar-nav">
        <?php 
        $dataa2=$this->db->query("select parent,menu,url from tabel_menu where parent !='parent'")->result();/*\DB::table('tabel_menu')
                ->where('parent', '=', $dt->id)
                ->select('*')
                ->orderBy('menu', 'asc')
                // ->first();
                ->get();*/
        foreach($dataa2 as $dtt){?>
          <li>
            <a href="<?=$dtt->url?>">
              <i class="bi bi-circle"></i><span><?=$dtt->menu?></span>
            </a>
          </li>
        <?php }?>
        </ul>
      </li>
      <?php }?>
    </ul>

  </aside><!-- End Sidebar-->

  <main id="main" class="main">

    <div class="pagetitle">
      <h1><?php 
      $urlku=base_url(); $seg=explode('/', $urlku);//print_r($seg);
      $a=0;
      $jml=count($seg);
      $max=$jml-1;
      // echo $max;
      foreach($seg as $sg){ 
        if($sg=="127.0.0.1:8000"){
          if($a==$max){echo strtoupper('Beranda');}
        }else{
          if($a==$max){echo strtoupper(str_replace('_',' ',$sg));}
        }
        $a++;
      }
    ?> </h1>
      <nav>
        <ol class="breadcrumb">
          <?php
          $b=0;
            foreach($seg as $sg){
            if($sg=="127.0.0.1:8000"){ 
              if($b==$max){
                echo '<li class="breadcrumb-item active">'.strtoupper('Beranda').'</li>';
              }else if($b>2){
                  echo '<li class="breadcrumb-item"><a href="#">'.strtoupper('Beranda').'</a></li>';
                }
              }
            // }
            else{ 
              if($b==$max){
                echo '<li class="breadcrumb-item active">'.strtoupper(str_replace('_',' ',$sg)).'</li>';
              }else if($b>2){
                  echo '<li class="breadcrumb-item"><a href="#">'.strtoupper(str_replace('_',' ',$sg)).'</a></li>';
                }
              }
              $b++;
            }?> 
        </ol>
      </nav>
    </div><!-- End Page Title -->

    <section class="section dashboard">
      <div class="row">

        <!-- Left side columns -->
        <div class="col-lg-12">
          <div class="row">  
             <?= $contents ?> 

        </div><!-- End Right side columns -->

      </div>
    </section>

  </main><!-- End #main -->

  <!-- ======= Footer ======= -->
  <footer id="footer" class="footer">
    <div class="copyright">
      &copy; Copyright <strong><span>Pemerintah Kabupaten Cilacap</span>&nbsp;<?=date('Y')?></strong>. All Rights Reserved
    </div>
    <div class="credits">
      <!-- All the links in the footer should remain intact. -->
      <!-- You can delete the links only if you purchased the pro version. -->
      <!-- Licensing information: https://bootstrapmade.com/license/ -->
      <!-- Purchase the pro version with working PHP/AJAX contact form: https://bootstrapmade.com/nice-admin-bootstrap-admin-html-template/ -->
      Designed by <a href="https://kominfo.cilacapkab.go.id/">Dinas Komunikasi dan Informatika Kab.Cilacap</a>
    </div>
  </footer><!-- End Footer -->

  <a href="#" class="back-to-top d-flex align-items-center justify-content-center"><i class="bi bi-arrow-up-short"></i></a>

  <!-- aplikasi_pokja JS Files -->
  <script src="<?=site_url()?>./assets/aplikasi_pokja/apexcharts/apexcharts.min.js"></script>
  <script src="<?=site_url()?>./assets/aplikasi_pokja/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="<?=site_url()?>./assets/aplikasi_pokja/chart.js/chart.umd.js"></script>
  <script src="<?=site_url()?>./assets/aplikasi_pokja/echarts/echarts.min.js"></script>
  <script src="<?=site_url()?>./assets/aplikasi_pokja/quill/quill.min.js"></script>
  <script src="<?=site_url()?>./assets/aplikasi_pokja/simple-datatables/simple-datatables.js"></script>
  <script src="<?=site_url()?>./assets/aplikasi_pokja/tinymce/tinymce.min.js"></script>
  <script src="<?=site_url()?>./assets/aplikasi_pokja/php-email-form/validate.js"></script>

  <!-- Template Main JS File -->
  <script src="<?=site_url()?>./assets/js/main.js"></script>

</body>

</html>