<div class="row"> 
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <div class="box-title">
                    <b> Data Tbl_user_menu</b>
                </div> 
            </div>
            <div class="box-body">
                <a href="<?= site_url('tbl_pengguna') ?>" class="btn btn-danger" style="margin-left : 15px"><i class="fa fa-arrow-left"></i> Kembali</a>
                <div class="table-responsive" style="padding: 15px">
                    <table class="table table-bordered table-striped table-hover" width="100%" id="mytable">
                        <thead>
                            <tr>
                                <th width="5%">No</th>
								<th>Id User</th>
								<th>Id Menu</th>
								<th width="15%">Aksi</th>
                            </tr>
                        </thead>    
                    </table>

                    <script src="<?= base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
                    <script type="text/javascript">
                        $(document).ready(function() {
                            $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
                            {
                                return {
                                    "iStart": oSettings._iDisplayStart,
                                    "iEnd": oSettings.fnDisplayEnd(),
                                    "iLength": oSettings._iDisplayLength,
                                    "iTotal": oSettings.fnRecordsTotal(),
                                    "iFilteredTotal": oSettings.fnRecordsDisplay(),
                                    "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                                    "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                                };
                            };

                            var t = $("#mytable").dataTable({
                                "processing"  : true,
                                "serverSide"  : true,
                                "oLanguage"   : { sProcessing : "Loading. . ." },
                                "ajax"        : { "url" : "<?= site_url('tbl_user_menu/json_user/'.$this->uri->segment('3')) ?>", "type": "POST"},
                                "columns"     : [
                                    {
                                        "data": "id",
                                        "orderable": false,
                                        "className" : "text-center"
                                    },
									{
										"data": "nama"
									},
									{
										"data": "menu"
									},
                                    {
                                        "data" : "action",
                                        "orderable": false,
                                        "className" : "text-center",
                                        render: function (data, type, row) { 
                                            var edit=" <a class='btn btn-warning' href='<?=site_url('Tbl_user_menu/update/')?>"+row.id+"' title='Ubah akses menu'><i class='fa fa-edit'></i></a>";
                                            var hapus=" <a class='btn btn-danger' href='<?=site_url('Tbl_user_menu/delete/')?>"+row.id+"' title='Ubah akses menu'><i class='fa fa-trash'></i></a>";
                                            return edit+hapus;
                                        }
                                    }
                                ],
                                order: [[0, 'desc']],
                                rowCallback: function(row, data, iDisplayIndex) {
                                    var info = this.fnPagingInfo();
                                    var page = info.iPage;
                                    var length = info.iLength;
                                    var index = page * length + (iDisplayIndex + 1);
                                    $('td:eq(0)', row).html(index);
                                }
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>
<?php // swal_delete("#mytable",".hapus") ?>
		<?= anchor(site_url('tbl_user_menu/excel'), '<i class="fa fa-file-excel-o" aria-hidden="true"></i> Export Excel', 'class="btn btn-success"'); ?>