<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tbl_user_menu extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Tbl_user_menu_model');
        $this->load->library('form_validation');
		$this->load->library('datatables');
    }

    public function index()
    {
        $this->template->load('template_antrian','tbl_user_menu/tbl_user_menu_list');
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Tbl_user_menu_model->json();
    }
    
    public function json_user($id) {
        header('Content-Type: application/json'); 
        echo $this->Tbl_user_menu_model->json_user($id);
    }

    public function read($id) 
    {
        $row = $this->Tbl_user_menu_model->get_by_id($id);
        if ($row) {
            $data = array(
				'id' => $row->id,
				'id_user' => $row->id_user,
				'id_menu' => $row->id_menu,
                'kembali' => 'tbl_user_menu',
			);
            $this->template->load('template_antrian','tbl_user_menu/tbl_user_menu_read', $data);
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('tbl_user_menu'));
        }
    }

    public function readd($id) 
    {
        $row = $this->Tbl_user_menu_model->get_by_id($id);
        if ($row) {
            $data = array(
                'id' => $row->id,
                'id_user' => $row->id_user,
                'id_menu' => $row->id_menu,
                'kembali' => 'tabel_pegawai/akses_menu/'.$row->id_user,
            );
            $this->template->load('template_antrian','tbl_user_menu/tbl_user_menu_read', $data);
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('tabel_pegawai/akses_menu/'.$row->id_user));  
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Tambah',
            'action' => site_url('tbl_user_menu/create_action'),
			'id' => set_value('id'),
			'id_user' => set_value('id_user'),
			'id_menu' => set_value('id_menu'),
		);
        $this->template->load('template_antrian','tbl_user_menu/tbl_user_menu_form', $data);
    }
    public function create_($id_user=null) 
    {
        $data = array(
            'button' => 'Tambah',
            'action' => site_url('tbl_user_menu/create_action_'),
            'id' => set_value('id'),
            'id_user' => $id_user,
            'id_menu' => set_value('id_menu'),
        );
        $this->template->load('template_antrian','tbl_user_menu/tbl_user_menu_form', $data);
    }
    
    public function create_action() 
    { 
            $data = array(
				'id_user' => $this->input->post('id_user',TRUE),
				'id_menu' => $this->input->post('id_menu',TRUE),
			); 
            $this->Tbl_user_menu_model->insert($data);
            $this->session->set_flashdata('success', 'Berhasil Tambah Data');
            redirect(site_url('tbl_user_menu')); 
    }
    public function create_action_() 
    { 
            $data = array(
                'id_user' => $this->input->post('id_user',TRUE),
                'id_menu' => $this->input->post('id_menu',TRUE),
            ); 
            $this->Tbl_user_menu_model->insert($data);
            $this->session->set_flashdata('success', 'Berhasil Tambah Data');
            redirect(site_url('tabel_pegawai/akses_menu/'.$this->input->post('id_user',TRUE))); 
    }
    
    public function update($id) 
    {
        $row = $this->Tbl_user_menu_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Ubah',
                'action' => site_url('tbl_user_menu/update_action/'.$id),
				'id' => set_value('id', $row->id),
				'id_user' => set_value('id_user', $row->id_user),
				'id_menu' => set_value('id_menu', $row->id_menu),
			);
            $this->template->load('template_antrian','tbl_user_menu/tbl_user_menu_form', $data);
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('tbl_user_menu'));
        }
    }
    
    public function updatee($id) 
    {
        $row = $this->Tbl_user_menu_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Ubah',
                'action' => site_url('tbl_user_menu/update_actionn/'.$id),
                'id' => set_value('id', $row->id),
                'id_user' => set_value('id_user', $row->id_user),
                'id_menu' => set_value('id_menu', $row->id_menu),
            );
            $this->template->load('template_antrian','tbl_user_menu/tbl_user_menu_form', $data);
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('tabel_pegawai/akses_menu/'.$row->id_user));  
        }
    }
    
    public function update_action($id) 
    { 
            $data = array(
				'id_user' => $this->input->post('id_user',TRUE),
				'id_menu' => $this->input->post('id_menu',TRUE),
			);

            $this->Tbl_user_menu_model->update($id, $data);
            $this->session->set_flashdata('success', 'Berhasil Edit Data');
            redirect(site_url('tbl_user_menu')); 
    }
    
    public function update_actionn($id) 
    { 
            $data = array(
                'id_user' => $this->input->post('id_user',TRUE),
                'id_menu' => $this->input->post('id_menu',TRUE),
            );

            $this->Tbl_user_menu_model->update($id, $data);
            $this->session->set_flashdata('success', 'Berhasil Edit Data');
            redirect(site_url('tabel_pegawai/akses_menu/'.$this->input->post('id_user',TRUE)));  
    }
    
    public function delete($id) 
    {
        $row = $this->Tbl_user_menu_model->get_by_id($id);

        if ($row) {
            $this->Tbl_user_menu_model->delete($id);
            $this->session->set_flashdata('success', 'Berhasil Hapus Data');
            redirect(site_url('tbl_user_menu'));
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('tbl_user_menu'));
        }
    }
    
    public function deletee($id) 
    {
        $row = $this->Tbl_user_menu_model->get_by_id($id);

        if ($row) {
            $this->Tbl_user_menu_model->delete($id);
            $this->session->set_flashdata('success', 'Berhasil Hapus Data');
            redirect(site_url('tabel_pegawai/akses_menu/'.$row->id_user));  
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('tabel_pegawai/akses_menu/'.$row->id_user));  
        }
    }

    public function _rules() 
    {
		$this->form_validation->set_rules('id_user', 'id user', 'trim|required');
		$this->form_validation->set_rules('id_menu', 'id menu', 'trim|required');

		$this->form_validation->set_rules('', '', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "tbl_user_menu.xls";
        $judul = "tbl_user_menu";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Id User");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Menu");

	foreach ($this->Tbl_user_menu_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteNumber($tablebody, $kolombody++, $data->id_user);
	    xlsWriteNumber($tablebody, $kolombody++, $data->id_menu);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Tbl_user_menu.php */
/* Location: ./application/controllers/Tbl_user_menu.php */
/* Please DO NOT modify this information : */
/* Generated by CRUDV2 Generator For AdminLTE Template 2024-01-09 09:04:36 */