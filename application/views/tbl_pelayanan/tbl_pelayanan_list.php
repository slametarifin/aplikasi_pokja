<div class="row"> 
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <div class="box-title">
                    <b><i class="fa fa-list-alt"></i> Data Tbl_pelayanan</b>
                </div> 
            </div>
            <div class="box-body">
                <a href="<?= site_url('tbl_pelayanan/create') ?>" class="btn btn-primary" style="margin-left : 15px"><i class="fa fa-plus"></i> Tambah Data</a>
                <div class="table-responsive" style="padding: 15px">
                    <table class="table table-bordered table-striped table-hover" width="100%" id="mytable">
                        <thead>
                            <tr>
                                <th width="5%">No</th>
								<th>Nama Instansi</th>
								<th>Nama Layanan</th>
								<th>Grup Layanan</th>
								<th>Parent</th>
								<th width="15%">Aksi</th>
                            </tr>
                        </thead>    
                    </table>
                    <?php 
                    $role='';
                    $data_role=$this->db->query("select role from Tbl_pengguna where id=".$this->session->userdata('id_user')."")->row();
                    if($data_role){
                        $role=$data_role->role;
                    }
                    ?>

                    <script src="<?= base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
                    <script type="text/javascript">
                        $(document).ready(function() {
                            $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
                            {
                                return {
                                    "iStart": oSettings._iDisplayStart,
                                    "iEnd": oSettings.fnDisplayEnd(),
                                    "iLength": oSettings._iDisplayLength,
                                    "iTotal": oSettings.fnRecordsTotal(),
                                    "iFilteredTotal": oSettings.fnRecordsDisplay(),
                                    "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                                    "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                                };
                            };

                            var t = $("#mytable").dataTable({
                                "processing"  : true,
                                "serverSide"  : true,
                                "oLanguage"   : { sProcessing : "Loading. . ." },
                                "ajax"        : { "url" : "<?= site_url('tbl_pelayanan/json') ?>", "type": "POST"},
                                "columns"     : [
                                    {
                                        "data": "id",
                                        "orderable": false,
                                        "className" : "text-center"
                                    }, 
									{
										"data": "id_instansi",
                                        render: function (data, type, row) {
                                            return row.nm_instansi;
                                        }
									},
									{
										"data": "nm_layanan"
									},
									{
										"data": "nm_layanan",
                                        render: function (data, type, row) {
                                            return row.nm_grup; 
                                        }
									},
									{
										"data": "parent"
									},
                                    {
                                        "data" : "action",
                                        "orderable": false,
                                        "className" : "text-center",
                                        // render: function (data, type, row) {
                                        //     return '-';
                                        //     var role='';
                                        //     var user="<?=$role?>"; 
                                            
                                        //     return row.action;
                                        //     // if(user){
                                        //     //     role=user;
                                        //     // }else{
                                        //     //     role ='';
                                        //     // }
                                        //     // if(role==2){
                                        //     //     return row.action;
                                        //     // }else{
                                        //     //     return '-';
                                        //     // }
                                        // }
                                    }
                                ],
                                order: [[0, 'desc']],
                                rowCallback: function(row, data, iDisplayIndex) {
                                    var info = this.fnPagingInfo();
                                    var page = info.iPage;
                                    var length = info.iLength;
                                    var index = page * length + (iDisplayIndex + 1);
                                    $('td:eq(0)', row).html(index);
                                }
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>
<?php // swal_delete("#mytable",".hapus") ?>
		<?= anchor(site_url('tbl_pelayanan/excel'), '<i class="fa fa-file-excel-o" aria-hidden="true"></i> Export Excel', 'class="btn btn-success"'); ?>