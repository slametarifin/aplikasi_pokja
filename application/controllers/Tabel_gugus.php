<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tabel_gugus extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Tabel_gugus_model');
        $this->load->library('form_validation');
		$this->load->library('datatables');
    }

    public function index()
    {
        $cek_role=$this->db->query("select id,role,user_name from tabel_akun where user_name='".$this->session->userdata('username')."'")->row();
        if($cek_role->role==2||$cek_role->role==1){
            $display="block";
        }elseif($cek_role->role==0){
            $display="none";
        }
        $data=array('id_user'=>$cek_role->id,'akses'=>$cek_role->role,'display'=>$display);
        $this->template->load('template_antrian','tabel_gugus/tabel_gugus_list',$data);
    } 
    public function opd($id_opd=null)
    {
        $cek_role=$this->db->query("select id,role,user_name from tabel_akun where user_name='".$this->session->userdata('username')."'")->row();
        if($cek_role->role==2||$cek_role->role==1){
            $display="block";
        }elseif($cek_role->role==0){
            $display="none";
        }
        $data=array('id_opd'=>$id_opd,'id_user'=>$cek_role->id,'akses'=>$cek_role->role,'display'=>$display);
        $this->template->load('template_antrian','tabel_gugus/tabel_gugus_list_opd',$data);
    } 
    public function tugas_gugus($id=null)
    {
            $this->template->load('template_antrian','tabel_tugas_gugus/tabel_tugas_gugus_listt');
        // if($id==null||$id==''){ redirect(site_url('tabel_gugus'));}
        // $data=array('id_tugas'=>$id);
        // $this->template->load('template_antrian','tabel_gugus/tabel_gugus_list/');
    } 
    
    public function json() {
        header('Content-Type: application/json');
        $cek_role=$this->db->query("select (select id_opd from tabel_pegawai where id_peg=id_user)id_opd,id,role,user_name from tabel_akun where user_name='".$this->session->userdata('username')."'")->row();
        if($cek_role->role==2||$cek_role->role==1){
            $display="block";
        }elseif($cek_role->role==0){
            $display="none";
        }
        $data=array('id_user'=>$cek_role->id,'akses'=>$cek_role->role,'display'=>$display);
        if($cek_role->role==2){
            echo $this->Tabel_gugus_model->json();
        }elseif($cek_role->role==0||$cek_role->role==1){
            echo $this->Tabel_gugus_model->json_opd($cek_role->id_opd);            
        }
    }
    
    public function json_opd($id_opd=null) {
        header('Content-Type: application/json');
        // $cek_role=$this->db->query("select (select id_opd from tabel_pegawai where id_peg=id_user)id_opd,id,role,user_name from tabel_akun where user_name='".$this->session->userdata('username')."'")->row();
        // if($cek_role->role==2||$cek_role->role==1){
        //     $display="block";
        // }elseif($cek_role->role==0){
        //     $display="none";
        // }
        // $data=array('id_user'=>$cek_role->id,'akses'=>$cek_role->role,'display'=>$display);
        // if($cek_role->role==2){
        //     echo $this->Tabel_gugus_model->json();
        // }elseif($cek_role->role==0||$cek_role->role==1){
            echo $this->Tabel_gugus_model->json_opd($id_opd);            
        // }
    }

    public function read($id) 
    {
        $row = $this->Tabel_gugus_model->get_by_id($id);
        if ($row) {
            $data = array(
				'id' => $row->id,
				'nama_gugus' => $row->nama_gugus,
				'nip_ketua' => $row->nip_ketua,
				'periode_awal' => $row->periode_awal,
				'periode_akhir' => $row->periode_akhir,
				'status' => $row->status,
				'kode_opd' => $row->kode_opd,
			);
            $this->template->load('template_antrian','tabel_gugus/tabel_gugus_read', $data);
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('tabel_gugus'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Tambah',
            'action' => site_url('tabel_gugus/create_action'),
			'id' => set_value('id'),
			'nama_gugus' => set_value('nama_gugus'),
			'nip_ketua' => set_value('nip_ketua'),
			'periode_awal' => set_value('periode_awal'),
			'periode_akhir' => set_value('periode_akhir'),
			'status' => set_value('status'),
			'kode_opd' => set_value('kode_opd'),
		);
        $this->template->load('template_antrian','tabel_gugus/tabel_gugus_form', $data);
    }
    
    public function create_action() 
    { 
            $data = array(
				'nama_gugus' => $this->input->post('nama_gugus',TRUE),
				'nip_ketua' => $this->input->post('nip_ketua',TRUE),
				'periode_awal' => $this->input->post('periode_awal',TRUE),
				'periode_akhir' => $this->input->post('periode_akhir',TRUE),
				'status' => 'true',
				'kode_opd' => $this->input->post('kode_opd',TRUE),
			);

            $this->Tabel_gugus_model->insert($data);
            $this->session->set_flashdata('success', 'Berhasil Tambah Data');
            redirect(site_url('tabel_gugus')); 
    }
    
    public function update($id) 
    {
        $row = $this->Tabel_gugus_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Ubah',
                'action' => site_url('tabel_gugus/update_action/'.$id),
				'id' => set_value('id', $row->id),
				'nama_gugus' => set_value('nama_gugus', $row->nama_gugus),
				'nip_ketua' => set_value('nip_ketua', $row->nip_ketua),
				'periode_awal' => set_value('periode_awal', $row->periode_awal),
				'periode_akhir' => set_value('periode_akhir', $row->periode_akhir),
				'status' => set_value('status', $row->status),
				'kode_opd' => set_value('kode_opd', $row->kode_opd),
			);
            $this->template->load('template_antrian','tabel_gugus/tabel_gugus_form', $data);
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('tabel_gugus'));
        }
    }
    
    public function update_action($id) 
    { 
            $data = array(
				'nama_gugus' => $this->input->post('nama_gugus',TRUE),
				'nip_ketua' => $this->input->post('nip_ketua',TRUE),
				'periode_awal' => $this->input->post('periode_awal',TRUE),
				'periode_akhir' => $this->input->post('periode_akhir',TRUE), 
				'kode_opd' => $this->input->post('kode_opd',TRUE),
			);

            $this->Tabel_gugus_model->update($id, $data);
            $this->session->set_flashdata('success', 'Berhasil Edit Data');
            redirect(site_url('tabel_gugus')); 
    }
    
    public function delete($id) 
    {
        $row = $this->Tabel_gugus_model->get_by_id($id);

        if ($row) {
            $this->Tabel_gugus_model->delete($id);
            $this->session->set_flashdata('success', 'Berhasil Hapus Data');
            redirect(site_url('tabel_gugus'));
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('tabel_gugus'));
        }
    }

    public function _rules() 
    {
		$this->form_validation->set_rules('nama_gugus', 'nama gugus', 'trim|required');
		$this->form_validation->set_rules('nip_ketua', 'nip ketua', 'trim|required');
		$this->form_validation->set_rules('periode_awal', 'periode awal', 'trim|required');
		$this->form_validation->set_rules('periode_akhir', 'periode akhir', 'trim|required');
		$this->form_validation->set_rules('status', 'status', 'trim|required');
		$this->form_validation->set_rules('kode_opd', 'kode opd', 'trim|required');

		$this->form_validation->set_rules('', '', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "tabel_gugus.xls";
        $judul = "tabel_gugus";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Nama Gugus");
	xlsWriteLabel($tablehead, $kolomhead++, "Nip Ketua");
	xlsWriteLabel($tablehead, $kolomhead++, "Periode Awal");
	xlsWriteLabel($tablehead, $kolomhead++, "Periode Akhir");
	xlsWriteLabel($tablehead, $kolomhead++, "Status");
	xlsWriteLabel($tablehead, $kolomhead++, "Kode Opd");

	foreach ($this->Tabel_gugus_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->nama_gugus);
	    xlsWriteLabel($tablebody, $kolombody++, $data->nip_ketua);
	    xlsWriteLabel($tablebody, $kolombody++, $data->periode_awal);
	    xlsWriteLabel($tablebody, $kolombody++, $data->periode_akhir);
	    xlsWriteLabel($tablebody, $kolombody++, $data->status);
	    xlsWriteNumber($tablebody, $kolombody++, $data->kode_opd);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Tabel_gugus.php */
/* Location: ./application/controllers/Tabel_gugus.php */
/* Please DO NOT modify this information : */
/* Generated by CRUDV2 Generator For AdminLTE Template 2024-01-08 08:30:09 */