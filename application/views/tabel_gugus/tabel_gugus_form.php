
  <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.0/dist/jquery.slim.min.js"></script>
  <div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header">
                <div class="box-title">
                    <b><i class="fa fa-tasks"></i> <?= $button ?> Data Tabel_gugus</b>
                </div>
                <div class="box-tools pull-right">
                    <button type="button" class="toggle-expand-btn btn btn-default btn-sm"><i class="fa fa-expand"></i></button>
                </div>
            </div>
            <div class="box-body">
                <form style="padding: 15px;" action="<?= $action; ?>" method="POST" enctype="multipart/form-data">
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="varchar">Nama Gugus</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="nama_gugus" id="nama_gugus" placeholder="Nama Gugus" value="<?= $nama_gugus; ?>" />
                                <?= form_error('nama_gugus') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="varchar">Nip Ketua</label>
                            <div class="col-md-6">
                                <select name="nip_ketua" class="form-control">
                                    <option value="-">pilih data</option>
                                    <?php 
                                    $peg=$this->db->query("select id_peg,nama,nama_opd,id_opd from tabel_pegawai join tabel_opd on tabel_opd.id = tabel_pegawai.id_opd")->result();
                                    foreach($peg as $pg){?>
                                        <option value="<?=$pg->id_peg?>" <?php if($nip_ketua==$pg->id_peg){?>selected <?php }?>><?=$pg->nama_opd?>|<?=$pg->nama?></option>
                                    <?php }?>
                                </select>
                                <!-- <input type="text" class="form-control" name="nip_ketua" id="nip_ketua" placeholder="Nip Ketua" value="<?= $nip_ketua; ?>" /> -->
                                <?= form_error('nip_ketua') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="varchar">Periode Awal</label>
                            <div class="col-md-6">
                                <input type="date" class="form-control" name="periode_awal" id="periode_awal" placeholder="Periode Awal" value="<?= $periode_awal; ?>" />
                                <?= form_error('periode_awal') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="varchar">Periode Akhir</label>
                            <div class="col-md-6">
                                <input type="date" class="form-control" name="periode_akhir" id="periode_akhir" placeholder="Periode Akhir" value="<?= $periode_akhir; ?>" />
                                <?= form_error('periode_akhir') ?>
                            </div>
                        </div>
                    </div> 
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="int">Kode Opd</label>
                            <div class="col-md-6">
                                <select name="kode_opd" class="form-control select2 form-select">
                                    <option value="-">pilih data</option>
                                    <?php 
                                    $peg=$this->db->query("select id,nama_opd from tabel_opd")->result();
                                    foreach($peg as $pg){?>
                                        <option value="<?=$pg->id?>" <?php if($kode_opd==$pg->id){?>selected <?php }?>><?=$pg->nama_opd?></option>
                                    <?php }?>
                                </select>
                                <!-- <input type="text" class="form-control" name="kode_opd" id="kode_opd" placeholder="Kode Opd" value="<?= $kode_opd; ?>" /> -->
                                <?= form_error('kode_opd') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-2">
                              
                                <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> <?= $button ?></button>
                                <a href="<?= site_url('tabel_gugus') ?>" class="btn btn-danger"><i class="fa fa-sign-out"></i> Kembali</a>
                            </div>
                        </div>
                    </div>
				</form>
            </div>
        </div>
    </div>
</div>