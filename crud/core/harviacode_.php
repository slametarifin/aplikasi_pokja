<?php

class Harviacode
{

    private $host;
    private $user;
    private $password;
    private $database;
    private $sql;

    function __construct()
    {
        $this->connection();
    }

    function connection()
    {
        $subject = file_get_contents('../application/config/database.php');
        $string = str_replace("defined('BASEPATH') OR exit('No direct script access allowed');", "", $subject);
        
        $con = 'core/connection.php';
        $create = fopen($con, "w") or die("Change your permision folder for application and harviacode folder to 777");
        fwrite($create, $string);
        fclose($create);
        
        require $con;

        $this->host = $db['default']['hostname'];
        $this->user = $db['default']['username'];
        $this->password = $db['default']['password'];
        $this->database = $db['default']['database'];
        $this->port = $db['default']['port'];

       // $this->sql = new pdo($this->host, $this->user, $this->password, $this->database);
        try {
            $this->sql = new PDO ("mysql:host=$this->host;port=$this->port;dbname=$this->database","$this->user","$this->password");
            // $this->sql = new PDO ("mysql:host=$this->host;dbname=$this->database","$this->user","$this->password");
            // $this->sql = new PDO ("mysqli:host=$this->host;dbname=$this->database","$this->user","$this->password");

        } catch (Exception $e) {

            echo $e->getMessage();

        }
        
        unlink($con);
    }

    function table_list()
    {
        $query = "select kcu.table_schema,
        kcu.table_name,
        tco.constraint_name,
        kcu.ordinal_position as position,
        kcu.column_name as key_column
        from information_schema.table_constraints tco
        join information_schema.key_column_usage kcu 
        on kcu.constraint_name = tco.constraint_name
        and kcu.constraint_schema = tco.constraint_schema
        and kcu.constraint_name = tco.constraint_name
        where tco.constraint_type = 'PRIMARY KEY'
        order by kcu.table_schema,
        kcu.table_name,
        position";
        $stmt = $this->sql->query($query);

        foreach ($stmt as $row) {
            $arr = array($row['table_schema'],$row['table_name']);
            $gabung= implode('.',$arr);
            $key=$row['key_column'];
            $value="$gabung-$key";

            $fields[] = array('table_name' => $value);
        }
        return $fields;
        $stmt->close();
        $this->sql->close();
    }

    function primary_field($table)
    {
        $query = "SELECT COLUMN_NAME,COLUMN_KEY FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_SCHEMA=? AND TABLE_NAME=? AND COLUMN_KEY = 'PRI'";
        $stmt = $this->sql->prepare($query) OR die("Error code :" . $this->sql->errno . " (primary_field)");
        $stmt->bind_param('ss', $this->database, $table);
        $stmt->bind_result($column_name, $column_key);
        $stmt->execute();
        $stmt->fetch();
        return $column_name;
        $stmt->close();
        $this->sql->close();
    }

    function not_primary_field($table)
    {
       $pecah=explode('.',$table);
       $pecahlagi=explode('-',$pecah[1]);
       $schema=$pecah[0];
       $nama_table=$pecahlagi[0];
       $pk=$pecahlagi[1];
       $query = "SELECT c.column_name, c.data_type, pgd.description
       from pg_catalog.pg_statio_all_tables as st
       inner join pg_catalog.pg_description pgd on (pgd.objoid=st.relid)
       right outer join information_schema.columns c on (pgd.objsubid=c.ordinal_position and  c.table_schema=st.schemaname and c.table_name=st.relname)
       where table_schema ='$schema' and table_name ='$nama_table' and c.column_name !='$pk'";
       $stmt = $this->sql->query($query);
       foreach($stmt as $row) {
        $fields[] = array('column_name' => $row['column_name'], 'column_key' => $pecahlagi[1], 'data_type' => $row['data_type']);
    }
    return $fields;
    $stmt->close();
    $this->sql->close();
}

function all_field($table)
{   
    $pecah=explode('.',$table);
    $pecahlagi=explode('-',$pecah[1]);
    $schema=$pecah[0];
    $nama_table=$pecahlagi[0];
    $query = "SELECT c.column_name, c.data_type, pgd.description
    from pg_catalog.pg_statio_all_tables as st
    inner join pg_catalog.pg_description pgd on (pgd.objoid=st.relid)
    right outer join information_schema.columns c on (pgd.objsubid=c.ordinal_position and  c.table_schema=st.schemaname and c.table_name=st.relname)
    where table_schema ='$schema' and table_name ='$nama_table'";
    $stmt = $this->sql->query($query);
    foreach($stmt as $row) {
        $fields[] = array('column_name' => $row['column_name'], 'column_key' => $pecahlagi[1], 'data_type' => $row['data_type']);
    }
    return $fields;
    $stmt->close();
    $this->sql->close();
}

}

$hc = new Harviacode();
?>