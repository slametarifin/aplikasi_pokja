<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tabel_atasan_bawahan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Tabel_atasan_bawahan_model');
        $this->load->model('Tabel_pegawai_model');
        $this->load->library('form_validation');
		$this->load->library('datatables');
    }

    public function instansi()
    {
        // echo "lanjuttttt....";
        // die();
        $cek_role=$this->db->query("select (select id_opd from tabel_pegawai where id_peg=id_user)id_opd,id,role,user_name from tabel_akun where user_name='".$this->session->userdata('username')."'")->row();
        if($cek_role->role==2){
            $display="block";
        }elseif($cek_role->role==0||$cek_role->role==1){
            $display="none";
        }
        $data=array('id_opd'=>$cek_role->id_opd,'id_user'=>$cek_role->id,'akses'=>$cek_role->role,'display'=>$display);
        $this->template->load('template_antrian','tabel_atasan_bawahan/tabel_atasan_bawahan_listtt',$data);
    }

    public function index()
    {
        $cek_role=$this->db->query("select id,role,user_name from tabel_akun where user_name='".$this->session->userdata('username')."'")->row();
        if($cek_role->role==2){
            $display="block";
        }elseif($cek_role->role==0||$cek_role->role==1){
            $display="none";
        }
        $data=array('id_user'=>$cek_role->id,'akses'=>$cek_role->role,'display'=>$display);
        $this->template->load('template_antrian','tabel_atasan_bawahan/tabel_atasan_bawahan_list',$data);
    } 
    
    public function jsonn($id_opd=null) {
        header('Content-Type: application/json');
        echo $this->Tabel_atasan_bawahan_model->json_opd($id_opd);

    }
    
    public function json() {
        header('Content-Type: application/json');
        $cek_role=$this->db->query("select (select id_opd from tabel_pegawai where tabel_pegawai.id_peg=tabel_akun.id_user)id_opd,id,role,user_name from tabel_akun where user_name='".$this->session->userdata('username')."'")->row();
        if($cek_role->role==2){
            // $display="block";
            echo $this->Tabel_atasan_bawahan_model->json();
        }elseif($cek_role->role==1){
            // $display="none";
            echo $this->Tabel_atasan_bawahan_model->json_opd($cek_role->id_opd);
        }elseif($cek_role->role==0){
            // $display="none";            
            echo $this->Tabel_atasan_bawahan_model->json_limit1($cek_role->id_user);
        }
    }

    public function read($id) 
    {
        $row = $this->Tabel_atasan_bawahan_model->get_by_id($id);
        if ($row) {
            $data = array(
				'id' => $row->id,
				'id_atasan' => $row->id_atasan,
				'id_staff' => $row->id_staff,
				'id_opd' => $row->id_opd,
			);
            $this->template->load('template_antrian','tabel_atasan_bawahan/tabel_atasan_bawahan_read', $data);
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('tabel_atasan_bawahan'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Tambah',
            'action' => site_url('tabel_atasan_bawahan/create_action'),
			'id' => set_value('id'),
			'id_atasan' => set_value('id_atasan'),
			'id_staff' => set_value('id_staff'),
			'id_opd' => set_value('id_opd'),
		);
        $this->template->load('template_antrian','tabel_atasan_bawahan/tabel_atasan_bawahan_form', $data);
    }

    public function tambah_staff($id=null) 
    {
        $row = $this->Tabel_pegawai_model->get_by_id($id);
        $data = array(
            'button' => 'Tambah',
            'action' => site_url('tabel_atasan_bawahan/create_action'),
            'id' => set_value('id'),
            'id_atasan' => set_value('id_atasan'),
            'id_staff' => set_value('id_staff'),
            // 'id_opd' => set_value('id_opd'), 
            'kd_atasan' => $id, 
            'id_opd'    => $row->id_opd,
        );
        $this->template->load('template_antrian','tabel_atasan_bawahan/tabel_atasan_bawahan_form', $data);
    }
    
    public function create_action() 
    { 
            $data = array(
				'id_atasan' => $this->input->post('id_atasan',TRUE),
				'id_staff' => $this->input->post('id_staff',TRUE),
				'id_opd' => $this->input->post('id_opd',TRUE),
			);

            $this->Tabel_atasan_bawahan_model->insert($data);
            $this->session->set_flashdata('success', 'Berhasil Tambah Data');
            redirect(site_url('tabel_pegawai')); 
    }
    
    public function update($id) 
    {
        $row = $this->Tabel_atasan_bawahan_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Ubah',
                'action' => site_url('tabel_atasan_bawahan/update_action/'.$id),
				'id' => set_value('id', $row->id),
				'id_atasan' => set_value('id_atasan', $row->id_atasan),
				'id_staff' => set_value('id_staff', $row->id_staff),
				'id_opd' => set_value('id_opd', $row->id_opd),
                'kd_atasan' => set_value('id_atasan', $row->id_atasan), 
			);
            $this->template->load('template_antrian','tabel_atasan_bawahan/tabel_atasan_bawahan_form', $data);
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('tabel_atasan_bawahan'));
        }
    }
    
    public function update_action($id) 
    { 
            $data = array(
				'id_atasan' => $this->input->post('id_atasan',TRUE),
				'id_staff' => $this->input->post('id_staff',TRUE),
				'id_opd' => $this->input->post('id_opd',TRUE),
			);

            $this->Tabel_atasan_bawahan_model->update($id, $data);
            $this->session->set_flashdata('success', 'Berhasil Edit Data');
            redirect(site_url('tabel_atasan_bawahan')); 
    }
    
    public function delete($id) 
    {
        $row = $this->Tabel_atasan_bawahan_model->get_by_id($id);

        if ($row) {
            $this->Tabel_atasan_bawahan_model->delete($id);
            $this->session->set_flashdata('success', 'Berhasil Hapus Data');
            redirect(site_url('tabel_atasan_bawahan'));
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('tabel_atasan_bawahan'));
        }
    }

    public function _rules() 
    {
		$this->form_validation->set_rules('id_atasan', 'id atasan', 'trim|required');
		$this->form_validation->set_rules('id_staff', 'id staff', 'trim|required');
		$this->form_validation->set_rules('id_opd', 'id opd', 'trim|required');

		$this->form_validation->set_rules('', '', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "tabel_atasan_bawahan.xls";
        $judul = "tabel_atasan_bawahan";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Atasan");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Staff");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Opd");

	foreach ($this->Tabel_atasan_bawahan_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteNumber($tablebody, $kolombody++, $data->id_atasan);
	    xlsWriteNumber($tablebody, $kolombody++, $data->id_staff);
	    xlsWriteNumber($tablebody, $kolombody++, $data->id_opd);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Tabel_atasan_bawahan.php */
/* Location: ./application/controllers/Tabel_atasan_bawahan.php */
/* Please DO NOT modify this information : */
/* Generated by CRUDV2 Generator For AdminLTE Template 2024-01-12 04:39:57 */