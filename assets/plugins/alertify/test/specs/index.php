
<!DOCTYPE html>
<html>
<head>
	<title>404 Page Not Found</title>
	<link rel="stylesheet" href="https://onlinersudbanyumas.banyumaskab.go.id/rs_web/assets/front/css/bootstrap.min.css">
	<link rel="stylesheet" href="https://onlinersudbanyumas.banyumaskab.go.id/rs_web/assets/front/css/font-awesome.min.css">
	<style>
		.navbar-default{background-image:linear-gradient(to bottom left,#35ccb5,#4c17ef,#35ccb5);}
	</style>
</head>
<body>
	<nav class="navbar navbar-default navbar-fixed-top bg-danger" role="navigation"></nav>
	<div class="container text-center" style="margin-top:100px">
		<h1 class="text-center">WARNING!!!</h1>
		<i class="fa fa-warning fa-5x" style="color:red"></i>
		<h1 class="text-center">404 Page Not Found</h1>
		<h3 class="text-center">You can't access this page!!!</h3>
		<a href="https://onlinersudbanyumas.banyumaskab.go.id" class="btn btn-danger"><i class="fa fa-hand-o-left"></i> Back to Home</a>
	</div>
	<nav class="navbar navbar-default navbar-fixed-bottom text-center" role="navigation">
		<strong style="color:white;">RUMAH SAKIT DAERAH BANYUMAS</strong>
	</nav>
</body>
</html>

