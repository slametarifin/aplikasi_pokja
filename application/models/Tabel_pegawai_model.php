<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tabel_pegawai_model extends CI_Model
{

    public $table = 'tabel_pegawai';
    public $id = 'id_peg';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }
    // datatables
    function json($nip=null,$nama=null,$opd=null,$jabatan=null) {
        $this->datatables->select('id_peg,nip,nama,tabel_pegawai.id_opd,id_jabatan,nama_opd,nama_jabatan');
        $this->datatables->from('tabel_pegawai');
        // $this->db->or_like("tabel_pegawai.id_opd", $opd);
        $this->db->or_like("nama", $nama);
        $this->db->or_like("id_jabatan", $jabatan);
        $this->db->where('tabel_pegawai.id_opd',$opd);
        // $this->datatables->or_where( 'nama', '%'.$nama.'%', 'LIKE' );
        // $this->datatables->or_where( 'nip', '%'.$nip.'%', 'LIKE' );
        //add this line for join
        $this->datatables->join('tabel_opd', 'tabel_pegawai.id_opd = tabel_opd.id');
        $this->datatables->join('tabel_jabatan', 'tabel_pegawai.id_jabatan = tabel_jabatan.id');
        $this->datatables->add_column('action', anchor(site_url('tabel_atasan_bawahan/tambah_staff/$1'),'<i class="bi bi-person-add" aria-hidden="true"></i>', array('class' => 'btn btn-sm btn-info', 'title' => 'Tambahkan Bawahan'))." 
            ".anchor(site_url('akun_pengguna/reset_passs/$1'),'<i class="bi bi-repeat" aria-hidden="true"></i>', array('class' => 'btn btn-sm btn-info', 'title' => 'Reset Akun'))." 
            ".anchor(site_url('tabel_pegawai/akses_menu/$1'),'<i class="bi bi-building-gear" aria-hidden="true"></i>', array('class' => 'btn btn-sm btn-primary', 'title' => 'Akses Menu'))." 
            ".anchor(site_url('tabel_pegawai/profile/$1'),'<i class="bi bi-eye" aria-hidden="true"></i>', array('class' => 'btn btn-sm btn-success', 'title' => 'Lihat Detail Data'))." 
            ".anchor(site_url('tabel_pegawai/update/$1'),'<i class="bi bi-pencil" aria-hidden="true"></i>', array('class' => 'btn btn-sm btn-warning', 'title' => 'Ubah Data'))." 
                ".anchor(site_url('tabel_pegawai/delete/$1'),'<i class="bi bi-trash" aria-hidden="true"></i>','class="btn btn-sm btn-danger hapus" title="Hapus Data"'), 'id_peg');
        return $this->datatables->generate();
    }
    // datatables
    function json_ori() {
        $this->datatables->select('id_peg,nip,nama,tabel_pegawai.id_opd,id_jabatan,nama_opd,nama_jabatan');
        $this->datatables->from('tabel_pegawai');
        //add this line for join
        $this->datatables->join('tabel_opd', 'tabel_pegawai.id_opd = tabel_opd.id');
        $this->datatables->join('tabel_jabatan', 'tabel_pegawai.id_jabatan = tabel_jabatan.id');
        $this->datatables->add_column('action', anchor(site_url('tabel_atasan_bawahan/tambah_staff/$1'),'<i class="bi bi-person-add" aria-hidden="true"></i>', array('class' => 'btn btn-sm btn-info', 'title' => 'Tambahkan Bawahan'))." 
            ".anchor(site_url('akun_pengguna/reset_passs/$1'),'<i class="bi bi-repeat" aria-hidden="true"></i>', array('class' => 'btn btn-sm btn-info', 'title' => 'Reset Akun'))." 
            ".anchor(site_url('tabel_pegawai/akses_menu/$1'),'<i class="bi bi-building-gear" aria-hidden="true"></i>', array('class' => 'btn btn-sm btn-primary', 'title' => 'Akses Menu'))." 
            ".anchor(site_url('tabel_pegawai/profile/$1'),'<i class="bi bi-eye" aria-hidden="true"></i>', array('class' => 'btn btn-sm btn-success', 'title' => 'Lihat Detail Data'))." 
            ".anchor(site_url('tabel_pegawai/update/$1'),'<i class="bi bi-pencil" aria-hidden="true"></i>', array('class' => 'btn btn-sm btn-warning', 'title' => 'Ubah Data'))." 
                ".anchor(site_url('tabel_pegawai/delete/$1'),'<i class="bi bi-trash" aria-hidden="true"></i>','class="btn btn-sm btn-danger hapus" title="Hapus Data"'), 'id_peg');
        return $this->datatables->generate();
    }
    // datatables
    function json_opd($id_opd) {
        $this->datatables->select('id_peg,nip,nama,tabel_pegawai.id_opd,id_jabatan,nama_opd,nama_jabatan');
        $this->datatables->from('tabel_pegawai');
        $this->datatables->where('tabel_pegawai.id_opd',$id_opd);
        //add this line for join
        $this->datatables->join('tabel_opd', 'tabel_pegawai.id_opd = tabel_opd.id');
        $this->datatables->join('tabel_jabatan', 'tabel_pegawai.id_jabatan = tabel_jabatan.id');
        $this->datatables->add_column('action', anchor(site_url('tabel_atasan_bawahan/tambah_staff/$1'),'<i class="bi bi-person-add" aria-hidden="true"></i>', array('class' => 'btn btn-sm btn-info', 'title' => 'Tambahkan Bawahan'))." 
            ".anchor(site_url('akun_pengguna/reset_passs/$1'),'<i class="bi bi-repeat" aria-hidden="true"></i>', array('class' => 'btn btn-sm btn-info', 'title' => 'Reset Akun'))." 
            ".anchor(site_url('tabel_pegawai/akses_menu/$1'),'<i class="bi bi-building-gear" aria-hidden="true"></i>', array('class' => 'btn btn-sm btn-primary', 'title' => 'Akses Menu'))." 
            ".anchor(site_url('tabel_pegawai/profile/$1'),'<i class="bi bi-eye" aria-hidden="true"></i>', array('class' => 'btn btn-sm btn-success', 'title' => 'Lihat Detail Data'))." 
            ".anchor(site_url('tabel_pegawai/update/$1'),'<i class="bi bi-pencil" aria-hidden="true"></i>', array('class' => 'btn btn-sm btn-warning', 'title' => 'Ubah Data'))." 
                ".anchor(site_url('tabel_pegawai/delete/$1'),'<i class="bi bi-trash" aria-hidden="true"></i>','class="btn btn-sm btn-danger hapus" title="Hapus Data"'), 'id_peg');
        return $this->datatables->generate();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }

    // get data by id
    function get_by_nip($id)
    {
        $this->db->where('nip', $id);
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
       $this->db->like("cast(id_peg as text)", $q);
		$this->db->or_like("cast(nip as text)", $q);
		$this->db->or_like("cast(nama as text)", $q);
		$this->db->or_like("cast(id_opd as text)", $q);
		$this->db->or_like("cast(id_jabatan as text)", $q);
		$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like("cast(id_peg as text)", $q);
		$this->db->or_like("cast(nip as text)", $q);
		$this->db->or_like("cast(nama as text)", $q);
		$this->db->or_like("cast(id_opd as text)", $q);
		$this->db->or_like("cast(id_jabatan as text)", $q);
		$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

/* End of file Tabel_pegawai_model.php */
/* Location: ./application/models/Tabel_pegawai_model.php */
/* Please DO NOT modify this information : */
/* Generated by CRUDV2 Generator For AdminLTE Template 2024-01-08 08:52:42 */