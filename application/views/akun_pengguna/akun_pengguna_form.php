<script src="https://cdn.jsdelivr.net/npm/jquery@3.5.0/dist/jquery.slim.min.js"></script>
<!-- select2 form-select -->
  <div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header">
                <div class="box-title">
                    <b><i class="fa fa-tasks"></i> <?= $button ?> Data Akun_pengguna</b>
                </div>
                <div class="box-tools pull-right">
                    <button type="button" class="toggle-expand-btn btn btn-default btn-sm"><i class="fa fa-expand"></i></button>
                </div>
            </div>
            <div class="box-body">
                <form style="padding: 15px;" action="<?= $action; ?>" method="POST" enctype="multipart/form-data">
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="varchar">user_name</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" minlength="18" name="user_name" id="user_name" placeholder="user_name" value="<?= $user_name; ?>" />
                                <?= form_error('user_name') ?>
                            </div>
                        </div>
                    </div>
					<!-- <div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="varchar">pass</label>
                            <div class="col-md-6">
                                <input type="password" class="form-control" name="pass" id="pass" placeholder="pass" value="<?= $pass; ?>" />
                                <?= form_error('pass') ?>
                            </div>
                        </div>
                    </div> -->
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="bigint">Nama Pengguna</label>
                            <div class="col-md-6">
                                <select name="id_user" class="form-control select2 form-select">
                                    <option value="-">pilih Pegawai</option> 
                                    <?php 
                                    $data=$this->db->query("select * from tabel_akun")->result();
                                    foreach($data as $dt){?>
                                        <option value="<?=$dt->id?>" <?php if($dt->id==$id_user){?>selected <?php }?>><?=$dt->user_name?></option>
                                    <?php }
                                    ?>
                                </select> 
                                <?= form_error('id_user') ?>
                            </div>
                        </div>
                    </div> 
                    <div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="bigint">Level Pengguna</label>
                            <div class="col-md-6">
                                <select name="role" class="form-control select2 form-select"> 
                                    <?php 
                                    $a=0;
                                    $data=array('Pengguna','Administrator','Superuser');
                                    foreach($data as $dt){?>
                                        <option value="<?=$a?>" <?php if($a==$role){?>selected <?php }?>><?=$dt?></option>
                                    <?php $a++;}
                                    ?>
                                </select> 
                                <?= form_error('id_user') ?>
                            </div>
                        </div>
                    </div> 
					<div class="form-group">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-2"> 
                                <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> <?= $button ?></button>
                                <a href="<?= site_url('akun_pengguna') ?>" class="btn btn-danger"><i class="fa fa-sign-out"></i> Kembali</a>
                            </div>
                        </div>
                    </div>
				</form>
            </div>
        </div>
    </div>
</div>