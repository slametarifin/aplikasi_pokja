<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tabel_anggota_gugus extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Tabel_anggota_gugus_model');
        $this->load->library('form_validation');
		$this->load->library('datatables');
    }

    public function index()
    {
        $this->template->load('template_antrian','tabel_anggota_gugus/tabel_anggota_gugus_list');
    } 
    
    public function json() {
        // cek ketua gugus
        header('Content-Type: application/json');
        // cek role
        $cek_role=$this->db->query("
            select a.id,a.role,a.user_name, b.nip, c.id id_gugus from tabel_akun a
            join tabel_pegawai b on b.id_peg=a.id_user
            join tabel_gugus c on c.nip_ketua = b.nip
            where a.user_name='".$this->session->userdata('username')."'
        ")->row();
        if($cek_role->role==2){
            $display="block";
        }elseif($cek_role->role==0||$cek_role->role==1){
            $display="none";
        }
        $data=array('id_user'=>$cek_role->id,'akses'=>$cek_role->role,'display'=>$display);
        if($cek_role->role==2){
            echo $this->Tabel_anggota_gugus_model->json();
        }elseif($cek_role->role==1||$cek_role->role==0){
            echo $this->Tabel_anggota_gugus_model->jsonn($cek_role->id_gugus);
        }
        // echo $this->Tabel_anggota_gugus_model->json();
    }
    public function get_data() {
        $id=$this->input->post('id');
        header('Content-Type: application/json');
        $data=$this->db->query("select * from tabel_gugus where id=".$id)->row(); 
        echo json_encode($data);
    }

    public function read($id) 
    {
        $row = $this->Tabel_anggota_gugus_model->get_by_id($id);
        if ($row) {
            $data = array(
				'id' => $row->id,
				'id_gugus' => $row->id_gugus,
				'nip' => $row->nip,
				'status' => $row->status,
			);
            $this->template->load('template_antrian','tabel_anggota_gugus/tabel_anggota_gugus_read', $data);
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('tabel_anggota_gugus'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Tambah',
            'action' => site_url('tabel_anggota_gugus/create_action'),
			'id' => set_value('id'),
			'id_gugus' => set_value('id_gugus'),
			'nip' => set_value('nip'),
			'status' => set_value('status'),
		);
        $this->template->load('template_antrian','tabel_anggota_gugus/tabel_anggota_gugus_form', $data);
    }
    
    public function create_action() 
    { 
            $data = array(
				'id_gugus' => $this->input->post('id_gugus',TRUE),
				'nip' => $this->input->post('nip',TRUE),
				'status' => 'true',
			);

            $this->Tabel_anggota_gugus_model->insert($data);
            $this->session->set_flashdata('success', 'Berhasil Tambah Data');
            redirect(site_url('tabel_anggota_gugus')); 
    }
    
    public function update($id) 
    {
        $row = $this->Tabel_anggota_gugus_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Ubah',
                'action' => site_url('tabel_anggota_gugus/update_action/'.$id),
				'id' => set_value('id', $row->id),
				'id_gugus' => set_value('id_gugus', $row->id_gugus),
				'nip' => set_value('nip', $row->nip),
				'status' => set_value('status', $row->status),
			);
            $this->template->load('template_antrian','tabel_anggota_gugus/tabel_anggota_gugus_form', $data);
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('tabel_anggota_gugus'));
        }
    }
    
    public function update_action($id) 
    { 
            $data = array(
				'id_gugus' => $this->input->post('id_gugus',TRUE),
				'nip' => $this->input->post('nip',TRUE),
				'status' => $this->input->post('status',TRUE),
			);

            $this->Tabel_anggota_gugus_model->update($id, $data);
            $this->session->set_flashdata('success', 'Berhasil Edit Data');
            redirect(site_url('tabel_anggota_gugus')); 
    }
    
    public function delete($id) 
    {
        $row = $this->Tabel_anggota_gugus_model->get_by_id($id);

        if ($row) {
            $this->Tabel_anggota_gugus_model->delete($id);
            $this->session->set_flashdata('success', 'Berhasil Hapus Data');
            redirect(site_url('tabel_anggota_gugus'));
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('tabel_anggota_gugus'));
        }
    }

    public function _rules() 
    {
		$this->form_validation->set_rules('id_gugus', 'id gugus', 'trim|required');
		$this->form_validation->set_rules('nip', 'nip', 'trim|required');
		$this->form_validation->set_rules('status', 'status', 'trim|required');

		$this->form_validation->set_rules('', '', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "tabel_anggota_gugus.xls";
        $judul = "tabel_anggota_gugus";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Gugus");
	xlsWriteLabel($tablehead, $kolomhead++, "Nip");
	xlsWriteLabel($tablehead, $kolomhead++, "Status");

	foreach ($this->Tabel_anggota_gugus_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteNumber($tablebody, $kolombody++, $data->id_gugus);
	    xlsWriteLabel($tablebody, $kolombody++, $data->nip);
	    xlsWriteLabel($tablebody, $kolombody++, $data->status);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Tabel_anggota_gugus.php */
/* Location: ./application/controllers/Tabel_anggota_gugus.php */
/* Please DO NOT modify this information : */
/* Generated by CRUDV2 Generator For AdminLTE Template 2024-01-09 01:37:22 */