<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ijin_keramaian extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Ijin_keramaian_model');
        $this->load->library('form_validation');
		$this->load->library('datatables');
    }

    public function index()
    {
        $data = array(
            'ijin_keramaian_data' => $this->Ijin_keramaian_model->get_all(),
        );
        $this->template->load('templatex','ijin_keramaian/ijin_keramaian_list', $data);
    }

    public function read($id) 
    {
        $row = $this->Ijin_keramaian_model->get_by_id($id);
        if ($row) {
            $data = array(
				'id' => $row->id,
				'id_user' => $row->id_user,
				'jenis_keramaian' => $row->jenis_keramaian,
				'tgl_mulai' => $row->tgl_mulai,
				'tgl_selesai' => $row->tgl_selesai,
				'waktu' => $row->waktu,
				'jenis_hiburan' => $row->jenis_hiburan,
				'jml_undangan' => $row->jml_undangan,
				'tempat' => $row->tempat,
				'tgl_surat' => $row->tgl_surat,
				'id_pejabat' => $row->id_pejabat,
				'id_desa' => $row->id_desa,
				'app_sekdes' => $row->app_sekdes,
				'app_kades' => $row->app_kades,
				'status' => $row->status,
				'nomor_surat' => $row->nomor_surat,
			);
            $this->template->load('templatex','ijin_keramaian/ijin_keramaian_read', $data);
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('ijin_keramaian'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Tambah',
            'action' => site_url('ijin_keramaian/create_action'),
			'id' => set_value('id'),
			'id_user' => set_value('id_user'),
			'jenis_keramaian' => set_value('jenis_keramaian'),
			'tgl_mulai' => set_value('tgl_mulai'),
			'tgl_selesai' => set_value('tgl_selesai'),
			'waktu' => set_value('waktu'),
			'jenis_hiburan' => set_value('jenis_hiburan'),
			'jml_undangan' => set_value('jml_undangan'),
			'tempat' => set_value('tempat'),
			'tgl_surat' => set_value('tgl_surat'),
			'id_pejabat' => set_value('id_pejabat'),
			'id_desa' => set_value('id_desa'),
			'app_sekdes' => set_value('app_sekdes'),
			'app_kades' => set_value('app_kades'),
			'status' => set_value('status'),
			'nomor_surat' => set_value('nomor_surat'),
		);
        $this->template->load('templatex','ijin_keramaian/ijin_keramaian_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
				'id_user' => $this->input->post('id_user',TRUE),
				'jenis_keramaian' => $this->input->post('jenis_keramaian',TRUE),
				'tgl_mulai' => $this->input->post('tgl_mulai',TRUE),
				'tgl_selesai' => $this->input->post('tgl_selesai',TRUE),
				'waktu' => $this->input->post('waktu',TRUE),
				'jenis_hiburan' => $this->input->post('jenis_hiburan',TRUE),
				'jml_undangan' => $this->input->post('jml_undangan',TRUE),
				'tempat' => $this->input->post('tempat',TRUE),
				'tgl_surat' => $this->input->post('tgl_surat',TRUE),
				'id_pejabat' => $this->input->post('id_pejabat',TRUE),
				'id_desa' => $this->input->post('id_desa',TRUE),
				'app_sekdes' => $this->input->post('app_sekdes',TRUE),
				'app_kades' => $this->input->post('app_kades',TRUE),
				'status' => $this->input->post('status',TRUE),
				'nomor_surat' => $this->input->post('nomor_surat',TRUE),
			);

            $this->Ijin_keramaian_model->insert($data);
            $this->session->set_flashdata('success', 'Berhasil Tambah Data');
            redirect(site_url('ijin_keramaian'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Ijin_keramaian_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Ubah',
                'action' => site_url('ijin_keramaian/update_action'),
				'id' => set_value('id', $row->id),
				'id_user' => set_value('id_user', $row->id_user),
				'jenis_keramaian' => set_value('jenis_keramaian', $row->jenis_keramaian),
				'tgl_mulai' => set_value('tgl_mulai', $row->tgl_mulai),
				'tgl_selesai' => set_value('tgl_selesai', $row->tgl_selesai),
				'waktu' => set_value('waktu', $row->waktu),
				'jenis_hiburan' => set_value('jenis_hiburan', $row->jenis_hiburan),
				'jml_undangan' => set_value('jml_undangan', $row->jml_undangan),
				'tempat' => set_value('tempat', $row->tempat),
				'tgl_surat' => set_value('tgl_surat', $row->tgl_surat),
				'id_pejabat' => set_value('id_pejabat', $row->id_pejabat),
				'id_desa' => set_value('id_desa', $row->id_desa),
				'app_sekdes' => set_value('app_sekdes', $row->app_sekdes),
				'app_kades' => set_value('app_kades', $row->app_kades),
				'status' => set_value('status', $row->status),
				'nomor_surat' => set_value('nomor_surat', $row->nomor_surat),
			);
            $this->template->load('templatex','ijin_keramaian/ijin_keramaian_form', $data);
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('ijin_keramaian'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('', TRUE));
        } else {
            $data = array(
				'id_user' => $this->input->post('id_user',TRUE),
				'jenis_keramaian' => $this->input->post('jenis_keramaian',TRUE),
				'tgl_mulai' => $this->input->post('tgl_mulai',TRUE),
				'tgl_selesai' => $this->input->post('tgl_selesai',TRUE),
				'waktu' => $this->input->post('waktu',TRUE),
				'jenis_hiburan' => $this->input->post('jenis_hiburan',TRUE),
				'jml_undangan' => $this->input->post('jml_undangan',TRUE),
				'tempat' => $this->input->post('tempat',TRUE),
				'tgl_surat' => $this->input->post('tgl_surat',TRUE),
				'id_pejabat' => $this->input->post('id_pejabat',TRUE),
				'id_desa' => $this->input->post('id_desa',TRUE),
				'app_sekdes' => $this->input->post('app_sekdes',TRUE),
				'app_kades' => $this->input->post('app_kades',TRUE),
				'status' => $this->input->post('status',TRUE),
				'nomor_surat' => $this->input->post('nomor_surat',TRUE),
			);

            $this->Ijin_keramaian_model->update($this->input->post('', TRUE), $data);
            $this->session->set_flashdata('success', 'Berhasil Edit Data');
            redirect(site_url('ijin_keramaian'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Ijin_keramaian_model->get_by_id($id);

        if ($row) {
            $this->Ijin_keramaian_model->delete($id);
            $this->session->set_flashdata('success', 'Berhasil Hapus Data');
            redirect(site_url('ijin_keramaian'));
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('ijin_keramaian'));
        }
    }

    public function _rules() 
    {
		$this->form_validation->set_rules('id_user', 'id user', 'trim|required');
		$this->form_validation->set_rules('jenis_keramaian', 'jenis keramaian', 'trim|required');
		$this->form_validation->set_rules('tgl_mulai', 'tgl mulai', 'trim|required');
		$this->form_validation->set_rules('tgl_selesai', 'tgl selesai', 'trim|required');
		$this->form_validation->set_rules('waktu', 'waktu', 'trim|required');
		$this->form_validation->set_rules('jenis_hiburan', 'jenis hiburan', 'trim|required');
		$this->form_validation->set_rules('jml_undangan', 'jml undangan', 'trim|required');
		$this->form_validation->set_rules('tempat', 'tempat', 'trim|required');
		$this->form_validation->set_rules('tgl_surat', 'tgl surat', 'trim|required');
		$this->form_validation->set_rules('id_pejabat', 'id pejabat', 'trim|required');
		$this->form_validation->set_rules('id_desa', 'id desa', 'trim|required');
		$this->form_validation->set_rules('app_sekdes', 'app sekdes', 'trim|required');
		$this->form_validation->set_rules('app_kades', 'app kades', 'trim|required');
		$this->form_validation->set_rules('status', 'status', 'trim|required');
		$this->form_validation->set_rules('nomor_surat', 'nomor surat', 'trim|required');

		$this->form_validation->set_rules('', '', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "ijin_keramaian.xls";
        $judul = "ijin_keramaian";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Id User");
	xlsWriteLabel($tablehead, $kolomhead++, "Jenis Keramaian");
	xlsWriteLabel($tablehead, $kolomhead++, "Tgl Mulai");
	xlsWriteLabel($tablehead, $kolomhead++, "Tgl Selesai");
	xlsWriteLabel($tablehead, $kolomhead++, "Waktu");
	xlsWriteLabel($tablehead, $kolomhead++, "Jenis Hiburan");
	xlsWriteLabel($tablehead, $kolomhead++, "Jml Undangan");
	xlsWriteLabel($tablehead, $kolomhead++, "Tempat");
	xlsWriteLabel($tablehead, $kolomhead++, "Tgl Surat");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Pejabat");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Desa");
	xlsWriteLabel($tablehead, $kolomhead++, "App Sekdes");
	xlsWriteLabel($tablehead, $kolomhead++, "App Kades");
	xlsWriteLabel($tablehead, $kolomhead++, "Status");
	xlsWriteLabel($tablehead, $kolomhead++, "Nomor Surat");

	foreach ($this->Ijin_keramaian_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteNumber($tablebody, $kolombody++, $data->id_user);
	    xlsWriteLabel($tablebody, $kolombody++, $data->jenis_keramaian);
	    xlsWriteLabel($tablebody, $kolombody++, $data->tgl_mulai);
	    xlsWriteLabel($tablebody, $kolombody++, $data->tgl_selesai);
	    xlsWriteLabel($tablebody, $kolombody++, $data->waktu);
	    xlsWriteLabel($tablebody, $kolombody++, $data->jenis_hiburan);
	    xlsWriteLabel($tablebody, $kolombody++, $data->jml_undangan);
	    xlsWriteLabel($tablebody, $kolombody++, $data->tempat);
	    xlsWriteLabel($tablebody, $kolombody++, $data->tgl_surat);
	    xlsWriteNumber($tablebody, $kolombody++, $data->id_pejabat);
	    xlsWriteNumber($tablebody, $kolombody++, $data->id_desa);
	    xlsWriteNumber($tablebody, $kolombody++, $data->app_sekdes);
	    xlsWriteNumber($tablebody, $kolombody++, $data->app_kades);
	    xlsWriteNumber($tablebody, $kolombody++, $data->status);
	    xlsWriteNumber($tablebody, $kolombody++, $data->nomor_surat);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Ijin_keramaian.php */
/* Location: ./application/controllers/Ijin_keramaian.php */
/* Please DO NOT modify this information : */
/* Generated by CRUDV2 Generator For AdminLTE Template 2024-01-04 15:15:52 */