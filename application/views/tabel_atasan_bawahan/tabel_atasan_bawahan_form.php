  <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.0/dist/jquery.slim.min.js"></script>
  <div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header">
                <div class="box-title">
                    <b><i class="fa fa-tasks"></i> <?= $button ?> Data Tabel_atasan_bawahan</b>
                </div>
                <div class="box-tools pull-right">
                    <button type="button" class="toggle-expand-btn btn btn-default btn-sm"><i class="fa fa-expand"></i></button>
                </div>
            </div>
            <div class="box-body">
                <form style="padding: 15px;" action="<?= $action; ?>" method="POST" enctype="multipart/form-data">
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="int">Nama Atasan</label>
                            <div class="col-md-6">
                                <?php
                                $data_atasan=$this->db->query("select * from tabel_pegawai where id_peg=".$kd_atasan)->row();
                                ?>
                                <select name="id_atasan" class="form-control select2 form-select">
                                    <option value="<?=$data_atasan->id_peg?>"><?=$data_atasan->nama?></option>
                                </select> 
                                <?= form_error('id_atasan') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="int">Nama Staff</label>
                            <div class="col-md-6">
                                <?php
                                $data_atasan=$this->db->query("select * from tabel_pegawai where id_opd=".$id_opd." and id_peg !=".$kd_atasan)->result();
                                ?>
                                <select name="id_staff" class="form-control select2 form-select">
                                    <option value="-">pilih data pegawai</option>
                                    <?php 
                                    foreach($data_atasan as $da){?>
                                    <option value="<?=$da->id_peg?>" <?php if($id_staff==$da->id_peg){?>selected <?php }?>><?=$da->nama?></option>
                                    <?php }?>
                                </select> 
                                <!-- <input type="text" class="form-control" name="id_staff" id="id_staff" placeholder="Id Staff" value="<?= $id_staff; ?>" /> -->
                                <?= form_error('id_staff') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="int">Instansi OPD</label>
                            <div class="col-md-6">
                                <?php
                                $data_opd=$this->db->query("select * from tabel_opd where id =".$id_opd)->row();
                                ?>
                                <select name="id_opd" class="form-control select2 form-select">
                                    <option value="<?=$data_opd->id?>"><?=$data_opd->nama_opd?></option> 
                                </select> 
                                <!-- <input type="text" class="form-control" name="id_opd" id="id_opd" placeholder="Id Opd" value="<?= $id_opd; ?>" /> -->
                                <?= form_error('id_opd') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-2">
                              
                                <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> <?= $button ?></button>
                                <a href="<?= site_url('tabel_pegawai') ?>" class="btn btn-danger"><i class="fa fa-sign-out"></i> Kembali</a>
                            </div>
                        </div>
                    </div>
				</form>
            </div>
        </div>
    </div>
</div>