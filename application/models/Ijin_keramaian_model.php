<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ijin_keramaian_model extends CI_Model
{

    public $table = 'ijin_keramaian';
    public $id = 'id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function json() {
        $this->datatables->select('id,id_user,jenis_keramaian,tgl_mulai,tgl_selesai,waktu,jenis_hiburan,jml_undangan,tempat,tgl_surat,id_pejabat,id_desa,app_sekdes,app_kades,status,nomor_surat');
        $this->datatables->from('ijin_keramaian');
        //add this line for join
        //$this->datatables->join('table2', 'ijin_keramaian.field = table2.field');
        $this->datatables->add_column('action', anchor(site_url('ijin_keramaian/read/$1'),'<i class="fa fa-eye" aria-hidden="true"></i>', array('class' => 'btn btn-success', 'title' => 'Lihat Detail Data'))." 
            ".anchor(site_url('ijin_keramaian/update/$1'),'<i class="fa fa-pencil-square-o" aria-hidden="true"></i>', array('class' => 'btn btn-warning', 'title' => 'Ubah Data'))." 
                ".anchor(site_url('ijin_keramaian/delete/$1'),'<i class="fa fa-trash-o" aria-hidden="true"></i>','class="btn btn-danger hapus" title="Hapus Data"'), 'id');
        return $this->datatables->generate();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
       $this->db->like("cast(id as text)", $q);
		$this->db->or_like("cast(id_user as text)", $q);
		$this->db->or_like("cast(jenis_keramaian as text)", $q);
		$this->db->or_like("cast(tgl_mulai as text)", $q);
		$this->db->or_like("cast(tgl_selesai as text)", $q);
		$this->db->or_like("cast(waktu as text)", $q);
		$this->db->or_like("cast(jenis_hiburan as text)", $q);
		$this->db->or_like("cast(jml_undangan as text)", $q);
		$this->db->or_like("cast(tempat as text)", $q);
		$this->db->or_like("cast(tgl_surat as text)", $q);
		$this->db->or_like("cast(id_pejabat as text)", $q);
		$this->db->or_like("cast(id_desa as text)", $q);
		$this->db->or_like("cast(app_sekdes as text)", $q);
		$this->db->or_like("cast(app_kades as text)", $q);
		$this->db->or_like("cast(status as text)", $q);
		$this->db->or_like("cast(nomor_surat as text)", $q);
		$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like("cast(id as text)", $q);
		$this->db->or_like("cast(id_user as text)", $q);
		$this->db->or_like("cast(jenis_keramaian as text)", $q);
		$this->db->or_like("cast(tgl_mulai as text)", $q);
		$this->db->or_like("cast(tgl_selesai as text)", $q);
		$this->db->or_like("cast(waktu as text)", $q);
		$this->db->or_like("cast(jenis_hiburan as text)", $q);
		$this->db->or_like("cast(jml_undangan as text)", $q);
		$this->db->or_like("cast(tempat as text)", $q);
		$this->db->or_like("cast(tgl_surat as text)", $q);
		$this->db->or_like("cast(id_pejabat as text)", $q);
		$this->db->or_like("cast(id_desa as text)", $q);
		$this->db->or_like("cast(app_sekdes as text)", $q);
		$this->db->or_like("cast(app_kades as text)", $q);
		$this->db->or_like("cast(status as text)", $q);
		$this->db->or_like("cast(nomor_surat as text)", $q);
		$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

/* End of file Ijin_keramaian_model.php */
/* Location: ./application/models/Ijin_keramaian_model.php */
/* Please DO NOT modify this information : */
/* Generated by CRUDV2 Generator For AdminLTE Template 2024-01-04 15:15:52 */