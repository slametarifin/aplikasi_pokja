<div class="row"> 
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <div class="box-title">
                    <b><i class="fa fa-list-alt"></i> Data Tabel_pegawai</b>
                </div> 
            </div>
            <div class="box-body">
                <a href="<?= site_url('tabel_pegawai/create') ?>" class="btn btn-primary col-md-2" style="margin-left : 15px;display:<?=$display?>"><i class="fa fa-plus"></i> Tambah Data</a><div class="row">
            <div class="col-md-12"> 
                <form style="padding: 15px;" action="<?= $action; ?>" method="POST" enctype="multipart/form-data">
                    <div class="form-group">
                        <div class="row">
                            <select id="cari_opd" name="kode_opd" data-id="kode_opd" class="cari_ select2 col-md-2">
                                <option value="-">data OPD</option> 
                                <?php 
                                  $data_opd=$this->db->query("select nama_opd,id from tabel_opd")->result();
                                  foreach($data_opd as $dtopd){?>
                                    <option value="<?=$dtopd->id?>" <?php if($dtopd->id==$kode_opd){?>selected <?php }?>><?=$dtopd->nama_opd?></option>
                                  <?php }
                                ?>
                            </select>
                            <select id="kode_jabatan" name="kode_jabatan" class="cari_ select2 col-md-2">
                                <option value="-">data Jabatan</option>  
                            </select>
                            <select id="cari_nama" name="nama" class="cari_ select2 col-md-2">
                                <option value="-">data Pegawai</option> 
                            </select> 
                            <input type="submit" class="btn btn-primary col-md-2">
                        </div>
                    </div>
                </form>
            </div>
                <div class="table-responsive" style="padding: 15px">  
                    <table class="table table-bordered table-striped table-hover" width="100%" id="mytable">
                        <thead>
                            <tr>
                                <th width="5%">No</th>
                                <th>NIP </th>
                                <th>Nama Lengkap </th>
                                <th>Instansi / OPD </th>
                                <th>Jabatan </th>
                                <th width="15%">Aksi</th>
                            </tr>
                        </thead>    
                    </table>

                    <script src="<?= base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
                    <script type="text/javascript">
                        $(document).ready(function() {
                            $('#cari_opd').change(function() { 
                                var kode_opd = $(this).val(); 
                                $.ajax({
                                    type: "POST",
                                    data: {
                                      kode_opd: kode_opd, 
                                    },
                                    url: "<?php echo site_url('tabel_pegawai/get_jabatan') ?>",
                                    dataType: "json",
                                    success: function(hasil) {
                                    $.each(hasil, function(index, option) {
                                        // console.log(option.id+':'+option.nama_jabatan);
                                      $('#kode_jabatan').append($('<option>', {
                                        value: option.id,
                                        text: option.nama_jabatan
                                      }));
                                    }); 
                                    },
                                    error: function(hasil){ 
                                        console.log('data tidak ditemukan');
                                    },
                                }); 
                            }); 
                            $('#kode_jabatan').change(function() { 
                                var kode_opd = $('#cari_opd').val(); 
                                var kode_jabatan = $(this).val(); 
                                $.ajax({
                                    type: "POST",
                                    data: {
                                      kode_opd: kode_opd, 
                                      kode_jabatan: kode_jabatan,
                                    },
                                    url: "<?php echo site_url('tabel_pegawai/get_pegawai') ?>",
                                    dataType: "json",
                                    success: function(hasil) {
                                    $.each(hasil, function(index, option) {
                                        // console.log(option.id+':'+option.nama_jabatan);
                                      $('#cari_nama').append($('<option>', {
                                        value: option.id,
                                        text: option.nip+' | '+option.nama
                                      }));
                                    }); 
                                    },
                                    error: function(hasil){ 
                                        console.log('data tidak ditemukan');
                                    },
                                }); 
                            }); 
                            $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
                            {
                                return {
                                    "iStart": oSettings._iDisplayStart,
                                    "iEnd": oSettings.fnDisplayEnd(),
                                    "iLength": oSettings._iDisplayLength,
                                    "iTotal": oSettings.fnRecordsTotal(),
                                    "iFilteredTotal": oSettings.fnRecordsDisplay(),
                                    "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                                    "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                                };
                            };

                            var t = $("#mytable").dataTable({
                                "processing"  : true,
                                "serverSide"  : true,
                                "oLanguage"   : { sProcessing : "Loading. . ." },
                                "ajax"        : { "url" : "<?= site_url('tabel_pegawai/json/'.$nip.'/'.$id_peg.'/'.$kode_opd.'/'.$kode_jabatan) ?>", "type": "POST"},
                                "columns"     : [
                                    {
                                        "data": "id_peg",
                                        "orderable": false,
                                        "className" : "text-center"
                                    },
                                    {
                                        "data": "nip"
                                    },
                                    {
                                        "data": "nama"
                                    },
                                    {
                                        "data": "nama_opd"
                                    },
                                    {
                                        "data": "nama_jabatan"
                                    },
                                    {
                                        "data" : "action",
                                        "orderable": false,
                                        "className" : "text-center",
                                        render: function (data, type, row) {
                                            var akses="<?=$akses?>";
                                            if(akses==2){return row.action;}else{return "tidak ada akses";}
                                        }
                                    }
                                ],
                                order: [[0, 'desc']],
                                rowCallback: function(row, data, iDisplayIndex) {
                                    var info = this.fnPagingInfo();
                                    var page = info.iPage;
                                    var length = info.iLength;
                                    var index = page * length + (iDisplayIndex + 1);
                                    $('td:eq(0)', row).html(index);
                                }
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>
<?php // swal_delete("#mytable",".hapus") ?>
        <?= anchor(site_url('tabel_pegawai/excel'), '<i class="fa fa-file-excel-o" aria-hidden="true"></i> Export Excel', 'class="btn btn-success"'); ?>