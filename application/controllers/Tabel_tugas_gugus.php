<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tabel_tugas_gugus extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Tabel_tugas_gugus_model');
        $this->load->model('Tabel_pegawai_model');
        $this->load->library('form_validation');
		$this->load->library('datatables');
    }

    public function index()
    {
        $data = array(
            'tabel_tugas_gugus_data' => $this->Tabel_tugas_gugus_model->get_all(),
        );
        $this->template->load('template_antrian','tabel_tugas_gugus/tabel_tugas_gugus_list', $data);
    }

    public function opd($id_opd=null)
    {
        $data = array(
            'id_opd'=>$id_opd,
            'tabel_tugas_gugus_data' => $this->Tabel_tugas_gugus_model->get_by_opd($id_opd),
        );
        $this->template->load('template_antrian','tabel_tugas_gugus/tabel_tugas_gugus_list_opd', $data);
    }
    public function json() {
        header('Content-Type: application/json');
        echo $this->Tabel_tugas_gugus_model->json();
    }  
    public function json_tugas($id_user=null) {
        header('Content-Type: application/json');
        echo $this->Tabel_tugas_gugus_model->json_user($id_user);
    }   
    public function json_opd($id_opd=null) {
        header('Content-Type: application/json');
        echo $this->Tabel_tugas_gugus_model->json_opd($id_opd);
    }  
    public function jsonn($id=null) {
        header('Content-Type: application/json');
        echo $this->Tabel_tugas_gugus_model->jsonn($id);
    }  
    public function read($id) 
    {
        $row = $this->Tabel_tugas_gugus_model->get_by_id($id);
        if ($row) {
            $data = array(
				'id' => $row->id,
				'id_gugus_tugas' => $row->id_gugus_tugas,
				'tugas' => $row->tugas,
				'deskripsi' => $row->deskripsi,
				'pic' => $row->pic,
				'status' => $row->status,
			);
            $this->template->load('template_antrian','tabel_tugas_gugus/tabel_tugas_gugus_read', $data);
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('tabel_tugas_gugus'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Tambah',
            'action' => site_url('tabel_tugas_gugus/create_action'),
			'id' => set_value('id'),
			'id_gugus_tugas' => set_value('id_gugus_tugas'),
			'tugas' => set_value('tugas'),
			'deskripsi' => set_value('deskripsi'),
			'pic' => set_value('pic'),
			'status' => set_value('status'),
            'kode_opd' => set_value('kode_opd'),
            'id_jabatan' => set_value('id_jabatan'),
		);
        $this->template->load('template_antrian','tabel_tugas_gugus/tabel_tugas_gugus_form', $data);
    } 

    public function createe($id) 
    {
        $data = array(
            'button' => 'Tambah',
            'action' => site_url('tabel_tugas_gugus/create_actionn/'.$id),
            'id' => set_value('id'),
            'id_gugus_tugas' => $id,
            'tugas' => set_value('tugas'),
            'deskripsi' => set_value('deskripsi'),
            'pic' => set_value('pic'),
            'status' => set_value('status'),
            'kode_opd' => set_value('kode_opd'),
            'id_jabatan' => set_value('id_jabatan'),
            'id_eselon' => set_value('id_eselon'),
        );//print_r($data);die();
        $this->template->load('template_antrian','tabel_tugas_gugus/tabel_tugas_gugus_form', $data);
    }
    
    public function create_action() 
    {  
            $data = array(
				'id_gugus_tugas' => $this->input->post('id_gugus_tugas',TRUE),
				'tugas' => $this->input->post('tugas',TRUE),
				'deskripsi' => $this->input->post('deskripsi',TRUE),
				'pic' => $this->input->post('pic',TRUE),
				'status' => 'true',
                'tahun' => date('Y'),
                'bulan' => date('m'),
                'tanggal' => date('d'),
                'created_date' => date('Y-m-d'),
			);

            $this->Tabel_tugas_gugus_model->insert($data);
            $this->session->set_flashdata('success', 'Berhasil Tambah Data');
            redirect(site_url('tabel_tugas_gugus')); 
    }
    
    public function create_actionn($id) 
    {
    $ex=explode(' | ',$this->input->post('pic',TRUE));  
    $peg = $this->Tabel_pegawai_model->get_by_nip($this->input->post('pic',TRUE));
    // print_r($ex);
            $data = array(
                'id_gugus_tugas' => $this->input->post('id_gugus_tugas',TRUE),
                'tugas' => $this->input->post('tugas',TRUE),
                'deskripsi' => $this->input->post('deskripsi',TRUE),
                'pic' => $peg->id_peg,
                'status' => 'true',
                'tahun' => date('Y'),
                'bulan' => date('m'),
                'tanggal' => date('d'),
                'created_date' => date('Y-m-d'),
            );
            // print_r($data);die();

            $this->Tabel_tugas_gugus_model->insert($data);
            $this->session->set_flashdata('success', 'Berhasil Tambah Data');
            redirect(site_url('tabel_gugus/tugas_gugus/'.$id)); 
    }
    
    public function update($id) 
    {
        $row = $this->Tabel_tugas_gugus_model->get_by_id($id);

        if ($row) {
            $peg = $this->Tabel_pegawai_model->get_by_id($row->pic);
            $data = array(
                'button' => 'Ubah',
                'action' => site_url('tabel_tugas_gugus/update_action/'.$id),
				'id' => set_value('id', $row->id),
				'id_gugus_tugas' => set_value('id_gugus_tugas', $row->id_gugus_tugas),
				'tugas' => set_value('tugas', $row->tugas),
				'deskripsi' => set_value('deskripsi', $row->deskripsi),
				'pic' => set_value('pic', $row->pic),
				'status' => set_value('status', $row->status),
                'tahun' => set_value('tahun', $row->tahun),
                'bulan' => set_value('bulan', $row->bulan),
                'tanggal' => set_value('tanggal', $row->tanggal),
                'created_date' => set_value('created_date', $row->created_date),
                'kode_opd' => set_value('kode_opd', $peg->id_opd),
                'id_jabatan' => set_value('id_jabatan', $peg->id_jabatan),
                'id_eselon' => set_value('id_eselon', $peg->id_eselon),
			); 
            $this->template->load('template_antrian','tabel_tugas_gugus/tabel_tugas_gugus_form', $data);
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('tabel_tugas_gugus'));
        }
    }
    
    public function updatee($id) 
    {
        $row = $this->Tabel_tugas_gugus_model->get_by_id($id);

        if ($row) {
            $peg = $this->Tabel_pegawai_model->get_by_nip($row->pic);
            $data = array(
                'button' => 'Ubah',
                'action' => site_url('tabel_tugas_gugus/update_action/'.$id),
                'id' => set_value('id', $row->id),
                'id_gugus_tugas' => set_value('id_gugus_tugas', $row->id_gugus_tugas),
                'tugas' => set_value('tugas', $row->tugas),
                'deskripsi' => set_value('deskripsi', $row->deskripsi),
                'pic' => set_value('pic', $row->pic),
                'status' => set_value('status', $row->status),
                'kode_opd' => set_value('kode_opd', $row->kode_opd),
                'id_jabatan' => set_value('id_jabatan', $row->id_jabatan),
                'id_eselon' => set_value('id_eselon', $row->id_eselon),
            );
            $this->template->load('template_antrian','tabel_tugas_gugus/tabel_tugas_gugus_form', $data);
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('tabel_tugas_gugus'));
        }
    }
    
    public function update_action($id) 
    { 
            $data = array(
				'id_gugus_tugas' => $this->input->post('id_gugus_tugas',TRUE),
				'tugas' => $this->input->post('tugas',TRUE),
				'deskripsi' => $this->input->post('deskripsi',TRUE),
				'pic' => $this->input->post('pic',TRUE), 
			);

            $this->Tabel_tugas_gugus_model->update($id, $data);
            $this->session->set_flashdata('success', 'Berhasil Edit Data');
            redirect(site_url('tabel_tugas_gugus')); 
    }
    
    public function delete($id) 
    {
        $row = $this->Tabel_tugas_gugus_model->get_by_id($id);

        if ($row) {
            $this->Tabel_tugas_gugus_model->delete($id);
            $this->session->set_flashdata('success', 'Berhasil Hapus Data');
            redirect(site_url('tabel_tugas_gugus'));
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('tabel_tugas_gugus'));
        }
    }

    public function _rules() 
    {
		$this->form_validation->set_rules('id_gugus_tugas', 'id gugus tugas', 'trim|required');
		$this->form_validation->set_rules('tugas', 'tugas', 'trim|required');
		$this->form_validation->set_rules('deskripsi', 'deskripsi', 'trim|required');
		$this->form_validation->set_rules('pic', 'pic', 'trim|required');
		$this->form_validation->set_rules('status', 'status', 'trim|required');

		$this->form_validation->set_rules('', '', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "tabel_tugas_gugus.xls";
        $judul = "tabel_tugas_gugus";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Gugus Tugas");
	xlsWriteLabel($tablehead, $kolomhead++, "Tugas");
	xlsWriteLabel($tablehead, $kolomhead++, "Deskripsi");
	xlsWriteLabel($tablehead, $kolomhead++, "Pic");
	xlsWriteLabel($tablehead, $kolomhead++, "Status");

	foreach ($this->Tabel_tugas_gugus_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->id_gugus_tugas);
	    xlsWriteLabel($tablebody, $kolombody++, $data->tugas);
	    xlsWriteLabel($tablebody, $kolombody++, $data->deskripsi);
	    xlsWriteLabel($tablebody, $kolombody++, $data->pic);
	    xlsWriteLabel($tablebody, $kolombody++, $data->status);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Tabel_tugas_gugus.php */
/* Location: ./application/controllers/Tabel_tugas_gugus.php */
/* Please DO NOT modify this information : */
/* Generated by CRUDV2 Generator For AdminLTE Template 2024-01-08 07:59:19 */