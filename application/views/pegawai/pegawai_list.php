<div class="row"> 
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <div class="box-title">
                    <b><i class="fa fa-list-alt"></i> Data Pegawai</b>
                </div>
                <div class="box-tools pull-right">
                    <button type="button" class="toggle-expand-btn btn btn-default btn-sm"><i class="fa fa-expand"></i></button>
                </div>
            </div>
            <div class="box-body">
                <!-- <a href="<?= site_url('pegawai/create') ?>" class="btn btn-primary" style="margin-left : 15px"><i class="fa fa-plus"></i> Tambah Data</a> -->
                <a href="<?= site_url('dashbord_admin') ?>" class="btn btn-danger" style="margin-left : 15px"><i class="fa fa-arrow-left"></i> Kembali</a>
                <div class="table-responsive" style="padding: 15px">
                    <table class="table table-bordered table-striped table-hover" width="100%" id="mytable">
                        <thead>
                            <tr>
                                <th width="5%">No</th>
								<th>Nama Depan</th>
								<th>Nama Belakang</th>
								<th>Nik</th>
								<th>Nip</th>
								<th>Jabatan</th>
								<th>Unit Kerja</th> 
								<th width="15%">Aksi</th>
                            </tr>
                        </thead>    
                    </table>

                    <script src="<?= base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
                    <script type="text/javascript">
                        $(document).ready(function() {
                            $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
                            {
                                return {
                                    "iStart": oSettings._iDisplayStart,
                                    "iEnd": oSettings.fnDisplayEnd(),
                                    "iLength": oSettings._iDisplayLength,
                                    "iTotal": oSettings.fnRecordsTotal(),
                                    "iFilteredTotal": oSettings.fnRecordsDisplay(),
                                    "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                                    "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                                };
                            };

                            var t = $("#mytable").dataTable({
                                "processing"  : true,
                                "serverSide"  : true,
                                "oLanguage"   : { sProcessing : "Loading. . ." },
                                "ajax"        : { "url" : "<?= site_url('pegawai/json') ?>", "type": "POST"},
                                "columns"     : [
                                    {
                                        "data": "id_pegawai",
                                        "orderable": false,
                                        "className" : "text-center"
                                    },
									{
										"data": "nama_depan"
									},
									{
										"data": "nama_belakang"
									},
									{
										"data": "nik"
									},
									{
										"data": "nip"
									},
									{
										"data": "nm_jabatan",
                                        render: function (data, type, row) {
                                            if(row.jabatan=='0'||row.jabatan=='true'){
                                                return '-';
                                            }else{
                                                return row.nm_jabatan;
                                            }
                                        },
									},
									{
										"data": "nm_unit_kerja"
									}, 
                                    {
                                        "data" : "action",
                                        "orderable": false,
                                        "className" : "text-center",
                                        render: function (data, type, row) {
                                            var url="<?=site_url('pegawai/')?>";
                                            var id= row.id_pegawai;
                                            var lihat="<a href='"+url+'read/'+row.id_pegawai+"' title='Lihat Data' class='btn btn-success'><i class='fa fa-eye' aria-hidden='true'></i></a> ";
                                            var update="<a href='"+url+'update/'+row.id_pegawai+"' title='Ubah Data' class='btn btn-warning'><i class='fa fa-edit' aria-hidden='true'></i></a> ";
                                            var hapus="<a href='"+url+'delete/'+row.id_pegawai+"' title='Hapus Data' class='btn btn-danger'><i class='fa fa-trash-o' aria-hidden='true'></i></a> ";
                                            return lihat+update+hapus;
                                        },
                                    }
                                ],
                                order: [[0, 'desc']],
                                rowCallback: function(row, data, iDisplayIndex) {
                                    var info = this.fnPagingInfo();
                                    var page = info.iPage;
                                    var length = info.iLength;
                                    var index = page * length + (iDisplayIndex + 1);
                                    $('td:eq(0)', row).html(index);
                                }
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>
<?php // swal_delete("#mytable",".hapus") ?>