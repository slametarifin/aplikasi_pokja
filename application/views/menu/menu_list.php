<div class="row"> 
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <div class="box-title">
                    <b><i class="fa fa-list-alt"></i> Data Menu</b>
                </div>
                <div class="box-tools pull-right">
                    <button type="button" class="toggle-expand-btn btn btn-default btn-sm"><i class="fa fa-expand"></i></button>
                </div>
            </div>
            <div class="box-body">
                <a href="<?= site_url('menu/create') ?>" class="btn btn-primary" style="margin-left : 15px"><i class="fa fa-plus"></i> Tambah Data</a>
                <div class="table-responsive" style="padding: 15px">
                    <table class="table table-bordered table-striped table-hover" width="100%" id="mytable">
                        <thead>
                            <tr>
                                <th width="5%">No</th>
								<th>Caption</th>
								<th>Name</th>
								<th>Icon</th>
								<th>Grup</th>
								<th width="15%">Aksi</th>
                            </tr>
                        </thead>    
                    </table>

                    <script src="<?= base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
                    <script type="text/javascript">
                        $(document).ready(function() {
                            $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
                            {
                                return {
                                    "iStart": oSettings._iDisplayStart,
                                    "iEnd": oSettings.fnDisplayEnd(),
                                    "iLength": oSettings._iDisplayLength,
                                    "iTotal": oSettings.fnRecordsTotal(),
                                    "iFilteredTotal": oSettings.fnRecordsDisplay(),
                                    "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                                    "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                                };
                            };

                            var t = $("#mytable").dataTable({
                                "processing"  : true,
                                "serverSide"  : true,
                                "oLanguage"   : { sProcessing : "Loading. . ." },
                                "ajax"        : { "url" : "<?= site_url('menu/json') ?>", "type": "POST"},
                                "columns"     : [
                                    {
                                        "data": "PK",
                                        "orderable": false,
                                        "className" : "text-center"
                                    },
									{
										"data": "caption"
									},
									{
										"data": "name"
									},
									{
										"data": "icon"
									},
									{
										"data": "grup"
									},
                                    {
                                        "data" : "action",
                                        "orderable": false,
                                        "className" : "text-center"
                                    }
                                ],
                                order: [[0, 'desc']],
                                rowCallback: function(row, data, iDisplayIndex) {
                                    var info = this.fnPagingInfo();
                                    var page = info.iPage;
                                    var length = info.iLength;
                                    var index = page * length + (iDisplayIndex + 1);
                                    $('td:eq(0)', row).html(index);
                                }
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>
<?php // swal_delete("#mytable",".hapus") ?>