<div class="row">
	<div class="col-md-12">
		<div class="box box-success">
			<div class="box-header">
				<div class="box-title">
                    <b><i class="fa fa-eye"></i> Detail Data Tabel_opd</b>
                </div>
                <div class="box-tools pull-right">
                    <button type="button" class="toggle-expand-btn btn btn-default btn-sm"><i class="fa fa-expand"></i></button>
                </div>
			</div>
			<div class="box-body">
				<div style="padding: 15px;">
					<table class="table table-striped">
						<tr>
							<td width="20%"><b>Kode Opd</b></td>
							<td><?= $kode_opd; ?></td>
						</tr>
						<tr>
							<td width="20%"><b>Nama Opd</b></td>
							<td><?= $nama_opd; ?></td>
						</tr>
						<tr>
							<td width="20%"><b>Created Date</b></td>
							<td><?= $created_date; ?></td>
						</tr>
						<tr>
							<td width="20%"><b>Deleted Date</b></td>
							<td><?= $deleted_date; ?></td>
						</tr>
						<tr>
							<td width="20%"><b>Deleted By</b></td>
							<td><?= $deleted_by; ?></td>
						</tr>
					</table>
					<a href="<?= site_url('tabel_opd') ?>" class="btn btn-danger pull-right">
						<i class="fa fa-sign-out"></i> Kembali
					</a>
				</div>
			</div>
		</div>
	</div>
</div>