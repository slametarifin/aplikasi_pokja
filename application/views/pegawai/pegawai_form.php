<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header">
                <div class="box-title">
                    <b><i class="fa fa-tasks"></i> <?= $button ?> Data Pegawai</b>
                </div>
                <div class="box-tools pull-right">
                    <button type="button" class="toggle-expand-btn btn btn-default btn-sm"><i class="fa fa-expand"></i></button>
                </div>
            </div>
            <div class="box-body">
                <form style="padding: 15px;" action="<?= $action; ?>" method="POST" enctype="multipart/form-data">
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="varchar">Nama Depan</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="nama_depan" id="nama_depan" placeholder="Nama Depan" value="<?= $nama_depan; ?>" />
                                <?= form_error('nama_depan') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="varchar">Nama Belakang</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="nama_belakang" id="nama_belakang" placeholder="Nama Belakang" value="<?= $nama_belakang; ?>" />
                                <?= form_error('nama_belakang') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="varchar">Nik</label>
                            <div class="col-md-6">
                                <input type="number" class="form-control" name="nik" id="nik" placeholder="Nik" value="<?= $nik; ?>" />
                                <?= form_error('nik') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="varchar">Nip</label>
                            <div class="col-md-6">
                                <input type="number" class="form-control" name="nip" id="nip" placeholder="Nip" value="<?= $nip; ?>" />
                                <?= form_error('nip') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="int">Jabatan</label>
                            <div class="col-md-6">
                                <select name="jabatan" class="form-control">
                                    <option value="-">pilih jabatan</option>
                                    <?php 
                                    foreach ($list_jabatan as$value) {?>
                                        <option value="<?=$value->id?>" <?php if($value->id==$jabatan){?>selected <?php }?>><?=$value->nm_jabatan?></option>
                                    <?php }?>
                                </select>
                                <!-- <input type="text" class="form-control" name="jabatan" id="jabatan" placeholder="Jabatan" value="<?= $jabatan; ?>" /> -->
                                <?= form_error('jabatan') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="int">Unit Kerja</label>
                            <div class="col-md-6">
                                <select name="unit_kerja" class="form-control">
                                    <option value="-">pilih jabatan</option>
                                    <?php 
                                    foreach ($list_unit_kerja as$value) {?>
                                        <option value="<?=$value->id?>" <?php if($value->id==$unit_kerja){?>selected <?php }?>><?=$value->nm_unit_kerja?></option>
                                    <?php }?>
                                </select>
                                <!-- <input type="text" class="form-control" name="unit_kerja" id="unit_kerja" placeholder="Unit Kerja" value="<?= $unit_kerja; ?>" /> -->
                                <?= form_error('unit_kerja') ?>
                            </div>
                        </div>
                    </div> 
					<div class="form-group">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-2">
                              
                                <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> <?= $button ?></button>
                                <a href="<?= site_url('pegawai') ?>" class="btn btn-danger"><i class="fa fa-sign-out"></i> Kembali</a>
                            </div>
                        </div>
                    </div>
				</form>
            </div>
        </div>
    </div>
</div>