<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mobile extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Akun_pengguna_model');
        $this->load->library('form_validation');
		$this->load->library('datatables');
    }

    public function index()
    {
        if($this->session->userdata()){
            if($this->session->userdata('username')){
            }else{
                redirect(site_url('login'));
            }
        } 
        $this->template->load('template_mobile','dashbord/beranda3'); 
    }
}