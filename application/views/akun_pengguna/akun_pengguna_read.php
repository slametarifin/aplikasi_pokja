<div class="row">
	<div class="col-md-12">
		<div class="box box-success">
			<div class="box-header">
				<div class="box-title">
                    <b><i class="fa fa-eye"></i> Detail Data Akun_pengguna</b>
                </div>
                <div class="box-tools pull-right">
                    <button type="button" class="toggle-expand-btn btn btn-default btn-sm"><i class="fa fa-expand"></i></button>
                </div>
			</div>
			<div class="box-body">
				<div style="padding: 15px;">
					<table class="table table-striped">
						<tr>
							<td width="20%"><b>Username</b></td>
							<td><?= $username; ?></td>
						</tr>
						<tr>
							<td width="20%"><b>Password</b></td>
							<td><?= $password; ?></td>
						</tr>
						<tr>
							<td width="20%"><b>Id Pegawai</b></td>
							<td><?= $id_pegawai; ?></td>
						</tr>
						<tr>
							<td width="20%"><b>Id Desa</b></td>
							<td><?= $id_desa; ?></td>
						</tr>
					</table>
					<a href="<?= site_url('akun_pengguna') ?>" class="btn btn-danger pull-right">
						<i class="fa fa-sign-out"></i> Kembali
					</a>
				</div>
			</div>
		</div>
	</div>
</div>