<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tbl_pelayanan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Tbl_pelayanan_model');
        $this->load->library('form_validation');
		$this->load->library('datatables');
    }

    public function index()
    {
        $this->template->load('template_antrian_user','tbl_pelayanan/tbl_pelayanan_list');
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Tbl_pelayanan_model->json();
    }

    public function read($id) 
    {
        $row = $this->Tbl_pelayanan_model->get_by_id($id);
        if ($row) {
            $data = array(
				'id' => $row->id,
				//'id_layanan' => $row->id_layanan,
				'id_instansi' => $row->id_instansi,
				'nm_layanan' => $row->nm_layanan,
				'parent' => $row->parent,
				//'kategori' => $row->kategori,
			);
            $this->template->load('template_antrian','tbl_pelayanan/tbl_pelayanan_read', $data);
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('tbl_pelayanan'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Tambah',
            'action' => site_url('tbl_pelayanan/create_action'),
			'id' => set_value('id'),
			//'id_layanan' => set_value(//'id_layanan'),
			'id_instansi' => $this->db->query("select id_instansi from Tbl_pengguna where id=".$this->session->userdata('id_user')."")->row()->id_instansi,
			'nm_layanan' => set_value('nm_layanan'),
			'parent' => set_value('parent'),
			//'kategori' => set_value(//'kategori'),
            // 'nomor_loket' => set_value('nomor_loket'),
            'grup_layanan' => set_value('grup_layanan'),
		);
        $this->template->load('template_antrian','tbl_pelayanan/tbl_pelayanan_form', $data);
    }
    
    public function create_action() 
    { 
            $data = array(
				//'id_layanan' => $this->input->post(//'id_layanan',TRUE),
				'id_instansi' => $this->input->post('instansi',TRUE),#$this->db->query("select id_instansi from Tbl_pengguna where id=".$this->session->userdata('id_user')."")->row()->id_instansi,
				'nm_layanan' => $this->input->post('nm_layanan',TRUE),
				'parent' => $this->input->post('parent',TRUE),
				//'kategori' => $this->input->post(//'kategori',TRUE),
                // 'nomor_loket' => $this->input->post('nomor_loket',TRUE),
                'grup_layanan' => $this->input->post('grup_layanan',TRUE),
			);

            $this->Tbl_pelayanan_model->insert($data);
            $this->session->set_flashdata('success', 'Berhasil Tambah Data');
            redirect(site_url('tbl_pelayanan')); 
    }
    
    public function update($id) 
    {
        $row = $this->Tbl_pelayanan_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Ubah',
                'action' => site_url('tbl_pelayanan/update_action/'.$id),
				'id' => set_value('id', $row->id),
				//'id_layanan' => set_value(//'id_layanan', $row->id_layanan),
				'id_instansi' => set_value('id_instansi', $row->id_instansi),
				'nm_layanan' => set_value('nm_layanan', $row->nm_layanan),
				'parent' => set_value('parent', $row->parent),
				//'kategori' => set_value(//'kategori', $row->kategori),
                // 'nomor_loket' => set_value('nomor_loket', $row->nomor_loket),
                'grup_layanan' => set_value('grup_layanan', $row->grup_layanan),
			);
            $this->template->load('template_antrian','tbl_pelayanan/tbl_pelayanan_form', $data);
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('tbl_pelayanan'));
        }
    }
    
    public function update_action($id) 
    { 
            $data = array(
				// //'id_layanan' => $this->input->post(//'id_layanan',TRUE),
				'id_instansi' => $this->input->post('id_instansi',TRUE),
				'nm_layanan' => $this->input->post('nm_layanan',TRUE),
				'parent' => $this->input->post('parent',TRUE),
				//'kategori' => $this->input->post(//'kategori',TRUE),
                // 'nomor_loket' => $this->input->post('nomor_loket',TRUE),
                'grup_layanan' => $this->input->post('grup_layanan',TRUE),
			);

            $this->Tbl_pelayanan_model->update($id, $data);
            $this->session->set_flashdata('success', 'Berhasil Edit Data');
            redirect(site_url('tbl_pelayanan')); 
    }
    
    public function delete($id) 
    {
        $row = $this->Tbl_pelayanan_model->get_by_id($id);

        if ($row) {
            $this->Tbl_pelayanan_model->delete($id);
            $this->session->set_flashdata('success', 'Berhasil Hapus Data');
            redirect(site_url('tbl_pelayanan'));
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('tbl_pelayanan'));
        }
    }

    public function _rules() 
    {
		// $this->form_validation->set_rules(//'id_layanan', 'id layanan', 'trim|required');
		$this->form_validation->set_rules('id_instansi', 'id instansi', 'trim|required');
		$this->form_validation->set_rules('nm_layanan', 'nm layanan', 'trim|required');
		$this->form_validation->set_rules('parent', 'parent', 'trim|required');
		// $this->form_validation->set_rules(//'kategori', //'kategori', 'trim|required');

		$this->form_validation->set_rules('', '', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "tbl_pelayanan.xls";
        $judul = "tbl_pelayanan";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Layanan");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Instansi");
	xlsWriteLabel($tablehead, $kolomhead++, "Nm Layanan");
	xlsWriteLabel($tablehead, $kolomhead++, "Parent");
	xlsWriteLabel($tablehead, $kolomhead++, "Kategori");

	foreach ($this->Tbl_pelayanan_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteNumber($tablebody, $kolombody++, $data->id_layanan);
	    xlsWriteNumber($tablebody, $kolombody++, $data->id_instansi);
	    xlsWriteLabel($tablebody, $kolombody++, $data->nm_layanan);
	    xlsWriteNumber($tablebody, $kolombody++, $data->parent);
	    xlsWriteLabel($tablebody, $kolombody++, $data->kategori);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Tbl_pelayanan.php */
/* Location: ./application/controllers/Tbl_pelayanan.php */
/* Please DO NOT modify this information : */
/* Generated by CRUDV2 Generator For AdminLTE Template 2023-07-05 09:15:32 */