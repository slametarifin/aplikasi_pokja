<?php
if($this->session->userdata()){
 
  if($this->session->userdata('username')){ 
  }else{ 
    redirect(site_url('login'));
  }
}else{
    redirect(site_url('login'));
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Desa Sidamulya | Digital</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=site_url('./assets/adminlte/')?>plugins/fontawesome-free/css/all.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- Tempusdominus Bootstrap 4 -->
  <link rel="stylesheet" href="<?=site_url('./assets/adminlte/')?>plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css">
  <!-- iCheck -->
  <link rel="stylesheet" href="<?=site_url('./assets/adminlte/')?>plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <link rel="stylesheet" href="<?= base_url() ?>assets/plugins/datatables/css/dataTables.bootstrap.min.css">
  <!-- JQVMap -->
  <!-- <link rel="stylesheet" href="<?=site_url('./assets/adminlte/')?>plugins/jqvmap/jqvmap.min.css"> -->
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=site_url('./assets/adminlte/')?>dist/css/adminlte.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="<?=site_url('./assets/adminlte/')?>plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="<?=site_url('./assets/adminlte/')?>plugins/daterangepicker/daterangepicker.css">
  <!-- summernote -->
  <link rel="stylesheet" href="<?=site_url('./assets/adminlte/')?>plugins/summernote/summernote-bs4.min.css">
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<div class="wrapper">

  <!-- Preloader -->
  <div class="preloader flex-column justify-content-center align-items-center">
    <label>Layanan Digital</label>
    <img class="" src="<?=site_url('./assets/img/Logo-Cilacap.png')?>" alt="CilacapLogo" height="60" width="60"> 
    <label>Pemerintah Desa Sidamulya</label>
  </div>

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
      </li>
      <li class="nav-item d-none d-sm-inline-block">
        <a href="<?=site_url('beranda')?>" class="nav-link">Home</a>
      </li> 
    </ul>

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">    
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="<?=site_url('')?>dashbord_antrian" class="brand-link">      
      <center><img class="brand-image img-circle elevation-3" style="opacity: .8" src="<?=site_url('./assets/img/Logo-Cilacap.png')?>" height="50%" width="30%"></center> 
                <!-- <img src="<?=site_url('./assets/adminlte/')?>dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8"> -->
      <span class="brand-text font-weight-light">Pelayanan Online</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar"> 
      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">  
           
          <li class="nav-item" style="display:block">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                FITUR LAYANAN
                <!-- <?=print_r($this->session->userdata())?> -->
                <i class="right fas fa-angle-left"></i>
              </p>
            </a>  
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?=site_url("tbl_histori_antrian/display")?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Pengajuan Online</p>
                </a>
              </li>
            </ul>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?=site_url("tbl_histori_antrian/dash_get_antrian_user")?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Surat Digital</p>
                </a>
              </li>
            </ul> 
          </li>
          <li class="nav-item" style="display:block">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                MENU ADMIN
                <!-- <?=print_r($this->session->userdata())?> -->
                <i class="right fas fa-angle-left"></i>
              </p>
            </a> 
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?=site_url("nomor_surat")?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Data Pengajuan</p>
                </a>
              </li>
            </ul>
            <!-- <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?=site_url("Tbl_grup_pelayanan")?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Grup Layanan</p>
                </a>
              </li>
            </ul> -->
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?=site_url("master_jenis_dokumen")?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Data Layanan</p>
                </a>
              </li>
            </ul>
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?=site_url("data_pengguna")?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Data Akun</p>
                </a>
              </li>
            </ul> 
          </li>
          <li class="nav-item" style="display:block">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                LAPORAN
                <!-- <?=print_r($this->session->userdata())?> -->
                <i class="right fas fa-angle-left"></i>
              </p>
            </a> 
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?=site_url("Tbl_histori_antrian/laporan_kunjungan")?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Laporan Kunjungan</p>
                </a>
              </li>
            </ul>
          </li>
          <li class="nav-item">
            <a href="#" class="nav-link active">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                ADMIN LAYANAN
                <!-- <?=print_r($this->session->userdata())?> -->
                <i class="right fas fa-angle-left"></i>
              </p>
            </a> 
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="#" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p><?=$this->session->userdata('username')?></p>
                </a>
              </li>
            </ul>
            <!--<ul class="nav nav-treeview" style="display:none">
              <li class="nav-item">
                <a href="<?=site_url("tbl_histori_antrian/user")?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Data Antrian Pengguna</p>
                </a>
              </li>
            </ul>-->
            <ul class="nav nav-treeview">
              <li class="nav-item">
                <a href="<?=site_url("Login/Log_out")?>" class="nav-link">
                  <i class="far fa-circle nav-icon"></i>
                  <p>Log Out</p>
                </a>
              </li>
            </ul>
          </li> 
        </ul> 
      </nav>   
    </div>
    <!-- /.sidebar -->
  </aside>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0">Dashboard</h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <!-- <li class="breadcrumb-item"><a href="#">Home</a></li> -->
              <!-- <li class="breadcrumb-item active">Dashboard v1</li> -->
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content"> 
      <?= $contents ?> 
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer">
    <strong>Copyright &copy; 2024 <a href="<?=site_url('')?>">Pemerintah Desa Sidamulya - Sidareja, Cilacap</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 1.0
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="<?=site_url()?>./assets/adminlte/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="<?=site_url()?>./assets/adminlte/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="<?=site_url()?>./assets/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="<?=site_url()?>./assets/adminlte/plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="<?=site_url()?>./assets/adminlte/plugins/sparklines/sparkline.js"></script>
    <!-- This is data table -->
    <script src="<?= site_url('template/') ?>js/datatables.min.js"></script>
<!-- JQVMap -->
<!-- <script src="<?=site_url()?>./assets/adminlte/plugins/jqvmap/jquery.vmap.min.js"></script> -->
<!-- <script src="<?=site_url()?>./assets/adminlte/plugins/jqvmap/maps/jquery.vmap.usa.js"></script> -->
<!-- jQuery Knob Chart -->
<script src="<?=site_url()?>./assets/adminlte/plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="<?=site_url()?>./assets/adminlte/plugins/moment/moment.min.js"></script>
<script src="<?=site_url()?>./assets/adminlte/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="<?=site_url()?>./assets/adminlte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="<?=site_url()?>./assets/adminlte/plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="<?=site_url()?>./assets/adminlte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="<?=site_url()?>./assets/adminlte/dist/js/adminlte.js"></script>
<!-- AdminLTE for demo purposes -->
<!-- <script src="<?=site_url()?>./assets/adminlte/dist/js/demo.js"></script> -->
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="<?=site_url()?>./assets/adminlte/dist/js/pages/dashboard.js"></script>
</body>
</html>
