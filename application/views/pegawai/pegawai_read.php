<div class="row">
	<div class="col-md-12">
		<div class="box box-success">
			<div class="box-header">
				<div class="box-title">
                    <b><i class="fa fa-eye"></i> Detail Data Pegawai</b>
                </div>
                <div class="box-tools pull-right">
                    <button type="button" class="toggle-expand-btn btn btn-default btn-sm"><i class="fa fa-expand"></i></button>
                </div>
			</div>
			<div class="box-body">
				<div style="padding: 15px;">
					<table class="table table-striped">
						<tr>
							<td width="20%"><b>Nama Depan</b></td>
							<td><?= $nama_depan; ?></td>
						</tr>
						<tr>
							<td width="20%"><b>Nama Belakang</b></td>
							<td><?= $nama_belakang; ?></td>
						</tr>
						<tr>
							<td width="20%"><b>Nik</b></td>
							<td><?= $nik; ?></td>
						</tr>
						<tr>
							<td width="20%"><b>Nip</b></td>
							<td><?= $nip; ?></td>
						</tr>
						<tr>
							<td width="20%"><b>Jabatan</b></td>
							<td><?= $jabatan; ?></td>
						</tr>
						<tr>
							<td width="20%"><b>Unit Kerja</b></td>
							<td><?= $unit_kerja; ?></td>
						</tr> 
					</table>
					<a href="<?= site_url('pegawai') ?>" class="btn btn-danger pull-right">
						<i class="fa fa-sign-out"></i> Kembali
					</a>
				</div>
			</div>
		</div>
	</div>
</div>