<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header">
                <div class="box-title">
                    <b><i class="fa fa-tasks"></i> <?= $button ?> Data Tabel_laporan_tugas</b>
                </div>
                <div class="box-tools pull-right">
                    <button type="button" class="toggle-expand-btn btn btn-default btn-sm"><i class="fa fa-expand"></i></button>
                </div>
            </div>
            <div class="box-body">
                <form style="padding: 15px;" action="<?= $action; ?>" method="POST" enctype="multipart/form-data">
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="int">Id Tugas</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="id_tugas" id="id_tugas" placeholder="Id Tugas" value="<?= $id_tugas; ?>" />
                                <?= form_error('id_tugas') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="int">Id Gugus</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="id_gugus" id="id_gugus" placeholder="Id Gugus" value="<?= $id_gugus; ?>" />
                                <?= form_error('id_gugus') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="varchar">Link</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="link" id="link" placeholder="Link" value="<?= $link; ?>" />
                                <?= form_error('link') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="varchar">Acc</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="acc" id="acc" placeholder="Acc" value="<?= $acc; ?>" />
                                <?= form_error('acc') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-2">
                              
                                <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> <?= $button ?></button>
                                <a href="<?= site_url('tabel_laporan_tugas') ?>" class="btn btn-danger"><i class="fa fa-sign-out"></i> Kembali</a>
                            </div>
                        </div>
                    </div>
				</form>
            </div>
        </div>
    </div>
</div>