<div class="row"> 
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <div class="box-title">
                    <b><i class="fa fa-list-alt"></i> Data Tabel_laporan_tugas</b>
                </div>
                <div class="box-tools pull-right">
                    <button type="button" class="toggle-expand-btn btn btn-default btn-sm"><i class="fa fa-expand"></i></button>
                </div>
            </div>
            <div class="box-body"> 
                <div class="table-responsive" style="padding: 15px">
                    <!-- <input type="text" id="id_tugas" name="id_tugas" class="col-md-2"> -->
                    <table class="table table-bordered table-striped table-hover" width="100%" id="mytable">
                        <thead>
                            <tr>
                                <th width="5%">No</th>
                                <th>Tugas</th>
                                <th>Gugus Kerja</th>
                                <th>Dokumen</th>
                                <th>Status Acc</th>
                                <th width="15%">Aksi</th>
                            </tr>
                        </thead>    
                    </table>

                    <script src="<?= base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
                    <script type="text/javascript">
                        $(document).ready(function() {
                            $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
                            {
                                return {
                                    "iStart": oSettings._iDisplayStart,
                                    "iEnd": oSettings.fnDisplayEnd(),
                                    "iLength": oSettings._iDisplayLength,
                                    "iTotal": oSettings.fnRecordsTotal(),
                                    "iFilteredTotal": oSettings.fnRecordsDisplay(),
                                    "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                                    "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                                };
                            };  
                            var dataTable = $("#mytable").dataTable({
                                "processing"  : true,
                                "serverSide"  : true,
                                "oLanguage"   : { sProcessing : "Loading. . ." },
                                "ajax"        : { "url" : "<?= site_url('tabel_laporan_tugas/jsonn/')?>", "type": "POST"},
                                "columns"     : [
                                    {
                                        "data": "id",
                                        "orderable": false,
                                        "className" : "text-center"
                                    },
                                    {
                                        "data": "tugas"
                                    },
                                    {
                                        "data": "nama_gugus"
                                    },
                                    {
                                        "data": "link"
                                    },
                                    {
                                        "data": "acc",
                                            render: function (data, type, row) {
                                                // if(row.acc==0){
                                                // return "<a class='btn btn-sm btn-success' title='acc laporan' href='<?=site_url('tabel_laporan_tugas/aprovall/1/')?>"+row.id+"'><i class='bi bi-check2'></i></a> <a class='btn btn-sm btn-danger' title='kembalikan laporan' href='<?=site_url('tabel_laporan_tugas/aprovall/2/')?>"+row.id+"'><i class='bi bi-escape'></i></a>";
                                                // }else 
                                                if(row.acc==1){
                                                    return "<span>Disetujui</span>";
                                                }else if(row.acc==2){
                                                    return "<span>Dikembalikan</span>";                           
                                                }else{
                                                    return "<a class='btn btn-sm btn-success' title='acc laporan' href='<?=site_url('tabel_laporan_tugas/aprovall/1/')?>"+row.id+"'><i class='bi bi-check2'></i></a> <a class='btn btn-sm btn-danger' title='kembalikan laporan' href='<?=site_url('tabel_laporan_tugas/aprovall/2/')?>"+row.id+"'><i class='bi bi-escape'></i></a>";
                                                }
                                            }
                                    },
                                    {
                                        "data" : "action",
                                        "orderable": false,
                                        "className" : "text-center",
                                            render: function (data, type, row) {
                                                return "<a class='btn btn-sm btn-success' title='Unduh Dokumen Laporan' href='<?=site_url('./uploads/laporan/')?>"+row.link+"'><i class='bi bi-download'></i></a>";
                                            }
                                    }
                                ],
                                order: [[0, 'desc']],
                                rowCallback: function(row, data, iDisplayIndex) {
                                    var info = this.fnPagingInfo();
                                    var page = info.iPage;
                                    var length = info.iLength;
                                    var index = page * length + (iDisplayIndex + 1);
                                    $('td:eq(0)', row).html(index);
                                }
                            }); 
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>
<?php // swal_delete("#mytable",".hapus") ?>
        <?= anchor(site_url('tabel_laporan_tugas/excel'), '<i class="fa fa-file-excel-o" aria-hidden="true"></i> Export Excel', 'class="btn btn-success"'); ?>