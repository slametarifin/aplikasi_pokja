<div class="row"> 
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <div class="box-title">
                    <b><i class="fa fa-list-alt"></i> Data Tabel_opd</b>
                </div>
                <div class="box-tools pull-right">
                    <button type="button" class="toggle-expand-btn btn btn-default btn-sm"><i class="fa fa-expand"></i></button>
                </div>
            </div>
            <div class="box-body">
                <a href="<?= site_url('tabel_opd/create') ?>" class="btn btn-primary col-md-2" style="margin-left : 15px;display: <?=$display?>"><i class="fa fa-plus"></i> Tambah Data</a>
                <div class="table-responsive" style="padding: 15px">
                    <table class="table table-bordered table-striped table-hover" width="100%" id="mytable">
                        <thead>
                            <tr>
                                <th width="5%">No</th>
								<th>Kode Opd</th>
								<th>Nama Opd</th>
								<th>Created Date</th>
								<th>Deleted Date</th>
								<th>Deleted By</th>
								<th width="15%">Aksi</th>
                            </tr>
                        </thead>    
                    </table>

                    <script src="<?= base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
                    <script type="text/javascript">
                        $(document).ready(function() {
                            $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
                            {
                                return {
                                    "iStart": oSettings._iDisplayStart,
                                    "iEnd": oSettings.fnDisplayEnd(),
                                    "iLength": oSettings._iDisplayLength,
                                    "iTotal": oSettings.fnRecordsTotal(),
                                    "iFilteredTotal": oSettings.fnRecordsDisplay(),
                                    "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                                    "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                                };
                            };

                            var t = $("#mytable").dataTable({
                                "processing"  : true,
                                "serverSide"  : true,
                                "oLanguage"   : { sProcessing : "Loading. . ." },
                                "ajax"        : { "url" : "<?= site_url('tabel_opd/json') ?>", "type": "POST"},
                                "columns"     : [
                                    {
                                        "data": "id",
                                        "orderable": false,
                                        "className" : "text-center"
                                    },
									{
										"data": "kode_opd"
									},
									{
										"data": "nama_opd"
									},
									{
										"data": "created_date"
									},
									{
										"data": "deleted_date"
									},
									{
										"data": "deleted_by"
									},
                                    {
                                        "data" : "action",
                                        "orderable": false,
                                        "className" : "text-center",
                                            render: function (data, type, row) {
                                                var akses="<?=$akses?>";
                                                if(akses==2){
                                                    return "<a title='Data Pegwai' class='btn btn-sm btn-info' href='<?=site_url('tabel_pegawai/opd/')?>"+row.id+"'><i class='bi bi-people-fill'></i></a> <a title='Data Gugus Tugas' class='btn btn-sm btn-primary' href='<?=site_url('tabel_gugus/opd/')?>"+row.id+"'><i class='bi bi-escape'></i></a> <a title='Data Tugas' class='btn btn-sm btn-warning' href='<?=site_url('tabel_tugas_gugus/opd/')?>"+row.id+"'><i class='bi bi-boxes'></i></a> "+row.action;
                                                }
                                                    else{
                                                        return "<a title='Data Pegwai' class='btn btn-sm btn-info' href='<?=site_url('tabel_pegawai/opd/')?>"+row.id+"'><i class='bi bi-people-fill'></i></a> <a title='Data Tugas' class='btn btn-sm btn-warning' href='<?=site_url('tabel_tugas_gugus/opd/')?>"+row.id+"'><i class='bi bi-boxes'></i></a> <a title='Data Gugus Tugas' class='btn btn-sm btn-primary' href='<?=site_url('tabel_gugus/opd/')?>"+row.id+"'><i class='bi bi-escape'></i></a>";
                                                    }
                                            }
                                    }
                                ],
                                order: [[0, 'desc']],
                                rowCallback: function(row, data, iDisplayIndex) {
                                    var info = this.fnPagingInfo();
                                    var page = info.iPage;
                                    var length = info.iLength;
                                    var index = page * length + (iDisplayIndex + 1);
                                    $('td:eq(0)', row).html(index);
                                }
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>
<?php // swal_delete("#mytable",".hapus") ?>
		<?= anchor(site_url('tabel_opd/excel'), '<i class="fa fa-file-excel-o" aria-hidden="true"></i> Export Excel', 'class="btn btn-success"'); ?>