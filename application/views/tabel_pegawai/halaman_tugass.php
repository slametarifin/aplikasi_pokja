<div class="row"> 
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <div class="box-title">
                    <b> Data tabel_tugas_gugus</b>
                </div> 
            </div>
            <div class="box-body"> 
                <!-- <a href="<?= site_url('tabel_gugus') ?>" class="btn btn-danger" style="margin-left : 15px"><i class="fa fa-plus"></i> Kembali</a>  -->
                <div class="table-responsive" style="padding: 15px">
                    <table class="table table-bordered table-striped table-hover" width="100%" id="mytable_tugas">
                        <thead>
                            <tr>
                                <th width="5%">No</th>
                                <th>Gugus Tugas</th>
                                <th>Tugas</th>
                                <th>Deskripsi</th>
                                <th>PIC<br>Tugas</th>
                                <th>Status</th>
                                <th width="15%">Aksi</th>
                            </tr>
                        </thead>    
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- <button class="my-button" data-id="123">Click Me</button>  -->
<script src="<?= base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
<script type="text/javascript">
    $(document).ready(function() {
        $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
        {
            return {
                "iStart": oSettings._iDisplayStart,
                "iEnd": oSettings.fnDisplayEnd(),
                "iLength": oSettings._iDisplayLength,
                "iTotal": oSettings.fnRecordsTotal(),
                "iFilteredTotal": oSettings.fnRecordsDisplay(),
                "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
            };
        };

        var t = $("#mytable_tugas").dataTable({
            "processing"  : true,
            "serverSide"  : true,
            "oLanguage"   : { sProcessing : "Loading. . ." },
            "ajax"        : { "url" : "<?= site_url('tabel_tugas_gugus/json_tugas/'.$this->uri->segment(3)) ?>", "type": "POST"},
            "columns"     : [
                { 
                    "data": "id",
                    "orderable": false,
                    "className" : "text-center"
                },
                {
                    "data": "nama_gugus"
                },
                {
                    "data": "id",
                        render: function (data, type, row) {
                        	return row.nm_tugas;
                        }
                },
                {
                    "data": "deskripsi"
                },
                {
                    "data": "nama"
                },
                {
                    "data": "status",
                        render: function (data, type, row) {
                            if(data.status=="true"){
                                return "<label class='lbl lbl-success'>Sudah Selesai</label>"
                            }
                            else if(data.status=="prosess"){
                                return "<label class='lbl lbl-success'>Peninjauan</label>"
                            }else{
                                return "<label class='lbl lbl-warning'>Belum Selesai</label>"
                            }
                        }
                },
                {
                    "data" : "action",
                    "orderable": false,
                    "className" : "text-center",
                        render: function (data, type, row) { 
                            if(data.status=="true"){
                            return "<button data-bs-toggle='modal' data-bs-target='#ExtralargeModal' data-id='"+row.id+"' id='modal_lap' class='btn btn-sm btn-info btn-show-modal' title='Unduh Laporan'><i class='bi bi-download'></i></button> <a href='<?=site_url('tabel_tugas_gugus/read/')?>"+row.id+"' title='Lihat Detail Data' class='btn btn-sm btn-success'><i class='bi bi-eye'></i></a> <a href='<?=site_url('tabel_tugas_gugus/update/')?>"+row.id+"' title='Ubah Data' class='btn btn-sm btn-warning'><i class='bi bi-pencil'></i></a>";
                            }else{
                            return "<a href='<?=site_url('tabel_laporan_tugas/laporan_tugas/')?>"+row.id+"' class='btn btn-sm btn-primary' title='upload laporan'><i class='bi bi-upload'></i></a> <a href='<?=site_url('tabel_laporan_tugas/bukti/')?>"+row.id+"/"+row.id_gugus_tugas+"'  data-id='"+row.id+"' id='modal_lap' class='btn btn-sm btn-info btn-show-modal' title='Unduh Laporan'><i class='bi bi-download'></i></a> <a href='<?=site_url('tabel_tugas_gugus/read/')?>"+row.id+"' title='Lihat Detail Data' class='btn btn-sm btn-success'><i class='bi bi-eye'></i></a> <a href='<?=site_url('tabel_tugas_gugus/update/')?>"+row.id+"' title='Ubah Data' class='btn btn-sm btn-warning'><i class='bi bi-pencil'></i></a> <a href='<?=site_url('tabel_tugas_gugus/delete/')?>"+row.id+"' title='Hapus Data' class='btn btn-sm btn-danger'><i class='bi bi-trash'></i></a> ";//data-bs-target='#ExtralargeModal' data-bs-toggle='modal' 
                            }
                        },
                }
            ],
        });
        $('#mytable_tugas').on('click', '.btn-show-modal', function() { 
            var dataId = $(this).data('id'); // Get the data-id attribute value 
            $.ajax({
                url: '<?=site_url('tabel_laporan_tugas/set_session')?>',
                type: 'POST',
                data: { id_tugas: dataId },
                success: function(response) { 
                    $('#mytable').DataTable().ajax.reload();
                    var modalBody = $(this).find('.modal-body');
                    modalBody.html('<p>New content</p>');
                    $('#ExtralargeModal').modal('show');   
                },
                error: function(xhr, status, error) {
                    console.error(error);
                }
            });
          });
        $('#ExtralargeModal').on('show.bs.modal', function() {
          var modalBody = $(this).find('.modal-body'); 
          modalBody.html("<style>table tr{border:1px solid;}</style><tabel style='border:1px solid'><tr><td style='cellspacing:0'>No</td><td>Dokumen</td></tr></table>");
              });
          }); </script>
<div class="modal fade" id="ExtralargeModal" tabindex="-1">
    <div class="modal-dialog modal-xl">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Dokumen Laporan Tugas</h5>
          <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
        </div>
        <div class="modal-body">
            <?php $this->load->view('tabel_laporan_tugas/tabel_laporan_tugas_list');?>
            <!-- <input type="text" name="id_tugas" id="id_tugas" class="col-md-2"> -->
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-danger" data-bs-dismiss="modal">Tutup</button>
          <!-- <button type="button" class="btn btn-primary">Save changes</button> -->
        </div>
      </div>
    </div>
</div>
</html>