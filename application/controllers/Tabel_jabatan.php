<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tabel_jabatan extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Tabel_jabatan_model');
        $this->load->library('form_validation');
		$this->load->library('datatables');
    }

    public function index()
    {
        $this->template->load('template_antrian','tabel_jabatan/tabel_jabatan_list');
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Tabel_jabatan_model->json();
    }

    public function read($id) 
    {
        $row = $this->Tabel_jabatan_model->get_by_id($id);
        if ($row) {
            $data = array(
				'id' => $row->id,
				'nama_jabatan' => $row->nama_jabatan,
				'id_opd' => $row->id_opd,
			);
            $this->template->load('template_antrian','tabel_jabatan/tabel_jabatan_read', $data);
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('tabel_jabatan'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Tambah',
            'action' => site_url('tabel_jabatan/create_action'),
			'id' => set_value('id'),
			'nama_jabatan' => set_value('nama_jabatan'),
			'id_opd' => set_value('id_opd'),
		);
        $this->template->load('template_antrian','tabel_jabatan/tabel_jabatan_form', $data);
    }
    
    public function create_action() 
    { 
            $data = array(
				'nama_jabatan' => $this->input->post('nama_jabatan',TRUE),
				'id_opd' => $this->input->post('id_opd',TRUE),
			);

            $this->Tabel_jabatan_model->insert($data);
            $this->session->set_flashdata('success', 'Berhasil Tambah Data');
            redirect(site_url('tabel_jabatan')); 
    }
    
    public function update($id) 
    {
        $row = $this->Tabel_jabatan_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Ubah',
                'action' => site_url('tabel_jabatan/update_action/'.$id),
				'id' => set_value('id', $row->id),
				'nama_jabatan' => set_value('nama_jabatan', $row->nama_jabatan),
				'id_opd' => set_value('id_opd', $row->id_opd),
			);
            $this->template->load('template_antrian','tabel_jabatan/tabel_jabatan_form', $data);
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('tabel_jabatan'));
        }
    }
    
    public function update_action($id) 
    { 
            $data = array(
				'nama_jabatan' => $this->input->post('nama_jabatan',TRUE),
				'id_opd' => $this->input->post('id_opd',TRUE),
			);

            $this->Tabel_jabatan_model->update($id, $data);
            $this->session->set_flashdata('success', 'Berhasil Edit Data');
            redirect(site_url('tabel_jabatan')); 
    }
    
    public function delete($id) 
    {
        $row = $this->Tabel_jabatan_model->get_by_id($id);

        if ($row) {
            $this->Tabel_jabatan_model->delete($id);
            $this->session->set_flashdata('success', 'Berhasil Hapus Data');
            redirect(site_url('tabel_jabatan'));
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('tabel_jabatan'));
        }
    }

    public function _rules() 
    {
		$this->form_validation->set_rules('nama_jabatan', 'nama jabatan', 'trim|required');
		$this->form_validation->set_rules('id_opd', 'id opd', 'trim|required');

		$this->form_validation->set_rules('', '', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "tabel_jabatan.xls";
        $judul = "tabel_jabatan";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Nama Jabatan");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Opd");

	foreach ($this->Tabel_jabatan_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->nama_jabatan);
	    xlsWriteNumber($tablebody, $kolombody++, $data->id_opd);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Tabel_jabatan.php */
/* Location: ./application/controllers/Tabel_jabatan.php */
/* Please DO NOT modify this information : */
/* Generated by CRUDV2 Generator For AdminLTE Template 2024-01-09 04:59:41 */