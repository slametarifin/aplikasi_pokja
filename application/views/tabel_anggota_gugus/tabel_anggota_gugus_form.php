<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header">
                <div class="box-title">
                    <b><i class="fa fa-tasks"></i> <?= $button ?> Data Tabel_anggota_gugus</b>
                </div>
                <div class="box-tools pull-right">
                    <button type="button" class="toggle-expand-btn btn btn-default btn-sm"><i class="fa fa-expand"></i></button>
                </div>
            </div>
            <div class="box-body">
                <form style="padding: 15px;" action="<?= $action; ?>" method="POST" enctype="multipart/form-data">
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="int">Gugus Tugas</label>
                            <div class="col-md-6">
                                <select name="id_gugus" class="form-control">
                                    <option value="-">pilih data</option>
                                    <?php
                                    $gugus_tugas = $this->db->query("select id,nama_gugus from tabel_gugus")->result();
                                    foreach($gugus_tugas as $gt){?>
                                        <option value="<?=$gt->id?>" <?php if($id_gugus==$gt->id){?> selected <?php }?>><?=$gt->nama_gugus?></option>
                                    <?php }
                                    ?>
                                </select>
                                <!-- <input type="text" class="form-control" name="id_gugus" id="id_gugus" placeholder="Id Gugus" value="<?= $id_gugus; ?>" /> -->
                                <?= form_error('id_gugus') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="varchar">Pegawai</label>
                            <div class="col-md-6">
                                <select name="nip" class="form-control">
                                    <option value="-">pilih data</option>
                                    <?php
                                    $gugus_tugas = $this->db->query("select id_peg,nip,nama from tabel_pegawai")->result();
                                    foreach($gugus_tugas as $gt){?>
                                        <option value="<?=$gt->nip?>" <?php if($nip==$gt->nip){?> selected <?php }?>><?=$gt->nama?></option>
                                    <?php }
                                    ?>
                                </select>
                                <!-- <input type="text" class="form-control" name="nip" id="nip" placeholder="Nip" value="<?= $nip; ?>" /> -->
                                <?= form_error('nip') ?>
                            </div>
                        </div>
                    </div>
					<!-- <div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="varchar">Status</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="status" id="status" placeholder="Status" value="<?= $status; ?>" />
                                <?= form_error('status') ?>
                            </div>
                        </div>
                    </div> -->
					<div class="form-group">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-2">
                              
                                <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> <?= $button ?></button>
                                <a href="<?= site_url('tabel_anggota_gugus') ?>" class="btn btn-danger"><i class="fa fa-sign-out"></i> Kembali</a>
                            </div>
                        </div>
                    </div>
				</form>
            </div>
        </div>
    </div>
</div>