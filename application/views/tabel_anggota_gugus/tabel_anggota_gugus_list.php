<div class="row"> 
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header with-border">
                <div class="box-title">
                    <b><i class="fa fa-list-alt"></i> Data Tabel_anggota_gugus</b>
                </div>
                <div class="box-tools pull-right">
                    <button type="button" class="toggle-expand-btn btn btn-default btn-sm"><i class="fa fa-expand"></i></button>
                </div>
            </div>
            <div class="box-body">
                <!-- <a href="<?= site_url('tabel_anggota_gugus/create') ?>" class="btn btn-primary" style="margin-left : 15px"><i class="fa fa-plus"></i> Tambah Data</a> -->
                <div class="table-responsive" style="padding: 15px">
                    <table class="table table-bordered table-striped table-hover" width="100%" id="mytable">
                        <thead>
                            <tr>
                                <th width="5%">No</th>
								<th>Gugus Kerja</th>
								<th>NIP<br>PEGAWAI</th>
								<th>Status</th>
								<th width="15%">Aksi</th>
                            </tr>
                        </thead>    
                    </table>

                    <script src="<?= base_url('assets/js/jquery-1.11.2.min.js') ?>"></script>
                    <script type="text/javascript">
                        $(document).ready(function() {
                            $.fn.dataTableExt.oApi.fnPagingInfo = function(oSettings)
                            {
                                return {
                                    "iStart": oSettings._iDisplayStart,
                                    "iEnd": oSettings.fnDisplayEnd(),
                                    "iLength": oSettings._iDisplayLength,
                                    "iTotal": oSettings.fnRecordsTotal(),
                                    "iFilteredTotal": oSettings.fnRecordsDisplay(),
                                    "iPage": Math.ceil(oSettings._iDisplayStart / oSettings._iDisplayLength),
                                    "iTotalPages": Math.ceil(oSettings.fnRecordsDisplay() / oSettings._iDisplayLength)
                                };
                            };

                            var t = $("#mytable").dataTable({
                                "processing"  : true,
                                "serverSide"  : true,
                                "oLanguage"   : { sProcessing : "Loading. . ." },
                                "ajax"        : { "url" : "<?= site_url('tabel_anggota_gugus/json') ?>", "type": "POST"},
                                "columns"     : [
                                    {
                                        "data": "id",
                                        "orderable": false,
                                        "className" : "text-center"
                                    },
									{
										"data": "id_gugus",
                                            render: function (data, type, row) {
                                                      return row.nama_gugus; 
                                                // var gugus=row.id_gugus;
                                                // $.ajax
                                                // ({
                                                //   type: "POST",
                                                //   url: "<?=site_url('tabel_anggota_gugus/get_data')?>",
                                                //   data: {id:row.id_gugus},
                                                //   success: function(html)
                                                //   {  
                                                //       return html.nama_gugus; 
                                                //   },
                                                //     error: function (xhr, ajaxOptions, thrownError) {
                                                //      return "-";//row.id_gugus;  
                                                //     }
                                                // }); 
                                            }
									},
									{
										"data": "nip",
                                            render: function (data, type, row) {
                                                      return row.nama+"<br>"+row.nip;
                                                  } 
									},
									{
										"data": "status"
									},
                                    {
                                        "data" : "action",
                                        "orderable": false,
                                        "className" : "text-center"
                                    }
                                ],
                                order: [[0, 'desc']],
                                rowCallback: function(row, data, iDisplayIndex) {
                                    var info = this.fnPagingInfo();
                                    var page = info.iPage;
                                    var length = info.iLength;
                                    var index = page * length + (iDisplayIndex + 1);
                                    $('td:eq(0)', row).html(index);
                                }
                            });
                        });
                    </script>
                </div>
            </div>
        </div>
    </div>
</div>
<?php // swal_delete("#mytable",".hapus") ?>
		<?= anchor(site_url('tabel_anggota_gugus/excel'), '<i class="fa fa-file-excel-o" aria-hidden="true"></i> Export Excel', 'class="btn btn-success"'); ?>