<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tbl_menu extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Tbl_menu_model');
        $this->load->library('form_validation');
		$this->load->library('datatables');
    }

    public function index()
    {
        $cek_role=$this->db->query("select id,role,user_name from tabel_akun where user_name='".$this->session->userdata('username')."'")->row();
        if($cek_role->role==2||$cek_role->role==1){
            $display="block";
        }elseif($cek_role->role==0){
            $display="none";
        }
        $data=array('id_user'=>$cek_role->id,'akses'=>$cek_role->role,'display'=>$display);
        $this->template->load('template_antrian','tbl_menu/tbl_menu_list',$data);
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Tbl_menu_model->json();
    }

    public function read($id) 
    {
        $row = $this->Tbl_menu_model->get_by_id($id);
        if ($row) {
            $data = array(
				'id' => $row->id,
				'menu' => $row->menu,
				'url' => $row->url,
				'ket' => $row->ket,
				'parent' => $row->parent,
			);
            $this->template->load('template_antrian','tbl_menu/tbl_menu_read', $data);
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('tbl_menu'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Tambah',
            'action' => site_url('tbl_menu/create_action'),
			'id' => set_value('id'),
			'menu' => set_value('menu'),
			'url' => set_value('url'),
			'ket' => set_value('ket'),
			'parent' => set_value('parent'),
		);
        $this->template->load('template_antrian','tbl_menu/tbl_menu_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
				'menu' => $this->input->post('menu',TRUE),
				'url' => $this->input->post('url',TRUE),
				'ket' => $this->input->post('ket',TRUE),
				'parent' => $this->input->post('parent',TRUE),
			);

            $this->Tbl_menu_model->insert($data);
            $this->session->set_flashdata('success', 'Berhasil Tambah Data');
            redirect(site_url('tbl_menu'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Tbl_menu_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Ubah',
                'action' => site_url('tbl_menu/update_action/'.$id),
				'id' => set_value('id', $row->id),
				'menu' => set_value('menu', $row->menu),
				'url' => set_value('url', $row->url),
				'ket' => set_value('ket', $row->ket),
				'parent' => set_value('parent', $row->parent),
			);
            $this->template->load('template_antrian','tbl_menu/tbl_menu_form', $data);
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('tbl_menu'));
        }
    }
    
    public function update_action($id) 
    { 
            $data = array(
				'menu' => $this->input->post('menu',TRUE),
				'url' => $this->input->post('url',TRUE),
				'ket' => $this->input->post('ket',TRUE),
				'parent' => $this->input->post('parent',TRUE),
			);

            $this->Tbl_menu_model->update($id, $data);
            $this->session->set_flashdata('success', 'Berhasil Edit Data');
            redirect(site_url('tbl_menu')); 
    }
    
    public function delete($id) 
    {
        $row = $this->Tbl_menu_model->get_by_id($id);

        if ($row) {
            $this->Tbl_menu_model->delete($id);
            $this->session->set_flashdata('success', 'Berhasil Hapus Data');
            redirect(site_url('tbl_menu'));
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('tbl_menu'));
        }
    }

    public function _rules() 
    {
		$this->form_validation->set_rules('menu', 'menu', 'trim|required');
		$this->form_validation->set_rules('url', 'url', 'trim|required');
		$this->form_validation->set_rules('ket', 'ket', 'trim|required');
		$this->form_validation->set_rules('parent', 'parent', 'trim|required');

		$this->form_validation->set_rules('', '', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "tbl_menu.xls";
        $judul = "tbl_menu";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Menu");
	xlsWriteLabel($tablehead, $kolomhead++, "Url");
	xlsWriteLabel($tablehead, $kolomhead++, "ket");
	xlsWriteLabel($tablehead, $kolomhead++, "Parent");

	foreach ($this->Tbl_menu_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->menu);
	    xlsWriteLabel($tablebody, $kolombody++, $data->url);
	    xlsWriteLabel($tablebody, $kolombody++, $data->ket);
	    xlsWriteNumber($tablebody, $kolombody++, $data->parent);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Tbl_menu.php */
/* Location: ./application/controllers/Tbl_menu.php */
/* Please DO NOT modify this information : */
/* Generated by CRUDV2 Generator For AdminLTE Template 2023-07-14 09:27:20 */