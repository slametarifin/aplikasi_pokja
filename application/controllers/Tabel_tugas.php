<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tabel_tugas extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Tabel_tugas_model');
        $this->load->library('form_validation');
		$this->load->library('datatables');
    }

    public function index()
    {
        $this->template->load('template_antrian','tabel_tugas/tabel_tugas_list');
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Tabel_tugas_model->json();
    }

    public function read($id) 
    {
        $row = $this->Tabel_tugas_model->get_by_id($id);
        if ($row) {
            $data = array(
				'id' => $row->id,
				'tugas' => $row->tugas,
				'deskripsi' => $row->deskripsi,
				'pic' => $row->pic,
				'id_opd' => $row->id_opd,
				'id_bidang' => $row->id_bidang,
			);
            $this->template->load('template_antrian','tabel_tugas/tabel_tugas_read', $data);
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('tabel_tugas'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Tambah',
            'action' => site_url('tabel_tugas/create_action'),
			'id' => set_value('id'),
			'tugas' => set_value('tugas'),
			'deskripsi' => set_value('deskripsi'),
			'pic' => set_value('pic'),
			'id_opd' => set_value('id_opd'),
			'id_bidang' => set_value('id_bidang'),
		);
        $this->template->load('template_antrian','tabel_tugas/tabel_tugas_form', $data);
    }
    
    public function create_action() 
    { 
            $data = array(
				'tugas' => $this->input->post('tugas',TRUE),
				'deskripsi' => $this->input->post('deskripsi',TRUE),
				'pic' => $this->input->post('pic',TRUE),
				'id_opd' => $this->input->post('id_opd',TRUE),
				'id_bidang' => $this->input->post('id_bidang',TRUE),
			);

            $this->Tabel_tugas_model->insert($data);
            $this->session->set_flashdata('success', 'Berhasil Tambah Data');
            redirect(site_url('tabel_tugas')); 
    }
    
    public function update($id) 
    { 
        $row = $this->Tabel_tugas_model->get_by_id($id);
        if($row){
            $data = array(
                'button' => 'Ubah',
                'action' => site_url('tabel_tugas/update_action/'.$id),
				'id' => set_value('id', $row->id),
				'tugas' => set_value('tugas', $row->tugas),
				'deskripsi' => set_value('deskripsi', $row->deskripsi),
				'pic' => set_value('pic', $row->pic),
				'id_opd' => set_value('id_opd', $row->id_opd),
				'id_bidang' => set_value('id_bidang', $row->id_bidang),
			);
            $this->template->load('template_antrian','tabel_tugas/tabel_tugas_form', $data);
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('tabel_tugas')); 
        }
    }
    
    public function update_action($id) 
    { 
            $data = array(
				'tugas' => $this->input->post('tugas',TRUE),
				'deskripsi' => $this->input->post('deskripsi',TRUE),
				'pic' => $this->input->post('pic',TRUE),
				'id_opd' => $this->input->post('id_opd',TRUE),
				'id_bidang' => $this->input->post('id_bidang',TRUE),
			);

            $this->Tabel_tugas_model->update($id, $data);
            $this->session->set_flashdata('success', 'Berhasil Edit Data');
            redirect(site_url('tabel_tugas')); 
    }
    
    public function delete($id) 
    {
        $row = $this->Tabel_tugas_model->get_by_id($id);

        if ($row) {
            $this->Tabel_tugas_model->delete($id);
            $this->session->set_flashdata('success', 'Berhasil Hapus Data');
            redirect(site_url('tabel_tugas'));
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('tabel_tugas'));
        }
    }

    public function _rules() 
    {
		$this->form_validation->set_rules('tugas', 'tugas', 'trim|required');
		$this->form_validation->set_rules('deskripsi', 'deskripsi', 'trim|required');
		$this->form_validation->set_rules('pic', 'pic', 'trim|required');
		$this->form_validation->set_rules('id_opd', 'id opd', 'trim|required');
		$this->form_validation->set_rules('id_bidang', 'id bidang', 'trim|required');

		$this->form_validation->set_rules('', '', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "tabel_tugas.xls";
        $judul = "tabel_tugas";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Tugas");
	xlsWriteLabel($tablehead, $kolomhead++, "Deskripsi");
	xlsWriteLabel($tablehead, $kolomhead++, "Pic");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Opd");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Bidang");

	foreach ($this->Tabel_tugas_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->tugas);
	    xlsWriteLabel($tablebody, $kolombody++, $data->deskripsi);
	    xlsWriteNumber($tablebody, $kolombody++, $data->pic);
	    xlsWriteNumber($tablebody, $kolombody++, $data->id_opd);
	    xlsWriteNumber($tablebody, $kolombody++, $data->id_bidang);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Tabel_tugas.php */
/* Location: ./application/controllers/Tabel_tugas.php */
/* Please DO NOT modify this information : */
/* Generated by CRUDV2 Generator For AdminLTE Template 2024-01-08 08:13:11 */