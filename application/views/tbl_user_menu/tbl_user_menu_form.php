  <script src="https://cdn.jsdelivr.net/npm/jquery@3.5.0/dist/jquery.slim.min.js"></script>
  <div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header">
                <div class="box-title">
                    <b><i class="fa fa-tasks"></i> <?= $button ?> Data Tbl_user_menu</b>
                </div>
                <div class="box-tools pull-right">
                    <button type="button" class="toggle-expand-btn btn btn-default btn-sm"><i class="fa fa-expand"></i></button>
                </div>
            </div>
            <div class="box-body">
                <form style="padding: 15px;" action="<?= $action; ?>" method="POST" enctype="multipart/form-data">
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="int">Nama Pegawai</label>
                            <div class="col-md-6">
                                <select name="id_user" class="form-control select2 form-select">
                                    <option value="-">pilih data</option>
                                    <?php 
                                    $data_peg=$this->db->query("
                                        select id_peg,nama,id_opd,nama_opd,nip from tabel_pegawai
                                        join tabel_opd 
                                        on tabel_opd.id = tabel_pegawai.id_opd
                                        ")->result();
                                    foreach ($data_peg as $dp) {?>
                                        <option value="<?=$dp->id_peg?>" <?php if($id_user==$dp->id_peg){?> selected <?php }?>><?=$dp->nama?>|<?=$dp->nip?>|<?=$dp->nama_opd?></option>
                                        <?php
                                    }?>
                                </select>
                                <!-- <input type="text" class="form-control" name="id_user" id="id_user" placeholder="Id User" value="<?= $id_user; ?>" /> -->
                                <?= form_error('id_user') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="int">Menu</label>
                            <div class="col-md-6">
                                <select name="id_menu" class="form-control select2 form-select">
                                    <option value="-">pilih data</option>
                                    <?php 
                                    $data_peg=$this->db->query("
                                        select id,parent,menu from tabel_menu 
                                        ")->result();
                                    foreach ($data_peg as $dp) {?>
                                        <option value="<?=$dp->id?>" <?php if($id_menu==$dp->id){?> selected <?php }?>><?=$dp->menu?></option>
                                        <?php
                                    }?>
                                </select>
                                <!-- <input type="text" class="form-control" name="id_menu" id="id_menu" placeholder="Id Menu" value="<?= $id_menu; ?>" /> -->
                                <?= form_error('id_menu') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-2">
                              
                                <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> <?= $button ?></button>
                                <a href="<?= site_url('tbl_user_menu') ?>" class="btn btn-danger"><i class="fa fa-sign-out"></i> Kembali</a>
                            </div>
                        </div>
                    </div>
				</form>
            </div>
        </div>
    </div>
</div>