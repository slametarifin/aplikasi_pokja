<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tabel_pegawai extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('Tabel_pegawai_model');
        $this->load->library('form_validation');
		$this->load->library('datatables');
    }

    public function index()
    { 
        $cek_role=$this->db->query("
            select a.id,a.role,a.user_name,a.id_user,b.nama,c.nama_jabatan from tabel_akun a 
            join tabel_pegawai b on a.id_user=b.id_peg
            join tabel_jabatan c on b.id_jabatan=c.id
            where a.user_name='".$this->session->userdata('username')."'")->row();
        if($cek_role->role==2){
            $display="block";
        }elseif($cek_role->role==0||$cek_role->role==1){
            $display="none";
        }
        $data=array('kode_jabatan'=>'','id_peg'=>'','nama_opd'=>'','kode_opd'=>'','nip'=>'','nama'=>'','action'=>site_url('pegawai/cari'),'nama_jabatan'=>$cek_role->nama_jabatan,'nama'=>$cek_role->nama,'id_user'=>$cek_role->id_user,'akses'=>$cek_role->role,'display'=>$display);
        $this->template->load('template_antrian','tabel_pegawai/tabel_pegawai_list',$data);
    } 
    public function get_jabatan(){
        $kode_opd=$this->input->post('kode_opd');
        $data=$this->db->query("select * from tabel_jabatan where id_opd='".$kode_opd."'")->result();  
        echo json_encode($data);      
    }
    public function get_pegawai(){
        $kode_opd=$this->input->post('kode_opd');
        $id_jabatan=$this->input->post('kode_jabatan');
        $kode_eselon=$this->input->post('kode_eselon');
        $data=$this->db->query("select (select nama_opd from tabel_opd where tabel_opd.id=tabel_pegawai.id_opd)nama_opd,tabel_pegawai.* from tabel_pegawai where id_opd='".$kode_opd."' and id_eselon='".$kode_eselon."' and id_jabatan=".$id_jabatan)->result(); 
        echo json_encode($data);      
    }

    public function cari()
    {  
        $data['nama']=$this->input->post('nama');
        $data['kode_opd']=$this->input->post('kode_opd');
        $data['kode_jabatan']=$this->input->post('kode_jabatan');
        $pegawai=$this->db->query("select * from tabel_pegawai where id_peg='".$this->input->post('nama')."'")->row();
        // print_r($data);print_r($pegawai);die();
        if($pegawai){
            $data['nip']=$pegawai->nip;
            $data['id_peg']=$pegawai->id_peg;
        }else{
            $data['nip']='-';
            $data['id_peg']='-';
        }
        
        $cek_role=$this->db->query("
            select a.id,a.role,a.user_name,a.id_user,b.nama,c.nama_jabatan from tabel_akun a 
            join tabel_pegawai b on a.id_user=b.id_peg
            join tabel_jabatan c on b.id_jabatan=c.id
            where a.user_name='".$this->session->userdata('username')."'")->row();
        if($cek_role->role==2){
            $display="block";
        }elseif($cek_role->role==0||$cek_role->role==1){
            $display="none";
        }
        $data['action']=site_url('pegawai/cari');
        $data['nama_jabatan']=$cek_role->nama_jabatan; 
        $data['id_user']=$cek_role->id_user;
        $data['akses']=$cek_role->role;
        $data['display']=$display;  
        $this->template->load('template_antrian','tabel_pegawai/tabel_pegawai_list',$data); 
    }

    public function opd($id_opd=null)
    {
        $cek_role=$this->db->query("
            select a.id,a.role,a.user_name,a.id_user,b.nama,c.nama_jabatan from tabel_akun a 
            join tabel_pegawai b on a.id_user=b.id_peg
            join tabel_jabatan c on b.id_jabatan=c.id
            where a.user_name='".$this->session->userdata('username')."'")->row();
        if($cek_role->role==2){
            $display="block";
        }elseif($cek_role->role==0||$cek_role->role==1){
            $display="none";
        }
        $data=array('id_opd'=>$id_opd,'nama_jabatan'=>$cek_role->nama_jabatan,'nama'=>$cek_role->nama,'id_user'=>$id_user,'akses'=>$cek_role->role,'display'=>$display,'row'=>$row);
        // $data['id_opd']=$id_opd;
        $this->template->load('template_antrian','tabel_pegawai/tabel_pegawai_list_opd',$data);
    } 
    
    public function json($nip=null,$nama=null,$opd=null,$jabatan=null) {
        header('Content-Type: application/json');
        echo $this->Tabel_pegawai_model->json($nip,$nama,$opd,$jabatan);
    } 
    
    public function jsonori() {
        header('Content-Type: application/json');
        echo $this->Tabel_pegawai_model->json();
    } 
    
    public function json_opd($id_opd=null) {
        header('Content-Type: application/json');
        echo $this->Tabel_pegawai_model->json_opd($id_opd);
    }
    public function akses_menu($id=null){
        $cek_role=$this->db->query("select id,role,user_name from tabel_akun where user_name='".$this->session->userdata('username')."'")->row();
        if($cek_role->role==2){
            $display="block";
        }elseif($cek_role->role==0||$cek_role->role==1){
            $display="none";
        }
        $data=array('id_user'=>$id,'akses'=>$cek_role->role,'display'=>$display);
        $this->template->load('template_antrian','tbl_user_menu/tbl_user_menu_list_user',$data);        
    }

    public function dashbord() 
    {
        $id_user=$this->session->userdata('id_user');
        $row = $this->Tabel_pegawai_model->get_by_id($id_user);
        // $data['row']=$row;

        $cek_role=$this->db->query("
            select a.id,a.role,a.user_name,a.id_user,b.nama,c.nama_jabatan from tabel_akun a 
            join tabel_pegawai b on a.id_user=b.id_peg
            join tabel_jabatan c on b.id_jabatan=c.id
            where a.user_name='".$this->session->userdata('username')."'")->row();
        if($cek_role->role==2){
            $display="block";
        }elseif($cek_role->role==0||$cek_role->role==1){
            $display="none";
        }
        $data=array('nama_jabatan'=>$cek_role->nama_jabatan,'nama'=>$cek_role->nama,'id_user'=>$id_user,'akses'=>$cek_role->role,'display'=>$display,'row'=>$row);

        $this->template->load('template_antrian','tabel_pegawai/tabel_pegawai_dashbord', $data);
    }

    public function profile($id_user) 
    {
        // $id_user=$this->session->userdata('id_user');
        $row = $this->Tabel_pegawai_model->get_by_id($id_user);
        $data['row']=$row;
        $this->template->load('template_antrian','tabel_pegawai/tabel_pegawai_dashbordd', $data);
    }

    public function read($id) 
    {
        $row = $this->Tabel_pegawai_model->get_by_id($id);
        if ($row) {
            $data = array(
				'id_peg' => $row->id_peg,
				'nip' => $row->nip,
				'nama' => $row->nama,
				'id_opd' => $row->id_opd,
				'id_jabatan' => $row->id_jabatan,
			);
            $this->template->load('template_antrian','tabel_pegawai/tabel_pegawai_read', $data);
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('tabel_pegawai'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Tambah',
            'action' => site_url('tabel_pegawai/create_action'),
			'id_peg' => set_value('id_peg'),
			'nip' => set_value('nip'),
			'nama' => set_value('nama'),
			'id_opd' => set_value('id_opd'),
			'id_jabatan' => set_value('id_jabatan'),
		);
        $this->template->load('template_antrian','tabel_pegawai/tabel_pegawai_form', $data);
    }
    
    public function create_action() 
    { 
            $data = array(
				'nip' => $this->input->post('nip',TRUE),
				'nama' => $this->input->post('nama',TRUE),
				'id_opd' => $this->input->post('id_opd',TRUE),
				'id_jabatan' => $this->input->post('id_jabatan',TRUE),
			);

            $this->Tabel_pegawai_model->insert($data);
            $this->session->set_flashdata('success', 'Berhasil Tambah Data');
            redirect(site_url('tabel_pegawai')); 
    }
    
    public function update($id) 
    {
        $row = $this->Tabel_pegawai_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Ubah',
                'action' => site_url('tabel_pegawai/update_action/'.$id),
				'id_peg' => set_value('id_peg', $row->id_peg),
				'nip' => set_value('nip', $row->nip),
				'nama' => set_value('nama', $row->nama),
				'id_opd' => set_value('id_opd', $row->id_opd),
				'id_jabatan' => set_value('id_jabatan', $row->id_jabatan),
			);
            $this->template->load('template_antrian','tabel_pegawai/tabel_pegawai_form', $data);
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('tabel_pegawai'));
        }
    }
    
    public function update_action($id) 
    { 
            $data = array(
				'nip' => $this->input->post('nip',TRUE),
				'nama' => $this->input->post('nama',TRUE),
				'id_opd' => $this->input->post('id_opd',TRUE),
				'id_jabatan' => $this->input->post('id_jabatan',TRUE),
			);

            $this->Tabel_pegawai_model->update($id, $data);
            $this->session->set_flashdata('success', 'Berhasil Edit Data');
            redirect(site_url('tabel_pegawai')); 
    }
    
    public function delete($id) 
    {
        $row = $this->Tabel_pegawai_model->get_by_id($id);

        if ($row) {
            $this->Tabel_pegawai_model->delete($id);
            $this->session->set_flashdata('success', 'Berhasil Hapus Data');
            redirect(site_url('tabel_pegawai'));
        } else {
            $this->session->set_flashdata('error', 'Data Tidak Ditemukan');
            redirect(site_url('tabel_pegawai'));
        }
    }

    public function _rules() 
    {
		$this->form_validation->set_rules('nip', 'nip', 'trim|required');
		$this->form_validation->set_rules('nama', 'nama', 'trim|required');
		$this->form_validation->set_rules('id_opd', 'id opd', 'trim|required');
		$this->form_validation->set_rules('id_jabatan', 'id jabatan', 'trim|required');

		$this->form_validation->set_rules('', '', 'trim');
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "tabel_pegawai.xls";
        $judul = "tabel_pegawai";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Nip");
	xlsWriteLabel($tablehead, $kolomhead++, "Nama");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Opd");
	xlsWriteLabel($tablehead, $kolomhead++, "Id Jabatan");

	foreach ($this->Tabel_pegawai_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->nip);
	    xlsWriteLabel($tablebody, $kolombody++, $data->nama);
	    xlsWriteNumber($tablebody, $kolombody++, $data->id_opd);
	    xlsWriteNumber($tablebody, $kolombody++, $data->id_jabatan);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

}

/* End of file Tabel_pegawai.php */
/* Location: ./application/controllers/Tabel_pegawai.php */
/* Please DO NOT modify this information : */
/* Generated by CRUDV2 Generator For AdminLTE Template 2024-01-08 08:52:42 */