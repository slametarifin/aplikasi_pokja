<div class="row">
    <div class="col-md-12">
        <div class="box box-success">
            <div class="box-header">
                <div class="box-title">
                    <b>  Data Tbl_grup_pelayanan</b>
                </div> 
            </div>
            <div class="box-body">
                <form style="padding: 15px;" action="<?= $action; ?>" method="POST" enctype="multipart/form-data">
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="int">Instansi</label>
                            <div class="col-md-6">
                                <select class="form-control" name="id_instansi">
                                    <option value='-'>pilih instansi</option>
                                    <?php 
                                        $role='1';
                                        $user=$this->db->query("select id,role,id_instansi from Tbl_pengguna where id=".$this->session->userdata('id_user')."")->row();
                                        if($user){
                                            if($user->role==2){}
                                            else{
                                                $where="where id=".$user->id_instansi."";
                                            }
                                        }
                                        $ins=$this->db->query("select * from Tbl_instansi ".$where."")->result();
                                        foreach($ins as $in){?>
                                        <option value="<?=$in->id?>" <?php if($id_instansi==$in->id){?>selected <?php }?>><?=$in->nm_instansi?></option>
                                        <?php }?>
                                    ?>
                                </select>
                                <!--<input type="text" class="form-control" name="id_instansi" id="id_instansi" placeholder="Id Instansi" value="<?= $id_instansi; ?>" />-->
                                <?= form_error('id_instansi') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <label class="col-md-2" for="varchar">Nm Grup Layanan</label>
                            <div class="col-md-6">
                                <input type="text" class="form-control" name="nm_grup_layanan" id="nm_grup_layanan" placeholder="Nm Grup Layanan" value="<?= $nm_grup_layanan; ?>" />
                                <?= form_error('nm_grup_layanan') ?>
                            </div>
                        </div>
                    </div>
					<div class="form-group">
                        <div class="row">
                            <div class="col-md-6 col-md-offset-2">
                              
                                <button type="submit" class="btn btn-primary"><i class="fa fa-floppy-o"></i> <?= $button ?></button>
                                <a href="<?= site_url('tbl_grup_pelayanan') ?>" class="btn btn-danger"><i class="fa fa-sign-out"></i> Kembali</a>
                            </div>
                        </div>
                    </div>
				</form>
            </div>
        </div>
    </div>
</div>