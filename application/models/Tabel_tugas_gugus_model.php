<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tabel_tugas_gugus_model extends CI_Model
{

    public $table = 'tabel_tugas_gugus';
    public $id = 'id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function json() {//tabel_laporan_tugas
        $this->datatables->select('nama_opd,tabel_pegawai.nama,tabel_tugas.tugas nm_tugas,nama_gugus,tabel_tugas_gugus.id,id_gugus_tugas,tabel_tugas_gugus.tugas,tabel_tugas_gugus.deskripsi,tabel_tugas_gugus.pic,tabel_tugas_gugus.status');
        $this->datatables->from('tabel_tugas_gugus');
        //add this line for join
        $this->datatables->join('tabel_gugus', 'tabel_tugas_gugus.id_gugus_tugas = tabel_gugus.id');
        $this->datatables->join('tabel_tugas', 'tabel_tugas_gugus.tugas = tabel_tugas.id');
        $this->datatables->join('tabel_pegawai', 'tabel_tugas_gugus.pic = tabel_pegawai.id_peg');
        $this->datatables->join('tabel_opd', 'tabel_gugus.kode_opd = tabel_opd.id');
        $this->datatables->add_column('action', anchor(site_url('tabel_tugas_gugus/read/$1'),'<i class="bi bi-eye" aria-hidden="true"></i>', array('class' => 'btn btn-sm btn-success', 'title' => 'Lihat Detail Data'))." 
            ".anchor(site_url('tabel_tugas_gugus/update/$1'),'<i class="bi bi-pencil" aria-hidden="true"></i>', array('class' => 'btn btn-sm btn-warning', 'title' => 'Ubah Data'))." 
                ".anchor(site_url('tabel_tugas_gugus/delete/$1'),'<i class="bi bi-trash" aria-hidden="true"></i>','class="btn btn-sm btn-danger hapus" title="Hapus Data"'), 'id');
        return $this->datatables->generate();
    }
    function jsonn($id) {
        $this->datatables->select('nama_opd,tabel_pegawai.nama,tabel_tugas_gugus.tugas nm_tugas,nama_gugus,tabel_tugas_gugus.id,id_gugus_tugas,tabel_tugas_gugus.deskripsi,tabel_tugas_gugus.pic,tabel_tugas_gugus.status');
        $this->datatables->from('tabel_tugas_gugus');
        $this->datatables->where('id_gugus_tugas',$id);
        //add this line for join
        $this->datatables->join('tabel_gugus', 'tabel_tugas_gugus.id_gugus_tugas = tabel_gugus.id');
        $this->datatables->join('tabel_tugas', 'tabel_tugas_gugus.tugas = tabel_tugas.id');
        $this->datatables->join('tabel_pegawai', 'tabel_tugas_gugus.pic = tabel_pegawai.id_peg');
        $this->datatables->join('tabel_opd', 'tabel_gugus.kode_opd = tabel_opd.id');
        $this->datatables->add_column('action', anchor(site_url('tabel_laporan_tugas/laporan_tugas/$1'),'<i class="bi bi-upload" aria-hidden="true"></i>', array('class' => 'btn btn-sm btn-primary', 'title' => 'Upload Laporan'))." 
            ".anchor('#','<i class="bi bi-download" aria-hidden="true"></i>', array('class' => 'btn btn-sm btn-info modal_lap','id'=>'modal_lap', 'title' => 'Unduh Laporan'))." 
            ".anchor(site_url('tabel_tugas_gugus/read/$1'),'<i class="bi bi-eye" aria-hidden="true"></i>', array('class' => 'btn btn-sm btn-success', 'title' => 'Lihat Detail Data'))." 
            ".anchor(site_url('tabel_tugas_gugus/update/$1'),'<i class="bi bi-pencil" aria-hidden="true"></i>', array('class' => 'btn btn-sm btn-warning', 'title' => 'Ubah Data'))." 
                ".anchor(site_url('tabel_tugas_gugus/delete/$1'),'<i class="bi bi-trash" aria-hidden="true"></i>','class="btn btn-sm btn-danger hapus" title="Hapus Data"'), 'id');
        return $this->datatables->generate();
    }
    function json_user($id) {
        $this->datatables->select('nama_opd,tabel_pegawai.nama,tabel_tugas_gugus.tugas nm_tugas,nama_gugus,tabel_tugas_gugus.id,id_gugus_tugas,tabel_tugas_gugus.deskripsi,tabel_tugas_gugus.pic,tabel_tugas_gugus.status');
        $this->datatables->from('tabel_tugas_gugus');
        $this->datatables->where('id_peg',$id);
        //add this line for join
        $this->datatables->join('tabel_gugus', 'tabel_tugas_gugus.id_gugus_tugas = tabel_gugus.id');
        $this->datatables->join('tabel_tugas', 'tabel_tugas_gugus.tugas = tabel_tugas.id');
        $this->datatables->join('tabel_pegawai', 'tabel_tugas_gugus.pic = tabel_pegawai.id_peg');
        $this->datatables->join('tabel_opd', 'tabel_gugus.kode_opd = tabel_opd.id');
        $this->datatables->add_column('action', anchor(site_url('tabel_laporan_tugas/laporan_tugas/$1'),'<i class="bi bi-upload" aria-hidden="true"></i>', array('class' => 'btn btn-sm btn-primary', 'title' => 'Upload Laporan'))." 
            ".anchor('#','<i class="bi bi-download" aria-hidden="true"></i>', array('class' => 'btn btn-sm btn-info modal_lap','id'=>'modal_lap', 'title' => 'Unduh Laporan'))." 
            ".anchor(site_url('tabel_tugas_gugus/read/$1'),'<i class="bi bi-eye" aria-hidden="true"></i>', array('class' => 'btn btn-sm btn-success', 'title' => 'Lihat Detail Data'))." 
            ".anchor(site_url('tabel_tugas_gugus/update/$1'),'<i class="bi bi-pencil" aria-hidden="true"></i>', array('class' => 'btn btn-sm btn-warning', 'title' => 'Ubah Data'))." 
                ".anchor(site_url('tabel_tugas_gugus/delete/$1'),'<i class="bi bi-trash" aria-hidden="true"></i>','class="btn btn-sm btn-danger hapus" title="Hapus Data"'), 'id');
        return $this->datatables->generate();
    }

    function json_opd($id_opd=null) {
        $this->datatables->select('nama_opd,tabel_pegawai.nama,tabel_tugas_gugus.tugas nm_tugas,nama_gugus,tabel_tugas_gugus.id,id_gugus_tugas,tabel_tugas_gugus.deskripsi,tabel_tugas_gugus.pic,tabel_tugas_gugus.status');
        $this->datatables->from('tabel_tugas_gugus');
        $this->datatables->where('tabel_gugus.kode_opd',$id_opd);
        //add this line for join
        $this->datatables->join('tabel_gugus', 'tabel_tugas_gugus.id_gugus_tugas = tabel_gugus.id');
        $this->datatables->join('tabel_tugas', 'tabel_tugas_gugus.tugas = tabel_tugas.id');
        $this->datatables->join('tabel_pegawai', 'tabel_tugas_gugus.pic = tabel_pegawai.id_peg');
        $this->datatables->join('tabel_opd', 'tabel_gugus.kode_opd = tabel_opd.id');
        $this->datatables->add_column('action', anchor(site_url('tabel_laporan_tugas/laporan_tugas/$1'),'<i class="bi bi-upload" aria-hidden="true"></i>', array('class' => 'btn btn-sm btn-primary', 'title' => 'Upload Laporan'))." 
            ".anchor('#','<i class="bi bi-download" aria-hidden="true"></i>', array('class' => 'btn btn-sm btn-info modal_lap','id'=>'modal_lap', 'title' => 'Unduh Laporan'))." 
            ".anchor(site_url('tabel_tugas_gugus/read/$1'),'<i class="bi bi-eye" aria-hidden="true"></i>', array('class' => 'btn btn-sm btn-success', 'title' => 'Lihat Detail Data'))." 
            ".anchor(site_url('tabel_tugas_gugus/update/$1'),'<i class="bi bi-pencil" aria-hidden="true"></i>', array('class' => 'btn btn-sm btn-warning', 'title' => 'Ubah Data'))." 
                ".anchor(site_url('tabel_tugas_gugus/delete/$1'),'<i class="bi bi-trash" aria-hidden="true"></i>','class="btn btn-sm btn-danger hapus" title="Hapus Data"'), 'id');
        return $this->datatables->generate();
    }


    // get all
    function get_all()
    {
        $this->db->select('tabel_tugas_gugus.*'); 
        $this->db->join('tabel_gugus','tabel_gugus.id=tabel_tugas_gugus.id_gugus_tugas');
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }
    // get all
    function get_by_opd($id_opd=null)
    {
        $this->db->select('tabel_tugas_gugus.*');
        $this->db->where('tabel_gugus.kode_opd', $id_opd);
        $this->db->join('tabel_gugus','tabel_gugus.id=tabel_tugas_gugus.id_gugus_tugas');
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
       $this->db->like("cast( id as text)", $q);
		$this->db->or_like("cast(id_gugus_tugas as text)", $q);
		$this->db->or_like("cast(tugas as text)", $q);
		$this->db->or_like("cast(deskripsi as text)", $q);
		$this->db->or_like("cast(pic as text)", $q);
		$this->db->or_like("cast(status as text)", $q);
		$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like("cast( id as text)", $q);
		$this->db->or_like("cast(id_gugus_tugas as text)", $q);
		$this->db->or_like("cast(tugas as text)", $q);
		$this->db->or_like("cast(deskripsi as text)", $q);
		$this->db->or_like("cast(pic as text)", $q);
		$this->db->or_like("cast(status as text)", $q);
		$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

/* End of file Tabel_tugas_gugus_model.php */
/* Location: ./application/models/Tabel_tugas_gugus_model.php */
/* Please DO NOT modify this information : */
/* Generated by CRUDV2 Generator For AdminLTE Template 2024-01-08 07:59:19 */