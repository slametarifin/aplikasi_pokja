// ini adalah file javascript yang tadinya ada di template master sekarang di ganti menjadi satu file agar lebih ringan dan di chaced oleh browser


//file document ready function 

$(document).ready(function () {
    //alertify.log('Sukses di muat');
    setTimeout(function () {
        $("#preloader").fadeOut();
    }, 100);

    //untuk inisialisasi summernote
    $('.summernote').summernote({
        height: 150,
        followingToolbar: false
    });

    //inisialisasi selec2 
    $(".select2").select2();

    //datepicker declare
    $('#reservation').daterangepicker()

    //datepicker declare
    $('.datetimepicker').bootstrapMaterialDatePicker({
      format: 'YYYY-MM-DD HH:mm',
      lang: 'id',
      shortTime :false,
    });
    
    //datepicker declare
    $('#datetimepicker').bootstrapMaterialDatePicker({
      format: 'YYYY-MM-DD HH:mm:ss',
      lang: 'id'
    });

    //datepicker declare
    $('.dtime').bootstrapMaterialDatePicker({
        format: 'YYYY-MM-DD HH:mm',
        lang : 'id',
        shortTime :false,
    });

    //timepicker declare
    $('.timepicker').datetimepicker({
          format : 'HH:mm'
    });

    //monthpicker declare
    $('.monthpicker').datetimepicker({
          format : 'YYYY-MM'
    });

    //multidatepicker declare
    $('.multidatepicker').datepicker({
      multidate: true,
      format : 'yyyy-mm-dd'
    });
    //--------------------

});



////====================================================////
////========== FUNGSI TAMBAHAN AGAR DI CHACED ==========////
////====================================================////

//metode untuk notifikasi pasien yang memiliki duplikasi nomer rekam medis
function notif_duplikat(no_rm) {
    Swal.fire({
      title: 'Error',
      text: 'Nomor NIK Sudah terdaftar dengan Nomor RM : ' +no_rm,
      type: 'warning',
      showCancelButton: false,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, Pilih'
    }).then((result) => {
      if (result.value) {
        // document.location.href = remove;
        location.reload();
      }
    })
  }    

  //metode untuk mengirim notifikasi pasien kosong
  function notif_kosong() {
    Swal.fire({
      title: 'Data Pasien Masih Kosong',
      text: 'silahkan lengkapi',
      type: 'warning',
      showCancelButton: false,
      confirmButtonColor: '#3085d6',
      cancelButtonColor: '#d33',
      confirmButtonText: 'Ya, lengkapi'
    }).then((result) => {
      if (result.value) {
        // document.location.href = remove;
        location.reload();
      }
    })
  }
  
  //javascrip untuk tampilan jam yang ada di header jam berjalan 
  window.setTimeout("waktu()", 1000);
	function waktu() {
		var waktu = new Date();
		setTimeout("waktu()", 1000);
        if(waktu.getHours()<10){
		document.getElementById("jam").innerHTML = '  0'+waktu.getHours();
        }else{
		document.getElementById("jam").innerHTML = waktu.getHours();
        }
        if(waktu.getMinutes()<10){
		document.getElementById("menit").innerHTML = ' : 0'+waktu.getMinutes();
        }else{
		document.getElementById("menit").innerHTML = ' : '+waktu.getMinutes();
        }
        if(waktu.getSeconds()<10){
		document.getElementById("detik").innerHTML = ' : 0'+waktu.getSeconds();
        }else{
		document.getElementById("detik").innerHTML = ' : '+waktu.getSeconds();
        }
	}