
function set_format_rupiah(data)
{
    if(data!=null)
    {
        split           = data.split(','),
        sisa            = split[0].length % 3,
        rupiah          = split[0].substr(0, sisa),
        ribuan          = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return 'Rp '+rupiah+'';    
    }else{
        return 'Rp 0';    
    }

}

function set_curency(data)
{
    if(data!=null)
    {
        split           = data.split(','),
        sisa            = split[0].length % 3,
        rupiah          = split[0].substr(0, sisa),
        ribuan          = split[0].substr(sisa).match(/\d{3}/gi);

        // tambahkan titik jika yang di input sudah menjadi angka ribuan
        if(ribuan){
            separator = sisa ? '.' : '';
            rupiah += separator + ribuan.join('.');
        }

        rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
        return rupiah;    
    }else{
        return '0';    
    }

}

function set_format_tanggal(data,spliter)
{
    var d = new Date(data),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;

    return [day, month, year].join(spliter);
}

function set_format_tanggal_new(data,spliter)
{
    var d = new Date(data),
    month = '' + (d.getMonth() + 1),
    day = '' + d.getDate(),
    year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;
        switch (month) {
			case '01':
				month = 'January';
				break;
			case '02':
				month = 'Februari';
				break;
			case '03':
				month = 'Maret';
				break;
			case '04':
				month = 'April';
				break;
			case '05':
				month = 'Mei';
				break;
			case '06':
				month = 'Juni';
				break;
			case '07':
				month = 'July';
				break;
			case '08':
				month = 'Agustus';
				break;
			case '09':
				month = 'September';
				break;
			case '10':
				month = 'Oktober';
				break;
			case '11':
				month = 'November';
				break;
			case '12':
				month = 'Desember';
				break;
			default:
				month = 'Empty';
		}
    return [day, month, year].join(spliter);
}



function input_idr(id){

    var angka = $('#'+id).val();

    var number_string = angka.replace(/[^,\d]/g, '').toString(),
    split           = number_string.split(','),
    sisa            = split[0].length % 3,
    rupiah          = split[0].substr(0, sisa),
    ribuan          = split[0].substr(sisa).match(/\d{3}/gi);

    // tambahkan titik jika yang di input sudah menjadi angka ribuan
    if(ribuan){
        separator = sisa ? '.' : '';
        rupiah += separator + ribuan.join('.');
    }

    rupiah = split[1] != undefined ? rupiah + ',' + split[1] : rupiah;
    // return 'Rp. ' + rupiah ;
    $('#'+id).val('Rp ' + rupiah)
}

function format_eachUpperCase(str) {
   var splitStr = str.toLowerCase().split('_');
   for (var i = 0; i < splitStr.length; i++) {
       // You do not need to check if i is larger than splitStr length, as your for does that for you
       // Assign it back to the array
       splitStr[i] = splitStr[i].charAt(0).toUpperCase() + splitStr[i].substring(1);     
   }
   // Directly return the joined string
   return splitStr.join(' '); 
}



