<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>Kab.Cilacap</title>

  <!-- Google Font: Source Sans Pro -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="<?=site_url('./assets/adminlte/')?>plugins/fontawesome-free/css/all.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="<?=site_url('./assets/adminlte/')?>dist/css/adminlte.min.css">
</head>
<body class="hold-transition lockscreen" style="background-color:white;">
  <!--style="background-image:url('<?=site_url('./assets/img/Logo-Cilacap.png')?>');background-size: 10% 30%;background-repeat: no-repeat;background-position-x: 10%;background-position-y: 10%;"-->
<!-- Automatic element centering -->
<div class="lockscreen-wrapper">
  <?= $contents ?> 
</div>
<!-- /.center -->

<!-- jQuery -->
<script src="<?=site_url('./assets/adminlte/')?>plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="<?=site_url('./assets/adminlte/')?>plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
</body>
</html>
