<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tabel_atasan_bawahan_model extends CI_Model
{

    public $table = 'tabel_atasan_bawahan';
    public $id = 'id';
    public $order = 'DESC';

    function __construct()
    {
        parent::__construct();
    }

    // datatables
    function json_opd($id_opd=null) {
        $this->datatables->select('tabel_atasan_bawahan.id,id_atasan,id_staff,tabel_atasan_bawahan.id_opd,a.nama nama_atasan,b.nama nama_staff,nama_opd');
        $this->datatables->from('tabel_atasan_bawahan');
        $this->datatables->where('tabel_atasan_bawahan.id_opd',$id_opd);
        //add this line for join
        $this->datatables->join('tabel_pegawai a', 'tabel_atasan_bawahan.id_atasan = a.id_peg');
        $this->datatables->join('tabel_pegawai b', 'tabel_atasan_bawahan.id_staff = b.id_peg');
        $this->datatables->join('tabel_opd c', 'tabel_atasan_bawahan.id_opd = c.id');
        $this->datatables->add_column('action', anchor(site_url('tabel_atasan_bawahan/read/$1'),'<i class="bi bi-eye" aria-hidden="true"></i>', array('class' => 'btn btn-sm btn-success', 'title' => 'Lihat Detail Data'))." 
            ".anchor(site_url('tabel_atasan_bawahan/update/$1'),'<i class="bi bi-pencil" aria-hidden="true"></i>', array('class' => 'btn btn-sm btn-warning', 'title' => 'Ubah Data'))." 
                ".anchor(site_url('tabel_atasan_bawahan/delete/$1'),'<i class="bi bi-trash" aria-hidden="true"></i>','class="btn btn-sm btn-danger hapus" title="Hapus Data"'), 'id');
        return $this->datatables->generate();
    }
    function json_limit1($id_user=null) {
        $this->datatables->select('tabel_atasan_bawahan.id,id_atasan,id_staff,tabel_atasan_bawahan.id_opd,a.nama nama_atasan,b.nama nama_staff,nama_opd');
        $this->datatables->from('tabel_atasan_bawahan');
        $this->datatables->where('id_staff',$id_user);
        //add this line for join
        $this->datatables->join('tabel_pegawai a', 'tabel_atasan_bawahan.id_atasan = a.id_peg');
        $this->datatables->join('tabel_pegawai b', 'tabel_atasan_bawahan.id_staff = b.id_peg');
        $this->datatables->join('tabel_opd c', 'tabel_atasan_bawahan.id_opd = c.id');
        $this->datatables->add_column('action', anchor(site_url('tabel_atasan_bawahan/read/$1'),'<i class="bi bi-eye" aria-hidden="true"></i>', array('class' => 'btn btn-sm btn-success', 'title' => 'Lihat Detail Data'))." 
            ".anchor(site_url('tabel_atasan_bawahan/update/$1'),'<i class="bi bi-pencil" aria-hidden="true"></i>', array('class' => 'btn btn-sm btn-warning', 'title' => 'Ubah Data'))." 
                ".anchor(site_url('tabel_atasan_bawahan/delete/$1'),'<i class="bi bi-trash" aria-hidden="true"></i>','class="btn btn-sm btn-danger hapus" title="Hapus Data"'), 'id');
        return $this->datatables->generate();
    }
    function json() {
        $this->datatables->select('tabel_atasan_bawahan.id,id_atasan,id_staff,tabel_atasan_bawahan.id_opd,a.nama nama_atasan,b.nama nama_staff,nama_opd');
        $this->datatables->from('tabel_atasan_bawahan');
        //add this line for join
        $this->datatables->join('tabel_pegawai a', 'tabel_atasan_bawahan.id_atasan = a.id_peg');
        $this->datatables->join('tabel_pegawai b', 'tabel_atasan_bawahan.id_staff = b.id_peg');
        $this->datatables->join('tabel_opd c', 'tabel_atasan_bawahan.id_opd = c.id');
        $this->datatables->add_column('action', anchor(site_url('tabel_atasan_bawahan/read/$1'),'<i class="bi bi-eye" aria-hidden="true"></i>', array('class' => 'btn btn-sm btn-success', 'title' => 'Lihat Detail Data'))." 
            ".anchor(site_url('tabel_atasan_bawahan/update/$1'),'<i class="bi bi-pencil" aria-hidden="true"></i>', array('class' => 'btn btn-sm btn-warning', 'title' => 'Ubah Data'))." 
                ".anchor(site_url('tabel_atasan_bawahan/delete/$1'),'<i class="bi bi-trash" aria-hidden="true"></i>','class="btn btn-sm btn-danger hapus" title="Hapus Data"'), 'id');
        return $this->datatables->generate();
    }

    // get all
    function get_all()
    {
        $this->db->order_by($this->id, $this->order);
        return $this->db->get($this->table)->result();
    }

    // get data by id
    function get_by_id($id)
    {
        $this->db->where($this->id, $id);
        return $this->db->get($this->table)->row();
    }
    
    // get total rows
    function total_rows($q = NULL) {
       $this->db->like("cast( id as text)", $q);
		$this->db->or_like("cast(id_atasan as text)", $q);
		$this->db->or_like("cast(id_staff as text)", $q);
		$this->db->or_like("cast(id_opd as text)", $q);
		$this->db->from($this->table);
        return $this->db->count_all_results();
    }

    // get data with limit and search
    function get_limit_data($limit, $start = 0, $q = NULL) {
        $this->db->order_by($this->id, $this->order);
        $this->db->like("cast( id as text)", $q);
		$this->db->or_like("cast(id_atasan as text)", $q);
		$this->db->or_like("cast(id_staff as text)", $q);
		$this->db->or_like("cast(id_opd as text)", $q);
		$this->db->limit($limit, $start);
        return $this->db->get($this->table)->result();
    }

    // insert data
    function insert($data)
    {
        $this->db->insert($this->table, $data);
    }

    // update data
    function update($id, $data)
    {
        $this->db->where($this->id, $id);
        $this->db->update($this->table, $data);
    }

    // delete data
    function delete($id)
    {
        $this->db->where($this->id, $id);
        $this->db->delete($this->table);
    }

}

/* End of file Tabel_atasan_bawahan_model.php */
/* Location: ./application/models/Tabel_atasan_bawahan_model.php */
/* Please DO NOT modify this information : */
/* Generated by CRUDV2 Generator For AdminLTE Template 2024-01-12 04:39:57 */