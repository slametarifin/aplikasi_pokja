<script type="text/javascript">
    
function get_option(schema_ctrl,fungsi,select_id,id=null,keyval=false)
{
    // console.log('tes');

    var url="<?=site_url()?>"+"/"+schema_ctrl+"/"+fungsi;
    select_id=select_id.replace('[','\\[');
    select_id=select_id.replace(']','\\]');

    
    $.ajax({
      type: "GET",
      url: url,
      dataType: "json",

      success: function(hasil){
        //
        $(select_id).children('option:not(:first)').remove();

        // console.log(hasil);
        $.each(hasil, function(key, value){
            
            if(id==key)
            {
                //
                if(keyval==true)
                {
                    $(select_id).append('<option  value="'+key+'"  selected>'+key+" - "+value+'</<option>');
                }else{
                    $(select_id).append('<option value="'+key+'"  selected>'+value+'</<option>');
                }
            }else{
                //
                if(keyval==true)
                {
                    $(select_id).append('<option value="'+key+'" >'+key+" - "+value+'</<option>');    
                }else{
                    $(select_id).append('<option value="'+key+'" >'+value+'</<option>');
                }
                
            }
        
        });
      }
        
    });
}


function get_option_server_side(schema_ctrl,fungsi,select_id,value=null,keyval=false,placeholder=null)
{
    select_id=select_id.replace('[','\\[');
    select_id=select_id.replace(']','\\]');
    
    var id_option='';
    var val_option='- Pilih -';
    if(value!=null){
    
      if(value['id']!=null && value['label']!=null)
      {
        id_option=value['id'];
        if(keyval==false)
        {
          val_option=value['label'];  
        }else{
          val_option=value['id']+' - '+value['label'];
        }
        

      }      
      
    }

    $(function(){
          $(select_id).children('option').remove();


          option = new Option(val_option, id_option, true, true);
          option.selected = true;
          $(select_id).append(option);
          
          
          $(select_id).select2({
            
            
            minimumInputLength: 3,
            placeholder: placeholder,
            allowClear: false,
          
            ajax: {
              dataType: 'json',
              url: '<?= site_url() ?>'+schema_ctrl+'/'+fungsi,
              delay: 800,
              data: function(params) {
                return {
                  search: params.term
                }
              },
              processResults: function(data) {
                // console.log(data);
                var results=[];
                $.each(data, function(index, item) {
                  if(keyval==false){
                    results.push({
                      id: index,
                      text: item
                    });
                  }else{
                    results.push({
                      id: index,
                      text: index+' - '+item
                    });
                  }
                  
                });
                return {
                  results: results
                };

              },
            }
          });


    });
}

function get_multi_option_server_side(schema_ctrl,fungsi,select_id,value=null,keyval=false,placeholder=null)
{
    select_id=select_id.replace('[','\\[');
    select_id=select_id.replace(']','\\]');
    
    var id_option='';
    var val_option='- Pilih -';
    if(value!=null){
    

      if(value['id']!=null && value['label']!=null)
      {
        id_option=value['id'];
        val_option=value['label'];
      }      
      
    }

    $(function(){
          $(select_id).children('option').remove();

          if (value!=null) {

            $.each(value,function(key,val){
              if(val.id!='')
              {
                option = new Option(val.label, val.id, true, true);
              
                $(select_id).append(option);    
              }

            });
              
          }

          
          
          $(select_id).select2({
            
            
            minimumInputLength: 3,
            placeholder: placeholder,
            allowClear: false,
          
            ajax: {
              dataType: 'json',
              url: '<?= site_url() ?>'+schema_ctrl+'/'+fungsi,
              delay: 800,
              data: function(params) {
                return {
                  search: params.term
                }
              },
              processResults: function(data) {
                // console.log(data);
                var results=[];
                $.each(data, function(index, item) {
                  if(keyval==false){
                    results.push({
                      id: index,
                      text: item
                    });
                  }else{
                    results.push({
                      id: index,
                      text: index+' - '+item
                    });
                  }
                  
                });
                return {
                  results: results
                };

              },
            }
          });
          

    });
}

String.prototype.replaceAll = function(str1, str2, ignore) 
{
    return this.replace(new RegExp(str1.replace(/([\/\,\!\\\^\$\{\}\[\]\(\)\.\*\+\?\|\<\>\-\&])/g,"\\$&"),(ignore?"gi":"g")),(typeof(str2)=="string")?str2.replace(/\$/g,"$$$$"):str2);
}
</script>